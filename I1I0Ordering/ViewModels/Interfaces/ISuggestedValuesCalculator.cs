﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.ViewModels.Interfaces
{
   public interface ISuggestedValuesCalculator
    {
        double SuggestedValue( double currentdensity, double targetvalue);
    }
}
