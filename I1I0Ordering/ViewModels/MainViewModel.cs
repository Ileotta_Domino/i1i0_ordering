﻿using I1I0Ordering.Helpers;
using I1I0Ordering.Model;
using I1I0Ordering.Model.Interface;
using I1I0Ordering.Parameters;
using I1I0Ordering.Parameters.Interface;
using I1I0Ordering.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Excel = Microsoft.Office.Interop.Excel;

namespace I1I0Ordering.ViewModels
{
     class MainViewModel : ViewModelBase, IClosableViewModel, INotifyPropertyChanged
    {
        public event EventHandler CloseWindowEvent;
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly ExcelParserViewModel _ExcelParserViewModel=new ExcelParserViewModel();

        private readonly IExtractor _extractor; 
        private readonly IListProcessor _listProcessor;
        private readonly IFileProcessor _fileProcessor;
        private readonly ICalculatorTools _calculatorTools;
        private readonly ILABDensityValuesProcessor _LABDensityValuesProcessor;
        private readonly IHeadValueProcessor _headValueProcessor;
        private readonly ISuggestedValuesCalculator _suggestedValuesCalculator;

        private string _InputFile;
        private string _InputLogFile;
        private string _fileCheckOutput = "";

        string fileName = "C:\\Users\\I_Leotta\\Documents\\Test_Organiser.txt";
        List<string[]> arrayList = new List<string[]>();
        List<Elements> ElementList = new List<Elements>();
        List<double> TestList = new List<double>();
        List<double> ElleList = new List<double>();
        List<double> AiList = new List<double>();
        List<double> BeList = new List<double>();
        List<double> DensityList = new List<double>();
        List<double> HeadThreeList = new List<double>();





        public VioletValue _violetValue { get; set; }
        public OrangeValue _orangeValue { get; set; }
        public BlackValue _blackValue { get; set; }
        public YellowValue _yellowValue { get; set; }
        public MagentaValue _magentaValue { get; set; }
        public CyanValue _cyanValue { get; set; }
      //  public HeadViolet _headViolet { get; set; }


        public string InputFile
        {
            get {
                return _InputFile;

            }
                set
            {  
                if (_InputFile != value)
                {
                    _fileCheckOutput = "";
                     _InputFile = value;
                    _fileCheckOutput = _fileProcessor.checkFile(_InputFile);
                    if (_fileCheckOutput == "NotFound")
                    {
                        MessageBox.Show("File cannot be found");

                    }
                    if (_fileCheckOutput == "Empty")
                    {
                        MessageBox.Show("File is empty");

                    }
                    _fileProcessor.SetBlankFileLocation(_InputFile);
                    OnPropertyChanged("InputFile");
                }
            }
        }

        public string InputLogFile
        {
            get
            {
                return _InputLogFile;

            }
            set
            {
                if (_InputLogFile != value)
                {
                    _fileCheckOutput = "";
                    _InputLogFile = value;
                    _fileCheckOutput = _fileProcessor.checkFile(_InputLogFile);
                    if (_fileCheckOutput == "NotFound")
                    {
                        MessageBox.Show("File cannot be found");

                    }
                    if (_fileCheckOutput == "Empty")
                    {
                        MessageBox.Show("File is empty");

                    }
                    _fileProcessor.SetBlankFileLocation(_InputLogFile);
                    OnPropertyChanged("InputLogFile");
                }
            }
        }


        public RelayCommand StartCommand { get; set; }
        public MainViewModel(IExtractor extractor, IListProcessor listProcessor, ICalculatorTools calculatorTools, ILABDensityValuesProcessor lABDensityValuesProcessor, IFileProcessor fileProcessor, IHeadValueProcessor headValueProcessor, ISuggestedValuesCalculator suggestedValuesCalculator)
        {
            _extractor = extractor;
            _listProcessor = listProcessor;
            _calculatorTools = calculatorTools;
           _LABDensityValuesProcessor = lABDensityValuesProcessor;
            _violetValue = new VioletValue();
        //    _headViolet = new HeadViolet();
            _orangeValue = new OrangeValue();
            _blackValue = new BlackValue();
            _yellowValue = new YellowValue();
            _magentaValue = new MagentaValue();
            _cyanValue = new CyanValue();
            _fileProcessor = fileProcessor;
            _headValueProcessor = headValueProcessor;
            _suggestedValuesCalculator = suggestedValuesCalculator;



            StartCommand = new RelayCommand(Organiser, canexecute);
        }

        private bool canexecute(object parameter)
        {

            bool res = true;
            if (_InputFile == null && _InputLogFile==null)
            { res = false; }
            if (_fileCheckOutput !="OK")
            {
                res = false;
            }

            return res;
        }

        public void Organiser(object parameter)

        {
            double VioletAverage = 0;
            int numberOfSets;
            int maxcounter;
            HeadSortedStrategy _headSortedStrategy=new HeadSortedStrategy();
            string[] elem = new string[4];
         //   var filePath = Environment.ExpandEnvironmentVariables(fileName);
            var filePath = Environment.ExpandEnvironmentVariables(_InputFile);

            //Find the number of sets
            numberOfSets = _extractor.FindLine(filePath);
            //sets/list length
            maxcounter = numberOfSets /72;


            
            //start countr

            List<int> Violet1 = ListKeeper.createVioletD4List();
            List<int> Violet2 = ListKeeper.createVioletD3List();
            List<int> Orange1 = ListKeeper.createOrangeD4List();
            List<int> Orange2 = ListKeeper.createOrangeD3List();
            List<int> Black1 = ListKeeper.createBlackD4List();
            List<int> Black2 = ListKeeper.createBlackD3List();
            List<int> Yellow1 = ListKeeper.createYellowD4List();
            List<int> Yellow2 = ListKeeper.createYellowD3List();
            List<int> Magenta1 = ListKeeper.createMagentaD4List();
            List<int> Magenta2 = ListKeeper.createMagentaD3List();
            List<int> Cyan1 = ListKeeper.createCyanD4List();
            List<int> Cyan2 = ListKeeper.createCyanD3List();


           

            Elements EL = new Elements();
                EL.Elle = "0";
                EL.Ai = "0";
                EL.Be = "0";
                EL.Density = "0";

            HeadSegment HSeg = new HeadSegment();
            HSeg.SegmentOne = 0;
            HSeg.SegmentTwo = 0;
            HSeg.SegmentThree = 0;
            HSeg.SegmentFour = 0;
            /////////////////////
            //Violet
            /////////////////////
            for (int i = 0; i < maxcounter; i++)
            {
                foreach (var l in Violet1)
                {
                    string inputElle= _extractor.ExtractValueFromFile(filePath, ((i*72)+l+23), 2).Trim();  
                    string inputAi = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23),  11).Trim();
                    string inputBe = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 20).Trim();
                    string inputDensity = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23),  47).Trim();

                    ElleList.Add(Convert.ToDouble(inputElle));
                    AiList.Add(Convert.ToDouble(inputAi));
                    BeList.Add(Convert.ToDouble(inputBe));
                    DensityList.Add(Convert.ToDouble(inputDensity));                   

                    HeadThreeList.Add(Convert.ToDouble(inputDensity));
                    ElementList.Add(EL);
                }
                // _headSortedStrategy.
                double DensitytoShow = Math.Round(HeadThreeList.Average(), 2);
                _headSortedStrategy.SetHeadStrategy(new VioletHeadStrategy(_headValueProcessor));
                _headSortedStrategy.GetList(HeadThreeList);
                _headSortedStrategy.SetCase(i);
                _headSortedStrategy.OrderHeadByColor();

            }
            _violetValue.Result1LValue = Math.Round(ElleList.Average(), 2).ToString();
            _violetValue.Result1AValue = Math.Round(AiList.Average(), 2).ToString();
            _violetValue.Result1BValue = Math.Round(BeList.Average(), 2).ToString();
            _violetValue.Result1DensityValue = Math.Round(DensityList.Average(), 2).ToString();

           
           
            _violetValue.VioletHead3Segment1Value= _headValueProcessor.GetVioletHead3Segment1Value().ToString();
            _violetValue.VioletHead3Segment2Value = _headValueProcessor.GetVioletHead3Segment2Value();
            _violetValue.VioletHead3Segment3Value = _headValueProcessor.GetVioletHead3Segment3Value();
            _violetValue.VioletHead3Segment4Value = _headValueProcessor.GetVioletHead3Segment4Value();
            _violetValue.VioletHead2Segment1Value = _headValueProcessor.GetVioletHead2Segment1Value();
            _violetValue.VioletHead2Segment2Value = _headValueProcessor.GetVioletHead2Segment2Value();
            _violetValue.VioletHead2Segment3Value = _headValueProcessor.GetVioletHead2Segment3Value();
            _violetValue.VioletHead2Segment4Value = _headValueProcessor.GetVioletHead2Segment4Value();
            _violetValue.VioletHead1Segment1Value = _headValueProcessor.GetVioletHead1Segment1Value();
            _violetValue.VioletHead1Segment2Value = _headValueProcessor.GetVioletHead1Segment2Value();
            _violetValue.VioletHead1Segment3Value = _headValueProcessor.GetVioletHead1Segment3Value();
            _violetValue.VioletHead1Segment4Value = _headValueProcessor.GetVioletHead1Segment4Value();

            ClearList();
            for (int i = 0; i < maxcounter; i++)
            {
                foreach (var l in Violet2)
                {
                    string inputElle = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 2).Trim();
                    string inputAi = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 11).Trim();
                    string inputBe = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 20).Trim();
                    string inputDensity = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 47).Trim();

                    ElleList.Add(Convert.ToDouble(inputElle));
                    AiList.Add(Convert.ToDouble(inputAi));
                    BeList.Add(Convert.ToDouble(inputBe));
                    DensityList.Add(Convert.ToDouble(inputDensity));
                    HeadThreeList.Add(Convert.ToDouble(inputDensity));
                    ElementList.Add(EL);
                }
                // _headSortedStrategy.
                double DensitytoShow = Math.Round(HeadThreeList.Average(), 2);
                _headSortedStrategy.SetHeadStrategy(new VioletHeadStrategy(_headValueProcessor));
                _headSortedStrategy.GetList(HeadThreeList);
                _headSortedStrategy.SetCase(i);
                _headSortedStrategy.OrderHeadByColor();
            }

            _violetValue.Violet2Head3Segment1Value = _headValueProcessor.GetVioletHead3Segment1Value().ToString();
            _violetValue.Violet2Head3Segment2Value = _headValueProcessor.GetVioletHead3Segment2Value();
            _violetValue.Violet2Head3Segment3Value = _headValueProcessor.GetVioletHead3Segment3Value();
            _violetValue.Violet2Head3Segment4Value = _headValueProcessor.GetVioletHead3Segment4Value();
            _violetValue.Violet2Head2Segment1Value = _headValueProcessor.GetVioletHead2Segment1Value();
            _violetValue.Violet2Head2Segment2Value = _headValueProcessor.GetVioletHead2Segment2Value();
            _violetValue.Violet2Head2Segment3Value = _headValueProcessor.GetVioletHead2Segment3Value();
            _violetValue.Violet2Head2Segment4Value = _headValueProcessor.GetVioletHead2Segment4Value();
            _violetValue.Violet2Head1Segment1Value = _headValueProcessor.GetVioletHead1Segment1Value();
            _violetValue.Violet2Head1Segment2Value = _headValueProcessor.GetVioletHead1Segment2Value();
            _violetValue.Violet2Head1Segment3Value = _headValueProcessor.GetVioletHead1Segment3Value();
            _violetValue.Violet2Head1Segment4Value = _headValueProcessor.GetVioletHead1Segment4Value();

            _violetValue.Result2LValue = Math.Round(ElleList.Average(), 2).ToString();
            _violetValue.Result2AValue = Math.Round(AiList.Average(), 2).ToString();
            _violetValue.Result2BValue = Math.Round(BeList.Average(), 2).ToString();
            _violetValue.Result2DensityValue = Math.Round(DensityList.Average(), 2).ToString();


            _violetValue.Violet1CurrentHead1Voltages = 0; 

            ClearList();
            /////////////////////
            //Orange
            /////////////////////
            for (int i = 0; i < maxcounter; i++)
            {
                foreach (var l in Orange1)
                { 


                    string inputElle = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 2).Trim();
                    string inputAi = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 11).Trim();
                    string inputBe = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 20).Trim();
                    string inputDensity = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 56).Trim();

                    ElleList.Add(Convert.ToDouble(inputElle));
                    AiList.Add(Convert.ToDouble(inputAi));
                    BeList.Add(Convert.ToDouble(inputBe));
                    DensityList.Add(Convert.ToDouble(inputDensity));

                    HeadThreeList.Add(Convert.ToDouble(inputDensity));
                    ElementList.Add(EL);
                }
                double DensitytoShow = Math.Round(HeadThreeList.Average(), 2);
                _headSortedStrategy.SetHeadStrategy(new OrangeHeadStrategy(_headValueProcessor));
                _headSortedStrategy.GetList(HeadThreeList);
                _headSortedStrategy.SetCase(i);
                _headSortedStrategy.OrderHeadByColor(); 


            }

            _orangeValue.OrangeHead3Segment1Value = _headValueProcessor.GetOrangeHead3Segment1Value();
            _orangeValue.OrangeHead3Segment2Value = _headValueProcessor.GetOrangeHead3Segment2Value();
            _orangeValue.OrangeHead3Segment3Value = _headValueProcessor.GetOrangeHead3Segment3Value();
            _orangeValue.OrangeHead3Segment4Value = _headValueProcessor.GetOrangeHead3Segment4Value();
            _orangeValue.OrangeHead2Segment1Value = _headValueProcessor.GetOrangeHead2Segment1Value();
            _orangeValue.OrangeHead2Segment2Value = _headValueProcessor.GetOrangeHead2Segment2Value();
            _orangeValue.OrangeHead2Segment3Value = _headValueProcessor.GetOrangeHead2Segment3Value();
            _orangeValue.OrangeHead2Segment4Value = _headValueProcessor.GetOrangeHead2Segment4Value();
            _orangeValue.OrangeHead1Segment1Value = _headValueProcessor.GetOrangeHead1Segment1Value();
            _orangeValue.OrangeHead1Segment2Value = _headValueProcessor.GetOrangeHead1Segment2Value();
            _orangeValue.OrangeHead1Segment3Value = _headValueProcessor.GetOrangeHead1Segment3Value();
            _orangeValue.OrangeHead1Segment4Value = _headValueProcessor.GetOrangeHead1Segment4Value();

            _orangeValue.Result1LValue = Math.Round(ElleList.Average(), 2).ToString();
            _orangeValue.Result1AValue = Math.Round(AiList.Average(), 2).ToString();
            _orangeValue.Result1BValue = Math.Round(BeList.Average(), 2).ToString();
            _orangeValue.Result1DensityValue = Math.Round(DensityList.Average(), 2).ToString();


            ClearList();
            //////////
            //Orange2
            //////////
            for (int i = 0; i < maxcounter; i++)
            {
                foreach (var l in Orange2)
                {
                    string inputElle = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 2).Trim();
                    string inputAi = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 11).Trim();
                    string inputBe = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 20).Trim();
                    string inputDensity = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 56).Trim();

                    ElleList.Add(Convert.ToDouble(inputElle));
                    AiList.Add(Convert.ToDouble(inputAi));
                    BeList.Add(Convert.ToDouble(inputBe));
                    DensityList.Add(Convert.ToDouble(inputDensity));
                    HeadThreeList.Add(Convert.ToDouble(inputDensity));
                    ElementList.Add(EL);
                }
                _orangeValue.Orange2Head3Segment1Value = _headValueProcessor.GetOrangeHead3Segment1Value();
                _orangeValue.Orange2Head3Segment2Value = _headValueProcessor.GetOrangeHead3Segment2Value();
                _orangeValue.Orange2Head3Segment3Value = _headValueProcessor.GetOrangeHead3Segment3Value();
                _orangeValue.Orange2Head3Segment4Value = _headValueProcessor.GetOrangeHead3Segment4Value();
                _orangeValue.Orange2Head2Segment1Value = _headValueProcessor.GetOrangeHead2Segment1Value();
                _orangeValue.Orange2Head2Segment2Value = _headValueProcessor.GetOrangeHead2Segment2Value();
                _orangeValue.Orange2Head2Segment3Value = _headValueProcessor.GetOrangeHead2Segment3Value();
                _orangeValue.Orange2Head2Segment4Value = _headValueProcessor.GetOrangeHead2Segment4Value();
                _orangeValue.Orange2Head1Segment1Value = _headValueProcessor.GetOrangeHead1Segment1Value();
                _orangeValue.Orange2Head1Segment2Value = _headValueProcessor.GetOrangeHead1Segment2Value();
                _orangeValue.Orange2Head1Segment3Value = _headValueProcessor.GetOrangeHead1Segment3Value();
                _orangeValue.Orange2Head1Segment4Value = _headValueProcessor.GetOrangeHead1Segment4Value();

                double DensitytoShow = Math.Round(HeadThreeList.Average(), 2);
                _headSortedStrategy.SetHeadStrategy(new OrangeHeadStrategy(_headValueProcessor));
                _headSortedStrategy.GetList(HeadThreeList);
                _headSortedStrategy.SetCase(i);
                _headSortedStrategy.OrderHeadByColor();
            }

            _orangeValue.Result2LValue = Math.Round(ElleList.Average(), 2).ToString();
            _orangeValue.Result2AValue = Math.Round(AiList.Average(), 2).ToString();
            _orangeValue.Result2BValue = Math.Round(BeList.Average(), 2).ToString();
            _orangeValue.Result2DensityValue = Math.Round(DensityList.Average(), 2).ToString();


            ClearList();
            /////////////////////
            //Black
            /////////////////////
            for (int i = 0; i < maxcounter; i++)
            {
                foreach (var l in Black1)
                {
                    string inputElle = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 2).Trim();
                    string inputAi = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 11).Trim();
                    string inputBe = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 20).Trim();
                    string inputDensity = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 29).Trim();

                    ElleList.Add(Convert.ToDouble(inputElle));
                    AiList.Add(Convert.ToDouble(inputAi));
                    BeList.Add(Convert.ToDouble(inputBe));
                    DensityList.Add(Convert.ToDouble(inputDensity));

                    HeadThreeList.Add(Convert.ToDouble(inputDensity));
                    ElementList.Add(EL);
                }
                double DensitytoShow = Math.Round(HeadThreeList.Average(), 2);
                _headSortedStrategy.SetHeadStrategy(new BlackHeadStrategy(_headValueProcessor));
                _headSortedStrategy.GetList(HeadThreeList);
                _headSortedStrategy.SetCase(i);
                _headSortedStrategy.OrderHeadByColor();
            }

            _blackValue.BlackHead3Segment1Value = _headValueProcessor.GetBlackHead3Segment1Value();
            _blackValue.BlackHead3Segment2Value = _headValueProcessor.GetBlackHead3Segment2Value();
            _blackValue.BlackHead3Segment3Value = _headValueProcessor.GetBlackHead3Segment3Value();
            _blackValue.BlackHead3Segment4Value = _headValueProcessor.GetBlackHead3Segment4Value();
            _blackValue.BlackHead2Segment1Value = _headValueProcessor.GetBlackHead2Segment1Value();
            _blackValue.BlackHead2Segment2Value = _headValueProcessor.GetBlackHead2Segment2Value();
            _blackValue.BlackHead2Segment3Value = _headValueProcessor.GetBlackHead2Segment3Value();
            _blackValue.BlackHead2Segment4Value = _headValueProcessor.GetBlackHead2Segment4Value();
            _blackValue.BlackHead1Segment1Value = _headValueProcessor.GetBlackHead1Segment1Value();
            _blackValue.BlackHead1Segment2Value = _headValueProcessor.GetBlackHead1Segment2Value();
            _blackValue.BlackHead1Segment3Value = _headValueProcessor.GetBlackHead1Segment3Value();
            _blackValue.BlackHead1Segment4Value = _headValueProcessor.GetBlackHead1Segment4Value();

            _blackValue.Result1LValue = Math.Round(ElleList.Average(), 2).ToString();
            _blackValue.Result1AValue = Math.Round(AiList.Average(), 2).ToString();
            _blackValue.Result1BValue = Math.Round(BeList.Average(), 2).ToString();
            _blackValue.Result1DensityValue = Math.Round(DensityList.Average(), 2).ToString();


            for (int i = 0; i < maxcounter; i++)
            {
                foreach (var l in Black2)
                {
                    string inputElle = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 2).Trim();
                    string inputAi = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 11).Trim();
                    string inputBe = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 20).Trim();
                    string inputDensity = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 29).Trim();

                    ElleList.Add(Convert.ToDouble(inputElle));
                    AiList.Add(Convert.ToDouble(inputAi));
                    BeList.Add(Convert.ToDouble(inputBe));
                    DensityList.Add(Convert.ToDouble(inputDensity));

                    HeadThreeList.Add(Convert.ToDouble(inputDensity));
                    ElementList.Add(EL);
                }
                double DensitytoShow = Math.Round(HeadThreeList.Average(), 2);
                _headSortedStrategy.SetHeadStrategy(new BlackHeadStrategy(_headValueProcessor));
                _headSortedStrategy.GetList(HeadThreeList);
                _headSortedStrategy.SetCase(i);
                _headSortedStrategy.OrderHeadByColor();
            }

            _blackValue.Black2Head3Segment1Value = _headValueProcessor.GetBlackHead3Segment1Value();
            _blackValue.Black2Head3Segment2Value = _headValueProcessor.GetBlackHead3Segment2Value();
            _blackValue.Black2Head3Segment3Value = _headValueProcessor.GetBlackHead3Segment3Value();
            _blackValue.Black2Head3Segment4Value = _headValueProcessor.GetBlackHead3Segment4Value();
            _blackValue.Black2Head2Segment1Value = _headValueProcessor.GetBlackHead2Segment1Value();
            _blackValue.Black2Head2Segment2Value = _headValueProcessor.GetBlackHead2Segment2Value();
            _blackValue.Black2Head2Segment3Value = _headValueProcessor.GetBlackHead2Segment3Value();
            _blackValue.Black2Head2Segment4Value = _headValueProcessor.GetBlackHead2Segment4Value();
            _blackValue.Black2Head1Segment1Value = _headValueProcessor.GetBlackHead1Segment1Value();
            _blackValue.Black2Head1Segment2Value = _headValueProcessor.GetBlackHead1Segment2Value();
            _blackValue.Black2Head1Segment3Value = _headValueProcessor.GetBlackHead1Segment3Value();
            _blackValue.Black2Head1Segment4Value = _headValueProcessor.GetBlackHead1Segment4Value();

            _blackValue.Result2LValue = Math.Round(ElleList.Average(), 2).ToString();
            _blackValue.Result2AValue = Math.Round(AiList.Average(), 2).ToString();
            _blackValue.Result2BValue = Math.Round(BeList.Average(), 2).ToString();
            _blackValue.Result2DensityValue = Math.Round(DensityList.Average(), 2).ToString();

            ClearList();
            /////////////////////
            // Yellow
            /////////////////////
            for (int i = 0; i < maxcounter; i++)
            {
                foreach (var l in Yellow1)
                {
                    string inputElle = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 2).Trim();
                    string inputAi = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 11).Trim();
                    string inputBe = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 20).Trim();
                    string inputDensity = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 56).Trim();

                    ElleList.Add(Convert.ToDouble(inputElle));
                    AiList.Add(Convert.ToDouble(inputAi));
                    BeList.Add(Convert.ToDouble(inputBe));
                    DensityList.Add(Convert.ToDouble(inputDensity));

                    HeadThreeList.Add(Convert.ToDouble(inputDensity));
                    ElementList.Add(EL);
                }
                double DensitytoShow = Math.Round(HeadThreeList.Average(), 2);
                _headSortedStrategy.SetHeadStrategy(new YellowHeadStrategy(_headValueProcessor));
                _headSortedStrategy.GetList(HeadThreeList);
                _headSortedStrategy.SetCase(i);
                _headSortedStrategy.OrderHeadByColor();
            }

            _yellowValue.YellowHead3Segment1Value = _headValueProcessor.GetYellowHead3Segment1Value();
            _yellowValue.YellowHead3Segment2Value = _headValueProcessor.GetYellowHead3Segment2Value();
            _yellowValue.YellowHead3Segment3Value = _headValueProcessor.GetYellowHead3Segment3Value();
            _yellowValue.YellowHead3Segment4Value = _headValueProcessor.GetYellowHead3Segment4Value();
            _yellowValue.YellowHead2Segment1Value = _headValueProcessor.GetYellowHead2Segment1Value();
            _yellowValue.YellowHead2Segment2Value = _headValueProcessor.GetYellowHead2Segment2Value();
            _yellowValue.YellowHead2Segment3Value = _headValueProcessor.GetYellowHead2Segment3Value();
            _yellowValue.YellowHead2Segment4Value = _headValueProcessor.GetYellowHead2Segment4Value();
            _yellowValue.YellowHead1Segment1Value = _headValueProcessor.GetYellowHead1Segment1Value();
            _yellowValue.YellowHead1Segment2Value = _headValueProcessor.GetYellowHead1Segment2Value();
            _yellowValue.YellowHead1Segment3Value = _headValueProcessor.GetYellowHead1Segment3Value();
            _yellowValue.YellowHead1Segment4Value = _headValueProcessor.GetYellowHead1Segment4Value();

            _yellowValue.Result1LValue = Math.Round(ElleList.Average(), 2).ToString();
            _yellowValue.Result1AValue = Math.Round(AiList.Average(), 2).ToString();
            _yellowValue.Result1BValue = Math.Round(BeList.Average(), 2).ToString();
            _yellowValue.Result1DensityValue = Math.Round(DensityList.Average(), 2).ToString();

            ClearList();

            for (int i = 0; i < maxcounter; i++)
            {
                foreach (var l in Yellow2)
                {
                    string inputElle = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 2).Trim();
                    string inputAi = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 11).Trim();
                    string inputBe = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 20).Trim();
                    string inputDensity = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 56).Trim();

                    ElleList.Add(Convert.ToDouble(inputElle));
                    AiList.Add(Convert.ToDouble(inputAi));
                    BeList.Add(Convert.ToDouble(inputBe));
                    DensityList.Add(Convert.ToDouble(inputDensity));
                    HeadThreeList.Add(Convert.ToDouble(inputDensity));
                    ElementList.Add(EL);
                }
                double DensitytoShow = Math.Round(HeadThreeList.Average(), 2);
                _headSortedStrategy.SetHeadStrategy(new YellowHeadStrategy(_headValueProcessor));
                _headSortedStrategy.GetList(HeadThreeList);
                _headSortedStrategy.SetCase(i);
                _headSortedStrategy.OrderHeadByColor();

            }

            _yellowValue.yellow2Head3Segment1Value = _headValueProcessor.GetYellowHead3Segment1Value();
            _yellowValue.yellow2Head3Segment2Value = _headValueProcessor.GetYellowHead3Segment2Value();
            _yellowValue.yellow2Head3Segment3Value = _headValueProcessor.GetYellowHead3Segment3Value();
            _yellowValue.yellow2Head3Segment4Value = _headValueProcessor.GetYellowHead3Segment4Value();
            _yellowValue.yellow2Head2Segment1Value = _headValueProcessor.GetYellowHead2Segment1Value();
            _yellowValue.yellow2Head2Segment2Value = _headValueProcessor.GetYellowHead2Segment2Value();
            _yellowValue.yellow2Head2Segment3Value = _headValueProcessor.GetYellowHead2Segment3Value();
            _yellowValue.yellow2Head2Segment4Value = _headValueProcessor.GetYellowHead2Segment4Value();
            _yellowValue.yellow2Head1Segment1Value = _headValueProcessor.GetYellowHead1Segment1Value();
            _yellowValue.yellow2Head1Segment2Value = _headValueProcessor.GetYellowHead1Segment2Value();
            _yellowValue.yellow2Head1Segment3Value = _headValueProcessor.GetYellowHead1Segment3Value();
            _yellowValue.yellow2Head1Segment4Value = _headValueProcessor.GetYellowHead1Segment4Value();

            _yellowValue.Result2LValue = Math.Round(ElleList.Average(), 2).ToString();
            _yellowValue.Result2AValue = Math.Round(AiList.Average(), 2).ToString();
            _yellowValue.Result2BValue = Math.Round(BeList.Average(), 2).ToString();
            _yellowValue.Result2DensityValue = Math.Round(DensityList.Average(), 2).ToString();

             ClearList();
            /////////////////////
            //Magenta
            /////////////////////
            for (int i = 0; i < maxcounter; i++)
            {
                foreach (var l in Magenta1)
                {
                    string inputElle = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 2).Trim();
                    string inputAi = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 11).Trim();
                    string inputBe = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 20).Trim();
                    string inputDensity = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 47).Trim();

                    ElleList.Add(Convert.ToDouble(inputElle));
                    AiList.Add(Convert.ToDouble(inputAi));
                    BeList.Add(Convert.ToDouble(inputBe));
                    DensityList.Add(Convert.ToDouble(inputDensity));


                    HeadThreeList.Add(Convert.ToDouble(inputDensity));
                    ElementList.Add(EL);
                }
                double DensitytoShow = Math.Round(HeadThreeList.Average(), 2);
                _headSortedStrategy.SetHeadStrategy(new MagentaHeadStrategy(_headValueProcessor));
                _headSortedStrategy.GetList(HeadThreeList);
                _headSortedStrategy.SetCase(i);
                _headSortedStrategy.OrderHeadByColor();
            }

            _magentaValue.MagentaHead3Segment1Value = _headValueProcessor.GetMagentaHead3Segment1Value();
            _magentaValue.MagentaHead3Segment2Value = _headValueProcessor.GetMagentaHead3Segment2Value();
            _magentaValue.MagentaHead3Segment3Value = _headValueProcessor.GetMagentaHead3Segment3Value();
            _magentaValue.MagentaHead3Segment4Value = _headValueProcessor.GetMagentaHead3Segment4Value();
            _magentaValue.MagentaHead2Segment1Value = _headValueProcessor.GetMagentaHead2Segment1Value();
            _magentaValue.MagentaHead2Segment2Value = _headValueProcessor.GetMagentaHead2Segment2Value();
            _magentaValue.MagentaHead2Segment3Value = _headValueProcessor.GetMagentaHead2Segment3Value();
            _magentaValue.MagentaHead2Segment4Value = _headValueProcessor.GetMagentaHead2Segment4Value();
            _magentaValue.MagentaHead1Segment1Value = _headValueProcessor.GetMagentaHead1Segment1Value();
            _magentaValue.MagentaHead1Segment2Value = _headValueProcessor.GetMagentaHead1Segment2Value();
            _magentaValue.MagentaHead1Segment3Value = _headValueProcessor.GetMagentaHead1Segment3Value();
            _magentaValue.MagentaHead1Segment4Value = _headValueProcessor.GetMagentaHead1Segment4Value();

            _magentaValue.Result1LValue = Math.Round(ElleList.Average(), 2).ToString();
            _magentaValue.Result1AValue = Math.Round(AiList.Average(), 2).ToString();
            _magentaValue.Result1BValue = Math.Round(BeList.Average(), 2).ToString();
            _magentaValue.Result1DensityValue = Math.Round(DensityList.Average(), 2).ToString();

            ClearList();

            for (int i = 0; i < maxcounter; i++)
            {
                foreach (var l in Magenta2)
                {

                    string inputElle = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 2).Trim();
                    string inputAi = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 11).Trim();
                    string inputBe = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 20).Trim();
                    string inputDensity = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 47).Trim();

                    ElleList.Add(Convert.ToDouble(inputElle));
                    AiList.Add(Convert.ToDouble(inputAi));
                    BeList.Add(Convert.ToDouble(inputBe));
                    DensityList.Add(Convert.ToDouble(inputDensity));

                    HeadThreeList.Add(Convert.ToDouble(inputDensity));
                    ElementList.Add(EL);
                }
                double DensitytoShow = Math.Round(HeadThreeList.Average(), 2);
                _headSortedStrategy.SetHeadStrategy(new MagentaHeadStrategy(_headValueProcessor));
                _headSortedStrategy.GetList(HeadThreeList);
                _headSortedStrategy.SetCase(i);
                _headSortedStrategy.OrderHeadByColor();
            }

            _magentaValue.magenta2Head3Segment1Value = _headValueProcessor.GetMagentaHead3Segment1Value();
            _magentaValue.magenta2Head3Segment2Value = _headValueProcessor.GetMagentaHead3Segment2Value();
            _magentaValue.magenta2Head3Segment3Value = _headValueProcessor.GetMagentaHead3Segment3Value();
            _magentaValue.magenta2Head3Segment4Value = _headValueProcessor.GetMagentaHead3Segment4Value();
            _magentaValue.magenta2Head2Segment1Value = _headValueProcessor.GetMagentaHead2Segment1Value();
            _magentaValue.magenta2Head2Segment2Value = _headValueProcessor.GetMagentaHead2Segment2Value();
            _magentaValue.magenta2Head2Segment3Value = _headValueProcessor.GetMagentaHead2Segment3Value();
            _magentaValue.magenta2Head2Segment4Value = _headValueProcessor.GetMagentaHead2Segment4Value();
            _magentaValue.magenta2Head1Segment1Value = _headValueProcessor.GetMagentaHead1Segment1Value();
            _magentaValue.magenta2Head1Segment2Value = _headValueProcessor.GetMagentaHead1Segment2Value();
            _magentaValue.magenta2Head1Segment3Value = _headValueProcessor.GetMagentaHead1Segment3Value();
            _magentaValue.magenta2Head1Segment4Value = _headValueProcessor.GetMagentaHead1Segment4Value();

            _magentaValue.Result2LValue = Math.Round(ElleList.Average(), 2).ToString();
            _magentaValue.Result2AValue = Math.Round(AiList.Average(), 2).ToString();
            _magentaValue.Result2BValue = Math.Round(BeList.Average(), 2).ToString();
            _magentaValue.Result2DensityValue = Math.Round(DensityList.Average(), 2).ToString();

            ClearList();
            /////////////////////
            //Cyan
            /////////////////////
            for (int i = 0; i < maxcounter; i++)
            {
                foreach (var l in Cyan1)
                {


                    string inputElle = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 2).Trim();
                    string inputAi = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 11).Trim();
                    string inputBe = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 20).Trim();
                    string inputDensity = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 38).Trim();

                    ElleList.Add(Convert.ToDouble(inputElle));
                    AiList.Add(Convert.ToDouble(inputAi));
                    BeList.Add(Convert.ToDouble(inputBe));
                    DensityList.Add(Convert.ToDouble(inputDensity));

                    HeadThreeList.Add(Convert.ToDouble(inputDensity));
                    ElementList.Add(EL);
                }
                double DensitytoShow = Math.Round(HeadThreeList.Average(), 2);
                _headSortedStrategy.SetHeadStrategy(new CyanHeadStrategy(_headValueProcessor));
                _headSortedStrategy.GetList(HeadThreeList);
                _headSortedStrategy.SetCase(i);
                _headSortedStrategy.OrderHeadByColor();
            }

            _cyanValue.CyanHead3Segment1Value = _headValueProcessor.GetCyanHead3Segment1Value();
            _cyanValue.CyanHead3Segment2Value = _headValueProcessor.GetCyanHead3Segment2Value();
            _cyanValue.CyanHead3Segment3Value = _headValueProcessor.GetCyanHead3Segment3Value();
            _cyanValue.CyanHead3Segment4Value = _headValueProcessor.GetCyanHead3Segment4Value();
            _cyanValue.CyanHead2Segment1Value = _headValueProcessor.GetCyanHead2Segment1Value();
            _cyanValue.CyanHead2Segment2Value = _headValueProcessor.GetCyanHead2Segment2Value();
            _cyanValue.CyanHead2Segment3Value = _headValueProcessor.GetCyanHead2Segment3Value();
            _cyanValue.CyanHead2Segment4Value = _headValueProcessor.GetCyanHead2Segment4Value();
            _cyanValue.CyanHead1Segment1Value = _headValueProcessor.GetCyanHead1Segment1Value();
            _cyanValue.CyanHead1Segment2Value = _headValueProcessor.GetCyanHead1Segment2Value();
            _cyanValue.CyanHead1Segment3Value = _headValueProcessor.GetCyanHead1Segment3Value();
            _cyanValue.CyanHead1Segment4Value = _headValueProcessor.GetCyanHead1Segment4Value();

            _cyanValue.Result1LValue = Math.Round(ElleList.Average(), 2).ToString();
            _cyanValue.Result1AValue = Math.Round(AiList.Average(), 2).ToString();
            _cyanValue.Result1BValue = Math.Round(BeList.Average(), 2).ToString();
            _cyanValue.Result1DensityValue = Math.Round(DensityList.Average(), 2).ToString();

            ClearList();

            for (int i = 0; i < maxcounter; i++)
            {
                foreach (var l in Cyan2)
                {
                    string inputElle = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 2).Trim();
                    string inputAi = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 11).Trim();
                    string inputBe = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 20).Trim();
                    string inputDensity = _extractor.ExtractValueFromFile(filePath, ((i * 72) + l + 23), 38).Trim();

                    ElleList.Add(Convert.ToDouble(inputElle));
                    AiList.Add(Convert.ToDouble(inputAi));
                    BeList.Add(Convert.ToDouble(inputBe));
                    DensityList.Add(Convert.ToDouble(inputDensity));

                    HeadThreeList.Add(Convert.ToDouble(inputDensity));
                    ElementList.Add(EL);
                }
                double DensitytoShow = Math.Round(HeadThreeList.Average(), 2);
                _headSortedStrategy.SetHeadStrategy(new CyanHeadStrategy(_headValueProcessor));
                _headSortedStrategy.GetList(HeadThreeList);
                _headSortedStrategy.SetCase(i);
                _headSortedStrategy.OrderHeadByColor();
            }

            _cyanValue.cyan2Head3Segment1Value = _headValueProcessor.GetCyanHead3Segment1Value();
            _cyanValue.cyan2Head3Segment2Value = _headValueProcessor.GetCyanHead3Segment2Value();
            _cyanValue.cyan2Head3Segment3Value = _headValueProcessor.GetCyanHead3Segment3Value();
            _cyanValue.cyan2Head3Segment4Value = _headValueProcessor.GetCyanHead3Segment4Value();
            _cyanValue.cyan2Head2Segment1Value = _headValueProcessor.GetCyanHead2Segment1Value();
            _cyanValue.cyan2Head2Segment2Value = _headValueProcessor.GetCyanHead2Segment2Value();
            _cyanValue.cyan2Head2Segment3Value = _headValueProcessor.GetCyanHead2Segment3Value();
            _cyanValue.cyan2Head2Segment4Value = _headValueProcessor.GetCyanHead2Segment4Value();
            _cyanValue.cyan2Head1Segment1Value = _headValueProcessor.GetCyanHead1Segment1Value();
            _cyanValue.cyan2Head1Segment2Value = _headValueProcessor.GetCyanHead1Segment2Value();
            _cyanValue.cyan2Head1Segment3Value = _headValueProcessor.GetCyanHead1Segment3Value();
            _cyanValue.cyan2Head1Segment4Value = _headValueProcessor.GetCyanHead1Segment4Value();

            _cyanValue.Result2LValue = Math.Round(ElleList.Average(), 2).ToString();
            _cyanValue.Result2AValue = Math.Round(AiList.Average(), 2).ToString();
            _cyanValue.Result2BValue = Math.Round(BeList.Average(), 2).ToString();
            _cyanValue.Result2DensityValue = Math.Round(DensityList.Average(), 2).ToString();

            ClearList();
            if (_InputLogFile!=null)
            {
                if (_InputLogFile.Length != 0)
                {
                    ExcelWorket();
                }
            }
        }
        private void ClearList()
        {
            ElleList.Clear();
            AiList.Clear();
            BeList.Clear();
            DensityList.Clear();
        }


        public void ExcelWorket() 
        {
            var app = new Excel.Application();
            var workbook = app.Workbooks.Open(_InputLogFile);
            var sheet = workbook.Worksheets["FPGA Diagnostics"];
            var range = (Excel.Range)sheet.Columns["B:B"];
            string[] densityValues = new string[4];
            var cellValue = (string)(sheet.Cells[42, 2] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');

            _violetValue.TargetValue = 1.91;
            _orangeValue.TargetValue = 1.65;
            _blackValue.TargetValue = 1.91;
            _yellowValue.TargetValue = 1.71;
            _magentaValue.TargetValue = 1.38;
            _cyanValue.TargetValue = 1.70;


            _magentaValue.Magenta1CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue( double.Parse(densityValues[0]), _magentaValue.TargetValue);
            _magentaValue.Magenta2CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _magentaValue.TargetValue);
            _magentaValue.Magenta3CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _magentaValue.TargetValue);
            _magentaValue.Magenta4CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _magentaValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 3] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _magentaValue.Magenta1CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _magentaValue.TargetValue);
            _magentaValue.Magenta2CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _magentaValue.TargetValue);
            _magentaValue.Magenta3CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _magentaValue.TargetValue);
            _magentaValue.Magenta4CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _magentaValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 4] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _magentaValue.Magenta1CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _magentaValue.TargetValue);
            _magentaValue.Magenta2CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _magentaValue.TargetValue);
            _magentaValue.Magenta3CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _magentaValue.TargetValue);
            _magentaValue.Magenta4CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _magentaValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 5] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _violetValue.Violet1CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _violetValue.TargetValue);
            _violetValue.Violet2CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _violetValue.TargetValue);
            _violetValue.Violet3CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _violetValue.TargetValue);
            _violetValue.Violet4CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _violetValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 6] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _violetValue.Violet1CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _violetValue.TargetValue);
            _violetValue.Violet2CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _violetValue.TargetValue);
            _violetValue.Violet3CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _violetValue.TargetValue);
            _violetValue.Violet4CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _violetValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 7] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _violetValue.Violet1CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _violetValue.TargetValue);
            _violetValue.Violet2CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _violetValue.TargetValue);
            _violetValue.Violet3CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _violetValue.TargetValue);
            _violetValue.Violet4CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _violetValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 10] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _cyanValue.Cyan1CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _cyanValue.TargetValue);
            _cyanValue.Cyan2CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]),_cyanValue.TargetValue);
            _cyanValue.Cyan3CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]),_cyanValue.TargetValue);
            _cyanValue.Cyan4CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]),_cyanValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 11] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _cyanValue.Cyan1CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _cyanValue.TargetValue);
            _cyanValue.Cyan2CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _cyanValue.TargetValue);
            _cyanValue.Cyan3CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _cyanValue.TargetValue);
            _cyanValue.Cyan4CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _cyanValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 12] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _cyanValue.Cyan1CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _cyanValue.TargetValue);
            _cyanValue.Cyan2CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _cyanValue.TargetValue);
            _cyanValue.Cyan3CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _cyanValue.TargetValue);
            _cyanValue.Cyan4CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _cyanValue.TargetValue);


            //Black

            cellValue = (string)(sheet.Cells[42, 13] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _blackValue.Black1CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _blackValue.TargetValue);
            _blackValue.Black2CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _blackValue.TargetValue);
            _blackValue.Black3CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _blackValue.TargetValue);
            _blackValue.Black4CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _blackValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 14] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _blackValue.Black1CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _blackValue.TargetValue);
            _blackValue.Black2CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _blackValue.TargetValue);
            _blackValue.Black3CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _blackValue.TargetValue);
            _blackValue.Black4CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _blackValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 15] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _blackValue.Black1CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _blackValue.TargetValue);
            _blackValue.Black2CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _blackValue.TargetValue);
            _blackValue.Black3CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _blackValue.TargetValue);
            _blackValue.Black4CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _blackValue.TargetValue);

            //Yellow

            cellValue = (string)(sheet.Cells[42, 26] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _yellowValue.Yellow1CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _yellowValue.TargetValue);
            _yellowValue.Yellow2CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _yellowValue.TargetValue);
            _yellowValue.Yellow3CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _yellowValue.TargetValue);
            _yellowValue.Yellow4CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _yellowValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 27] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _yellowValue.Yellow1CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _yellowValue.TargetValue);
            _yellowValue.Yellow2CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _yellowValue.TargetValue);
            _yellowValue.Yellow3CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _yellowValue.TargetValue);
            _yellowValue.Yellow4CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _yellowValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 28] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _yellowValue.Yellow1CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _yellowValue.TargetValue);
            _yellowValue.Yellow2CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _yellowValue.TargetValue);
            _yellowValue.Yellow3CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _yellowValue.TargetValue);
            _yellowValue.Yellow4CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _yellowValue.TargetValue);

            //Orange

            cellValue = (string)(sheet.Cells[42, 29] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _orangeValue.Orange1CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _orangeValue.TargetValue);
            _orangeValue.Orange2CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _orangeValue.TargetValue);
            _orangeValue.Orange3CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _orangeValue.TargetValue);
            _orangeValue.Orange4CurrentHead1Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _orangeValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 30] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _orangeValue.Orange1CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _orangeValue.TargetValue);
            _orangeValue.Orange2CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _orangeValue.TargetValue);
            _orangeValue.Orange3CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _orangeValue.TargetValue);
            _orangeValue.Orange4CurrentHead2Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _orangeValue.TargetValue);

            cellValue = (string)(sheet.Cells[42, 31] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _orangeValue.Orange1CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[0]), _orangeValue.TargetValue);
            _orangeValue.Orange2CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[1]), _orangeValue.TargetValue);
            _orangeValue.Orange3CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[2]), _orangeValue.TargetValue);
            _orangeValue.Orange4CurrentHead3Voltages = _suggestedValuesCalculator.SuggestedValue(double.Parse(densityValues[3]), _orangeValue.TargetValue);

            
            // var result = range.Find("apples", LookAt: Excel.XlLookAt.xlWhole);
            // var address = result.Address;//cell address
            //  var value = result.Value2;//cell value
        }















        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
