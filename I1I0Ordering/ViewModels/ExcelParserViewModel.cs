﻿using I1I0Ordering.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace I1I0Ordering.ViewModels
{
   public class ExcelParserViewModel
    {
        public VioletValue _violetValue { get; set; }
        public OrangeValue _orangeValue { get; set; }
        public BlackValue _blackValue { get; set; }
        public YellowValue _yellowValue { get; set; }
        public MagentaValue _magentaValue { get; set; }
        public CyanValue _cyanValue { get; set; }
        public ExcelParserViewModel()
        {
            _violetValue = new VioletValue();
            //    _headViolet = new HeadViolet();
            _orangeValue = new OrangeValue();
            _blackValue = new BlackValue();
            _yellowValue = new YellowValue();
            _magentaValue = new MagentaValue();
            _cyanValue = new CyanValue();
        }

        public void ExcelWorket()
        {
            var app = new Excel.Application();
            var workbook = app.Workbooks.Open("C:\\Users\\I_Leotta\\Documents\\TestWorkSheet.xls");
            var sheet = workbook.Worksheets["FPGA Diagnostics"];
            var range = (Excel.Range)sheet.Columns["B:B"];
            string[] densityValues = new string[4];
            var cellValue = (string)(sheet.Cells[42, 2] as Excel.Range).Value;
             densityValues = cellValue.Split(' ');
            _magentaValue.Magenta1CurrentHead1Voltages = double.Parse(densityValues[0]);
            _magentaValue.Magenta2CurrentHead1Voltages = double.Parse(densityValues[1]);
            _magentaValue.Magenta3CurrentHead1Voltages = double.Parse(densityValues[2]);
            _magentaValue.Magenta4CurrentHead1Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 3] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _magentaValue.Magenta1CurrentHead2Voltages = double.Parse(densityValues[0]);
            _magentaValue.Magenta2CurrentHead2Voltages = double.Parse(densityValues[1]);
            _magentaValue.Magenta3CurrentHead2Voltages = double.Parse(densityValues[2]);
            _magentaValue.Magenta4CurrentHead2Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 4] as Excel.Range).Value;
           densityValues = cellValue.Split(' ');
            _magentaValue.Magenta1CurrentHead3Voltages = double.Parse(densityValues[0]);
            _magentaValue.Magenta2CurrentHead3Voltages = double.Parse(densityValues[1]);
            _magentaValue.Magenta3CurrentHead3Voltages = double.Parse(densityValues[2]);
            _magentaValue.Magenta4CurrentHead3Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 5] as Excel.Range).Value;
             densityValues = cellValue.Split(' ');
            _violetValue.Violet1CurrentHead1Voltages = double.Parse(densityValues[0]);
            _violetValue.Violet2CurrentHead1Voltages = double.Parse(densityValues[1]);
            _violetValue.Violet3CurrentHead1Voltages = double.Parse(densityValues[2]);
            _violetValue.Violet4CurrentHead1Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 6] as Excel.Range).Value;
           densityValues = cellValue.Split(' ');
            _violetValue.Violet1CurrentHead2Voltages = double.Parse(densityValues[0]);
            _violetValue.Violet2CurrentHead2Voltages = double.Parse(densityValues[1]);
            _violetValue.Violet3CurrentHead2Voltages = double.Parse(densityValues[2]);
            _violetValue.Violet4CurrentHead2Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 7] as Excel.Range).Value;
             densityValues = cellValue.Split(' ');
            _violetValue.Violet1CurrentHead3Voltages = double.Parse(densityValues[0]);
            _violetValue.Violet2CurrentHead3Voltages = double.Parse(densityValues[1]);
            _violetValue.Violet3CurrentHead3Voltages = double.Parse(densityValues[2]);
            _violetValue.Violet4CurrentHead3Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 10] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _cyanValue.Cyan1CurrentHead1Voltages = double.Parse(densityValues[0]);
            _cyanValue.Cyan2CurrentHead1Voltages = double.Parse(densityValues[1]);
            _cyanValue.Cyan3CurrentHead1Voltages = double.Parse(densityValues[2]);
            _cyanValue.Cyan4CurrentHead1Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 11] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _cyanValue.Cyan1CurrentHead2Voltages = double.Parse(densityValues[0]);
            _cyanValue.Cyan2CurrentHead2Voltages = double.Parse(densityValues[1]);
            _cyanValue.Cyan3CurrentHead2Voltages = double.Parse(densityValues[2]);
            _cyanValue.Cyan4CurrentHead2Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 12] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _cyanValue.Cyan1CurrentHead3Voltages = double.Parse(densityValues[0]);
            _cyanValue.Cyan2CurrentHead3Voltages = double.Parse(densityValues[1]);
            _cyanValue.Cyan3CurrentHead3Voltages = double.Parse(densityValues[2]);
            _cyanValue.Cyan4CurrentHead3Voltages = double.Parse(densityValues[3]);


            //Black
                         
            cellValue = (string)(sheet.Cells[42, 13] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _blackValue.Black1CurrentHead1Voltages = double.Parse(densityValues[0]);
            _blackValue.Black2CurrentHead1Voltages = double.Parse(densityValues[1]);
            _blackValue.Black3CurrentHead1Voltages = double.Parse(densityValues[2]);
            _blackValue.Black4CurrentHead1Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 14] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _blackValue.Black1CurrentHead2Voltages = double.Parse(densityValues[0]);
            _blackValue.Black2CurrentHead2Voltages = double.Parse(densityValues[1]);
            _blackValue.Black3CurrentHead2Voltages = double.Parse(densityValues[2]);
            _blackValue.Black4CurrentHead2Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 15] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _blackValue.Black1CurrentHead3Voltages = double.Parse(densityValues[0]);
            _blackValue.Black2CurrentHead3Voltages = double.Parse(densityValues[1]);
            _blackValue.Black3CurrentHead3Voltages = double.Parse(densityValues[2]);
            _blackValue.Black4CurrentHead3Voltages = double.Parse(densityValues[3]);

            //Yellow

            cellValue = (string)(sheet.Cells[42, 26] as Excel.Range).Value;
             densityValues = cellValue.Split(' ');
            _yellowValue.Yellow1CurrentHead1Voltages = double.Parse(densityValues[0]);
            _yellowValue.Yellow2CurrentHead1Voltages = double.Parse(densityValues[1]);
            _yellowValue.Yellow3CurrentHead1Voltages = double.Parse(densityValues[2]);
            _yellowValue.Yellow4CurrentHead1Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 27] as Excel.Range).Value;
             densityValues = cellValue.Split(' ');
            _yellowValue.Yellow1CurrentHead2Voltages = double.Parse(densityValues[0]);
            _yellowValue.Yellow2CurrentHead2Voltages = double.Parse(densityValues[1]);
            _yellowValue.Yellow3CurrentHead2Voltages = double.Parse(densityValues[2]);
            _yellowValue.Yellow4CurrentHead2Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 28] as Excel.Range).Value;
           densityValues = cellValue.Split(' ');
            _yellowValue.Yellow1CurrentHead3Voltages = double.Parse(densityValues[0]);
            _yellowValue.Yellow2CurrentHead3Voltages = double.Parse(densityValues[1]);
            _yellowValue.Yellow3CurrentHead3Voltages = double.Parse(densityValues[2]);
            _yellowValue.Yellow4CurrentHead3Voltages = double.Parse(densityValues[3]);

            //Orange

            cellValue = (string)(sheet.Cells[42, 29] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _orangeValue.Orange1CurrentHead1Voltages = double.Parse(densityValues[0]);
            _orangeValue.Orange2CurrentHead1Voltages = double.Parse(densityValues[1]);
            _orangeValue.Orange3CurrentHead1Voltages = double.Parse(densityValues[2]);
            _orangeValue.Orange4CurrentHead1Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 30] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _orangeValue.Orange1CurrentHead2Voltages = double.Parse(densityValues[0]);
            _orangeValue.Orange2CurrentHead2Voltages = double.Parse(densityValues[1]);
            _orangeValue.Orange3CurrentHead2Voltages = double.Parse(densityValues[2]);
            _orangeValue.Orange4CurrentHead2Voltages = double.Parse(densityValues[3]);

            cellValue = (string)(sheet.Cells[42, 31] as Excel.Range).Value;
            densityValues = cellValue.Split(' ');
            _orangeValue.Orange1CurrentHead3Voltages = double.Parse(densityValues[0]);
            _orangeValue.Orange2CurrentHead3Voltages = double.Parse(densityValues[1]);
            _orangeValue.Orange3CurrentHead3Voltages = double.Parse(densityValues[2]);
            _orangeValue.Orange4CurrentHead3Voltages = double.Parse(densityValues[3]);

            // var result = range.Find("apples", LookAt: Excel.XlLookAt.xlWhole);
            // var address = result.Address;//cell address
            //  var value = result.Value2;//cell value
        }
    }
}
