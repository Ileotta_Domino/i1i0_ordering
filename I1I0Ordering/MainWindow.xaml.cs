﻿using I1I0Ordering.Helpers;
using I1I0Ordering.Model;
using I1I0Ordering.Model.Interface;
using I1I0Ordering.Parameters;
using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace I1I0Ordering
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IFileProcessor _fileProcessor;
        private readonly IInputListValue _inputListValue = new InputListValue();
        public MainWindow()
        {
           
            InitializeComponent();
            
            TextBox1.Drop += TextBox1_Drop;
            TextBox1.PreviewDragOver += TextBox1_PreviewDragOver;

            TextBox2.Drop += TextBox2_Drop;
            TextBox2.PreviewDragOver += TextBox2_PreviewDragOver;
            //     ListBox1.Drop += ListBox1_Drop;
            //     ListBox1.PreviewDragOver += ListBox1_PreviewDragOver;
            DataContext = this;
            this.MouseLeftButtonDown += delegate { this.DragMove(); };
            DataContextChanged += new DependencyPropertyChangedEventHandler(MainWindow_DataContextChanged);
        }



        void MainWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dc = DataContext as IClosableViewModel;
            dc.CloseWindowEvent += new EventHandler(dc_CloseWindowEvent);
        }
        void dc_CloseWindowEvent(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Circle_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void ImagePanel_Drop(object sender, DragEventArgs e)
        {

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // Note that you can have more than one file.
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                // Assuming you have one file that you care about, pass it off to whatever
                // handling code you have defined.
                foreach (string file in files) { FileKeeper.BlankFileLocation = file;  };
            }
        }

        private void TextBox1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
        }
        private void TextBox2_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void TextBox2_DragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
        }
        //private void TextBox1_Drop(object sender, DragEventArgs e)
        //{
        //    if (e.Data.GetDataPresent(DataFormats.FileDrop))
        //    {
        //        // Note that you can have more than one file.
        //        string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

        //        // Assuming you have one file that you care about, pass it off to whatever
        //        // handling code you have defined.
        //        foreach (string file in files) { FileKeeper.BlankFileLocation = file; };
        //    }
        //}

        private void TextBox1_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Handled = true;
        }

        private void TextBox1_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // Note that you can have more than one file.
              //  string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
               // var file = files[0];
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    // Note that you can have more than one file.
                    string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                    // Assuming you have one file that you care about, pass it off to whatever
                    // handling code you have defined.
                    foreach (string file in files) {
                        _inputListValue.InputListAdd(file);
                        FileKeeper.BlankFileLocation = file; };
                }
            }
        }

        private void TextBox2_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // Note that you can have more than one file.
                //  string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                // var file = files[0];
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    // Note that you can have more than one file.
                    string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                    // Assuming you have one file that you care about, pass it off to whatever
                    // handling code you have defined.
                    foreach (string file in files)
                    {
                        _inputListValue.InputListAdd(file);
                        FileKeeper.BlankFileLocation = file;
                    };
                }
            }
        }

        private void TextBox2_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Handled = true;
        }

        private void TextBox1_PreviewDrop(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
                                                                                 //   TextBox1.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            TextBox1.Text = string.Join("", fileloadup); 
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }
        private void TextBox2_PreviewDrop(object sender, DragEventArgs e)
        {
            string[] fileloadup = (string[])e.Data.GetData(DataFormats.FileDrop);//Get the filename including path
                                                                                 //   TextBox1.Text = File.ReadAllText(fileloadup[0]);//Load data to textBox2
            TextBox1.Text = string.Join("", fileloadup);
            e.Handled = true;//This is file being dropped not copied text so we handle it
        }

        
        private void textBox1_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.All;
            e.Handled = true;
        }





        private void ListBox1_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Handled = true;
        }

        private void ListBox1_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // Note that you can have more than one file.
                //  string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                // var file = files[0];
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    // Note that you can have more than one file.
                    string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                    // Assuming you have one file that you care about, pass it off to whatever
                    // handling code you have defined.
                    foreach (string file in files) { FileKeeper.BlankFileLocation = file;
                        _inputListValue.InputListAdd(file);};
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
            Close();
        }
    }
}
