﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class HeadYellow : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string _YellowHead3Segment1Value;
        double _YellowHead3Segment3Value;

        public HeadYellow()
        { }
        public string YellowHead3Segment1Value
        {
            get
            {
                return _YellowHead3Segment1Value;
            }
            set
            {
                if (_YellowHead3Segment1Value != value)
                {
                    _YellowHead3Segment1Value = value;
                    OnPropertyChanged("YellowHead3Segment1Value");
                    try
                    {
                        //  _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("YellowHead3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double YellowHead3Segment3Value
        {
            get
            {
                return _YellowHead3Segment3Value;
            }
            set
            {
                if (_YellowHead3Segment3Value != value)
                {
                    _YellowHead3Segment3Value = value;
                    OnPropertyChanged("YellowHead3Segment3Value");
                    try
                    {
                        //  _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("YellowHead3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
