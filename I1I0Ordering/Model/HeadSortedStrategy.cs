﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    class HeadSortedStrategy
    {
        private HeadStrategy _sortstrategy;
        List<double> _HeadList = new List<double>();
        int _case;

        public void GetList(List<double> inputList)
        {
            this._HeadList = inputList;
        }

        public void SetHeadStrategy(HeadStrategy sortstrategy)
        {
            this._sortstrategy = sortstrategy;
        }
        public void SetCase(int casenumb)
        {
            this._case = casenumb;
        }
        public void OrderHeadByColor()
        {
            _sortstrategy.OrderHeadByColour(_case, _HeadList);
        }

    }
}
