﻿using I1I0Ordering.ViewModels.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.ViewModels
{
   public  class FileHandler: IFileHandler
    {
        public void CopyFile()
        {

            try
            {
                File.Copy("file-missing.txt", "file-new.txt");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // System.IO.FileNotFoundException: Could not find file 'file-missing.txt'.
            }
        }
    }
}
