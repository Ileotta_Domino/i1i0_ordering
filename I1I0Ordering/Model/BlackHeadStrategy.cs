﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    class BlackHeadStrategy : HeadStrategy
    {
        double DensitytoShow;
        BlackValue _blackValue { get; set; }
        public HeadBlack _headBlack { get; set; }

        private readonly IHeadValueProcessor _headValueProcessor;
        public BlackHeadStrategy(IHeadValueProcessor headValueProcessor)
        {
            _headBlack = new HeadBlack();
            _headValueProcessor = headValueProcessor;
        }
        public override void OrderHeadByColour(int input, List<double> inputList)
        {
            _blackValue = new BlackValue();

            DensitytoShow = Math.Round(inputList.Average(), 2);

            switch (input + 1)
            {
                case 1:
                    _headValueProcessor.SetBlackHead3Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 2:
                    _headValueProcessor.SetBlackHead3Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 3:
                    _headValueProcessor.SetBlackHead3Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 4:
                    _headValueProcessor.SetBlackHead3Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 5:
                    _headValueProcessor.SetBlackHead2Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 6:
                    _headValueProcessor.SetBlackHead2Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 7:
                    _headValueProcessor.SetBlackHead2Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 8:
                    _headValueProcessor.SetBlackHead2Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 9:
                    _headValueProcessor.SetBlackHead1Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 10:
                    _headValueProcessor.SetBlackHead1Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 11:
                    _headValueProcessor.SetBlackHead1Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 12:
                    _headValueProcessor.SetBlackHead1Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
            }
        }
    }
}
