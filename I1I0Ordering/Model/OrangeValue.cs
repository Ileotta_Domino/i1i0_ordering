﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
   public  class OrangeValue : INotifyPropertyChanged
    { 
 public event PropertyChangedEventHandler PropertyChanged;

    string _Orange1L;
    string _Orange1A;
    string _Orange1B;
    string _Orange1D;
    string _Orange2L;
    string _Orange2A;
    string _Orange2B;
    string _Orange2D;


        double _OrangeHead3Segment1Value;
        double _OrangeHead3Segment2Value;
        double _OrangeHead3Segment3Value;
        double _OrangeHead3Segment4Value;
        double _OrangeHead2Segment1Value;
        double _OrangeHead2Segment2Value;
        double _OrangeHead2Segment3Value;
        double _OrangeHead2Segment4Value;
        double _OrangeHead1Segment1Value;
        double _OrangeHead1Segment2Value;
        double _OrangeHead1Segment3Value;
        double _OrangeHead1Segment4Value;

        double _Orange2Head3Segment1Value;
        double _Orange2Head3Segment2Value;
        double _Orange2Head3Segment3Value;
        double _Orange2Head3Segment4Value;
        double _Orange2Head2Segment1Value;
        double _Orange2Head2Segment2Value;
        double _Orange2Head2Segment3Value;
        double _Orange2Head2Segment4Value;
        double _Orange2Head1Segment1Value;
        double _Orange2Head1Segment2Value;
        double _Orange2Head1Segment3Value;
        double _Orange2Head1Segment4Value;

        double _Orange1CurrentHead1Voltages;
        double _Orange2CurrentHead1Voltages;
        double _Orange3CurrentHead1Voltages;
        double _Orange4CurrentHead1Voltages;
        double _Orange1CurrentHead2Voltages;
        double _Orange2CurrentHead2Voltages;
        double _Orange3CurrentHead2Voltages;
        double _Orange4CurrentHead2Voltages;
        double _Orange1CurrentHead3Voltages;
        double _Orange2CurrentHead3Voltages;
        double _Orange3CurrentHead3Voltages;
        double _Orange4CurrentHead3Voltages;

        double _TargetValue;
        public OrangeValue()
    {
    }

    public string Result1LValue
    {
        get
        {
            return _Orange1L;
        }
        set
        {
            if (_Orange1L != value)
            {
                _Orange1L = value;
                try
                {
                    OnPropertyChanged("Result1LValue");
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            }
        }
    }

    public string Result1AValue
    {
        get
        {
            return _Orange1A;
        }
        set
        {
            if (_Orange1A != value)
            {
                _Orange1A = value;
                try
                {
                    OnPropertyChanged("Result1AValue");
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            }
        }
    }

    public string Result1BValue
    {
        get
        {
            return _Orange1B;
        }
        set
        {
            if (_Orange1B != value)
            {
                _Orange1B = value;
                try
                {
                    OnPropertyChanged("Result1BValue");
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            }
        }
    }

    public string Result1DensityValue
    {
        get
        {
            return _Orange1D;
        }
        set
        {
            if (_Orange1D != value)
            {
                _Orange1D = value;
                try
                {
                    OnPropertyChanged("Result1DensityValue");
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            }
        }
    }

    public string Result2LValue
    {
        get
        {
            return _Orange2L;
        }
        set
        {
            if (_Orange2L != value)
            {
                _Orange2L = value;
                try
                {
                    OnPropertyChanged("Result2LValue");
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            }
        }
    }

    public string Result2AValue
    {
        get
        {
            return _Orange2A;
        }
        set
        {
            if (_Orange2A != value)
            {
                _Orange2A = value;
                try
                {
                    OnPropertyChanged("Result2AValue");
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            }
        }
    }

    public string Result2BValue
    {
        get
        {
            return _Orange2B;
        }
        set
        {
            if (_Orange2B != value)
            {
                _Orange2B = value;
                try
                    { 
                    OnPropertyChanged("Result2BValue");
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            }
        }
    }

    public string Result2DensityValue
    {
        get
        {
            return _Orange2D;
        }
        set
        {
            if (_Orange2D != value)
            {
                _Orange2D = value;
                try
                {
                    OnPropertyChanged("Result2DensityValue");
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            }
        }
    }


        public double OrangeHead3Segment1Value
        {
            get
            {
                return _OrangeHead3Segment1Value;
            }
            set
            {
                if (_OrangeHead3Segment1Value != value)
                {
                    _OrangeHead3Segment1Value = value;
                    try
                    {
                        //  _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double OrangeHead3Segment2Value
        {
            get
            {
                return _OrangeHead3Segment2Value;
            }
            set
            {
                if (_OrangeHead3Segment2Value != value)
                {
                    _OrangeHead3Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead3Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double OrangeHead3Segment3Value
        {
            get
            {
                return _OrangeHead3Segment3Value;
            }
            set
            {
                if (_OrangeHead3Segment3Value != value)
                {
                    _OrangeHead3Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double OrangeHead3Segment4Value
        {
            get
            {
                return _OrangeHead3Segment4Value;
            }
            set
            {
                if (_OrangeHead3Segment4Value != value)
                {
                    _OrangeHead3Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead3Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double OrangeHead2Segment1Value
        {
            get
            {
                return _OrangeHead2Segment1Value;
            }
            set
            {
                if (_OrangeHead2Segment1Value != value)
                {
                    _OrangeHead2Segment1Value = value;
                    OnPropertyChanged("OrangeHead2Segment1Value");
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead2Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double OrangeHead2Segment2Value
        {
            get
            {
                return _OrangeHead2Segment2Value;
            }
            set
            {
                if (_OrangeHead2Segment2Value != value)
                {
                    _OrangeHead2Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead2Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double OrangeHead2Segment3Value
        {
            get
            {
                return _OrangeHead2Segment3Value;
            }
            set
            {
                if (_OrangeHead2Segment3Value != value)
                {
                    _OrangeHead2Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead2Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double OrangeHead2Segment4Value
        {
            get
            {
                return _OrangeHead2Segment4Value;
            }
            set
            {
                if (_OrangeHead2Segment4Value != value)
                {
                    _OrangeHead2Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead2Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double OrangeHead1Segment1Value
        {
            get
            {
                return _OrangeHead1Segment1Value;
            }
            set
            {
                if (_OrangeHead1Segment1Value != value)
                {
                    _OrangeHead1Segment1Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead1Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double OrangeHead1Segment2Value
        {
            get
            {
                return _OrangeHead1Segment2Value;
            }
            set
            {
                if (_OrangeHead1Segment2Value != value)
                {
                    _OrangeHead1Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead1Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double OrangeHead1Segment3Value
        {
            get
            {
                return _OrangeHead1Segment3Value;
            }
            set
            {
                if (_OrangeHead1Segment3Value != value)
                {
                    _OrangeHead1Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead1Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double OrangeHead1Segment4Value
        {
            get
            {
                return _OrangeHead1Segment4Value;
            }
            set
            {
                if (_OrangeHead1Segment4Value != value)
                {
                    _OrangeHead1Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead1Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }


        public double Orange2Head3Segment1Value
        {
            get
            {
                return _Orange2Head3Segment1Value;
            }
            set
            {
                if (_Orange2Head3Segment1Value != value)
                {
                    _Orange2Head3Segment1Value = value;
                    try
                    {
                        //  _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2Head3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange2Head3Segment2Value
        {
            get
            {
                return _Orange2Head3Segment2Value;
            }
            set
            {
                if (_Orange2Head3Segment2Value != value)
                {
                    _Orange2Head3Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2Head3Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange2Head3Segment3Value
        {
            get
            {
                return _Orange2Head3Segment3Value;
            }
            set
            {
                if (_Orange2Head3Segment3Value != value)
                {
                    _Orange2Head3Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2Head3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange2Head3Segment4Value
        {
            get
            {
                return _Orange2Head3Segment4Value;
            }
            set
            {
                if (_Orange2Head3Segment4Value != value)
                {
                    _Orange2Head3Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2Head3Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange2Head2Segment1Value
        {
            get
            {
                return _Orange2Head2Segment1Value;
            }
            set
            {
                if (_Orange2Head2Segment1Value != value)
                {
                    _Orange2Head2Segment1Value = value;
                    OnPropertyChanged("Orange2Head2Segment1Value");
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2Head2Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Orange2Head2Segment2Value
        {
            get
            {
                return _Orange2Head2Segment2Value;
            }
            set
            {
                if (_Orange2Head2Segment2Value != value)
                {
                    _Orange2Head2Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2Head2Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange2Head2Segment3Value
        {
            get
            {
                return _Orange2Head2Segment3Value;
            }
            set
            {
                if (_Orange2Head2Segment3Value != value)
                {
                    _Orange2Head2Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2Head2Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange2Head2Segment4Value
        {
            get
            {
                return _Orange2Head2Segment4Value;
            }
            set
            {
                if (_Orange2Head2Segment4Value != value)
                {
                    _Orange2Head2Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2Head2Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange2Head1Segment1Value
        {
            get
            {
                return _Orange2Head1Segment1Value;
            }
            set
            {
                if (_Orange2Head1Segment1Value != value)
                {
                    _Orange2Head1Segment1Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2Head1Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Orange2Head1Segment2Value
        {
            get
            {
                return _Orange2Head1Segment2Value;
            }
            set
            {
                if (_Orange2Head1Segment2Value != value)
                {
                    _Orange2Head1Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2Head1Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange2Head1Segment3Value
        {
            get
            {
                return _Orange2Head1Segment3Value;
            }
            set
            {
                if (_Orange2Head1Segment3Value != value)
                {
                    _Orange2Head1Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2Head1Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange2Head1Segment4Value
        {
            get
            {
                return _Orange2Head1Segment4Value;
            }
            set
            {
                if (_Orange2Head1Segment4Value != value)
                {
                    _Orange2Head1Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2Head1Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Orange1CurrentHead1Voltages
        {
            get
            {
                return _Orange1CurrentHead1Voltages;
            }
            set
            {
                if (_Orange1CurrentHead1Voltages != value)
                {
                    _Orange1CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange1CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange2CurrentHead1Voltages
        {
            get
            {
                return _Orange2CurrentHead1Voltages;
            }
            set
            {
                if (_Orange2CurrentHead1Voltages != value)
                {
                    _Orange2CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange3CurrentHead1Voltages
        {
            get
            {
                return _Orange3CurrentHead1Voltages;
            }
            set
            {
                if (_Orange3CurrentHead1Voltages != value)
                {
                    _Orange3CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange3CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Orange4CurrentHead1Voltages
        {
            get
            {
                return _Orange4CurrentHead1Voltages;
            }
            set
            {
                if (_Orange4CurrentHead1Voltages != value)
                {
                    _Orange4CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange4CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Orange1CurrentHead2Voltages
        {
            get
            {
                return _Orange1CurrentHead2Voltages;
            }
            set
            {
                if (_Orange1CurrentHead2Voltages != value)
                {
                    _Orange1CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange1CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange2CurrentHead2Voltages
        {
            get
            {
                return _Orange2CurrentHead2Voltages;
            }
            set
            {
                if (_Orange2CurrentHead2Voltages != value)
                {
                    _Orange2CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange3CurrentHead2Voltages
        {
            get
            {
                return _Orange3CurrentHead2Voltages;
            }
            set
            {
                if (_Orange3CurrentHead2Voltages != value)
                {
                    _Orange3CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange3CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Orange4CurrentHead2Voltages
        {
            get
            {
                return _Orange4CurrentHead2Voltages;
            }
            set
            {
                if (_Orange4CurrentHead2Voltages != value)
                {
                    _Orange4CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange4CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Orange1CurrentHead3Voltages
        {
            get
            {
                return _Orange1CurrentHead3Voltages;
            }
            set
            {
                if (_Orange1CurrentHead3Voltages != value)
                {
                    _Orange1CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange1CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange2CurrentHead3Voltages
        {
            get
            {
                return _Orange2CurrentHead3Voltages;
            }
            set
            {
                if (_Orange2CurrentHead3Voltages != value)
                {
                    _Orange2CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange2CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Orange3CurrentHead3Voltages
        {
            get
            {
                return _Orange3CurrentHead3Voltages;
            }
            set
            {
                if (_Orange3CurrentHead3Voltages != value)
                {
                    _Orange3CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange3CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Orange4CurrentHead3Voltages
        {
            get
            {
                return _Orange4CurrentHead3Voltages;
            }
            set
            {
                if (_Orange4CurrentHead3Voltages != value)
                {
                    _Orange4CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("Orange4CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double TargetValue
        {
            get
            {
                return _TargetValue;
            }
            set
            {
                if (_TargetValue != value)
                {
                    _TargetValue = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("TargetValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public void OnPropertyChanged(string propertyName)
    {
        if (PropertyChanged != null)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
}

