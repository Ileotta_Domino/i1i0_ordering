﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    class OrangeHeadStrategy : HeadStrategy
    {
        double DensitytoShow;
        OrangeValue _orangeValue { get; set; }
        public HeadOrange _headOrange { get; set; }

        private readonly IHeadValueProcessor _headValueProcessor;
        public OrangeHeadStrategy(IHeadValueProcessor headValueProcessor)
        {
            _headOrange = new HeadOrange();
            _headValueProcessor = headValueProcessor;
        }
        public override void OrderHeadByColour(int input, List<double> inputList)
        {
            _orangeValue = new OrangeValue();

            DensitytoShow = Math.Round(inputList.Average(), 2);

            switch (input + 1)
            {
                case 1:
                    _headValueProcessor.SetOrangeHead3Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 2:
                    _headValueProcessor.SetOrangeHead3Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 3:
                    _headValueProcessor.SetOrangeHead3Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 4:
                    _headValueProcessor.SetOrangeHead3Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 5:
                    _headValueProcessor.SetOrangeHead2Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 6:
                    _headValueProcessor.SetOrangeHead2Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 7:
                    _headValueProcessor.SetOrangeHead2Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 8:
                    _headValueProcessor.SetOrangeHead2Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 9:
                    _headValueProcessor.SetOrangeHead1Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 10:
                    _headValueProcessor.SetOrangeHead1Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 11:
                    _headValueProcessor.SetOrangeHead1Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 12:
                    _headValueProcessor.SetOrangeHead1Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
            }
        }
    }
}
