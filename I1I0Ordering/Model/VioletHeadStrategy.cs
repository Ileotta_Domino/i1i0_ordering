﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
     class VioletHeadStrategy:  HeadStrategy
    {
        double DensitytoShow;
         VioletValue _violetValue { get; set; }
        public HeadViolet _headViolet { get; set; }

        private readonly IHeadValueProcessor _headValueProcessor;
        public VioletHeadStrategy(IHeadValueProcessor headValueProcessor)
        {
            _headViolet = new HeadViolet();
            _headValueProcessor = headValueProcessor;
        }
        public override void OrderHeadByColour(int input, List<double> inputList)
        {
           _violetValue = new VioletValue();

            DensitytoShow = Math.Round(inputList.Average(), 2);
            
            switch (input + 1)
            {
                case 1:
                    _headValueProcessor.SetVioletHead3Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 2:
                    _headValueProcessor.SetVioletHead3Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 3:
                    _headValueProcessor.SetVioletHead3Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 4:
                    _headValueProcessor.SetVioletHead3Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 5:
                    _headValueProcessor.SetVioletHead2Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 6:
                    _headValueProcessor.SetVioletHead2Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 7:
                    _headValueProcessor.SetVioletHead2Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 8:
                    _headValueProcessor.SetVioletHead2Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 9:
                    _headValueProcessor.SetVioletHead1Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 10:
                    _headValueProcessor.SetVioletHead1Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 11:
                    _headValueProcessor.SetVioletHead1Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 12:
                    _headValueProcessor.SetVioletHead1Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
            }
           
        }
    }
}
