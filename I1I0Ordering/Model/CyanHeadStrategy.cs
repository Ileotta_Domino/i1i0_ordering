﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    class CyanHeadStrategy : HeadStrategy
    {
        double DensitytoShow;
        CyanValue _cyanValue { get; set; }
        public HeadCyan _headCyan { get; set; }

        private readonly IHeadValueProcessor _headValueProcessor;
        public CyanHeadStrategy(IHeadValueProcessor headValueProcessor)
        {
            _headCyan = new HeadCyan();
            _headValueProcessor = headValueProcessor;
        }
        public override void OrderHeadByColour(int input, List<double> inputList)
        {
            _cyanValue = new CyanValue();

            DensitytoShow = Math.Round(inputList.Average(), 2);

            switch (input + 1)
            {
                case 1:
                    _headValueProcessor.SetCyanHead3Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 2:
                    _headValueProcessor.SetCyanHead3Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 3:
                    _headValueProcessor.SetCyanHead3Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 4:
                    _headValueProcessor.SetCyanHead3Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 5:
                    _headValueProcessor.SetCyanHead2Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 6:
                    _headValueProcessor.SetCyanHead2Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 7:
                    _headValueProcessor.SetCyanHead2Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 8:
                    _headValueProcessor.SetCyanHead2Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 9:
                    _headValueProcessor.SetCyanHead1Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 10:
                    _headValueProcessor.SetCyanHead1Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 11:
                    _headValueProcessor.SetCyanHead1Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 12:
                    _headValueProcessor.SetCyanHead1Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
            }
        }
    }
}
