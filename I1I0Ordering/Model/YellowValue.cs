﻿ using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class YellowValue : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string _Yellow1L;
        string _Yellow1A;
        string _Yellow1B;
        string _Yellow1D;
        string _Yellow2L;
        string _Yellow2A;
        string _Yellow2B;
        string _Yellow2D;

        double _yellowHead3Segment1Value;
        double _yellowHead3Segment2Value;
        double _yellowHead3Segment3Value;
        double _yellowHead3Segment4Value;
        double _yellowHead2Segment1Value;
        double _yellowHead2Segment2Value;
        double _yellowHead2Segment3Value;
        double _yellowHead2Segment4Value;
        double _yellowHead1Segment1Value;
        double _yellowHead1Segment2Value;
        double _yellowHead1Segment3Value;
        double _yellowHead1Segment4Value;

        double _yellow2Head3Segment1Value;
        double _yellow2Head3Segment2Value;
        double _yellow2Head3Segment3Value;
        double _yellow2Head3Segment4Value;
        double _yellow2Head2Segment1Value;
        double _yellow2Head2Segment2Value;
        double _yellow2Head2Segment3Value;
        double _yellow2Head2Segment4Value;
        double _yellow2Head1Segment1Value;
        double _yellow2Head1Segment2Value;
        double _yellow2Head1Segment3Value;
        double _yellow2Head1Segment4Value;

        double _Yellow1CurrentHead1Voltages;
        double _Yellow2CurrentHead1Voltages;
        double _Yellow3CurrentHead1Voltages;
        double _Yellow4CurrentHead1Voltages;
        double _Yellow1CurrentHead2Voltages;
        double _Yellow2CurrentHead2Voltages;
        double _Yellow3CurrentHead2Voltages;
        double _Yellow4CurrentHead2Voltages;
        double _Yellow1CurrentHead3Voltages;
        double _Yellow2CurrentHead3Voltages;
        double _Yellow3CurrentHead3Voltages;
        double _Yellow4CurrentHead3Voltages;

        double _TargetValue; 

        private readonly ILABDensityValuesProcessor _LABDensityValuesProcessor;


        public YellowValue()
        { }

        public YellowValue(ILABDensityValuesProcessor lABDensityValuesProcessor)
        {
            _LABDensityValuesProcessor = lABDensityValuesProcessor;
        }

        public string Result1LValue
        {
            get
            {
                return _Yellow1L;
            }
            set
            {
                if (_Yellow1L != value)
                {
                    _Yellow1L = value;
                    try
                    {
                        OnPropertyChanged("Result1LValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1AValue
        {
            get
            {
                return _Yellow1A;
            }
            set
            {
                if (_Yellow1A != value)
                {
                    _Yellow1A = value;
                    try
                    {
                        OnPropertyChanged("Result1AValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1BValue
        {
            get
            {
                return _Yellow1B;
            }
            set
            {
                if (_Yellow1B != value)
                {
                    _Yellow1B = value;
                    try
                    {
                        OnPropertyChanged("Result1BValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1DensityValue
        {
            get
            {
                return _Yellow1D;
            }
            set
            {
                if (_Yellow1D != value)
                {
                    _Yellow1D = value;
                    try
                    {
                        OnPropertyChanged("Result1DensityValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2LValue
        {
            get
            {
                return _Yellow2L;
            }
            set
            {
                if (_Yellow2L != value)
                {
                    _Yellow2L = value;
                    try
                    {
                        OnPropertyChanged("Result2LValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2AValue
        {
            get
            {
                return _Yellow2A;
            }
            set
            {
                if (_Yellow2A != value)
                {
                    _Yellow2A = value;
                    try
                    {
                        OnPropertyChanged("Result2AValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2BValue
        {
            get
            {
                return _Yellow2B;
            }
            set
            {
                if (_Yellow2B != value)
                {
                    _Yellow2B = value;
                    try
                    {
                        OnPropertyChanged("Result2BValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2DensityValue
        {
            get
            {
                return _Yellow2D;
            }
            set
            {
                if (_Yellow2D != value)
                {
                    _Yellow2D = value;
                    try
                    {
                        OnPropertyChanged("Result2DensityValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }


        public double YellowHead3Segment1Value
        {
            get
            {
                return _yellowHead3Segment1Value;
            }
            set
            {
                if (_yellowHead3Segment1Value != value)
                {
                    _yellowHead3Segment1Value = value;
                    try
                    {
                        //  _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("YellowHead3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double YellowHead3Segment2Value
        {
            get
            {
                return _yellowHead3Segment2Value;
            }
            set
            {
                if (_yellowHead3Segment2Value != value)
                {
                    _yellowHead3Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("YellowHead3Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double YellowHead3Segment3Value
        {
            get
            {
                return _yellowHead3Segment3Value;
            }
            set
            {
                if (_yellowHead3Segment3Value != value)
                {
                    _yellowHead3Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("YellowHead3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double YellowHead3Segment4Value
        {
            get
            {
                return _yellowHead3Segment4Value;
            }
            set
            {
                if (_yellowHead3Segment4Value != value)
                {
                    _yellowHead3Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("YellowHead3Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double YellowHead2Segment1Value
        {
            get
            {
                return _yellowHead2Segment1Value;
            }
            set
            {
                if (_yellowHead2Segment1Value != value)
                {
                    _yellowHead2Segment1Value = value;
                    OnPropertyChanged("YellowHead2Segment1Value");
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("YellowHead2Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double YellowHead2Segment2Value
        {
            get
            {
                return _yellowHead2Segment2Value;
            }
            set
            {
                if (_yellowHead2Segment2Value != value)
                {
                    _yellowHead2Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("YellowHead2Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double YellowHead2Segment3Value
        {
            get
            {
                return _yellowHead2Segment3Value;
            }
            set
            {
                if (_yellowHead2Segment3Value != value)
                {
                    _yellowHead2Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("YellowHead2Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double YellowHead2Segment4Value
        {
            get
            {
                return _yellowHead2Segment4Value;
            }
            set
            {
                if (_yellowHead2Segment4Value != value)
                {
                    _yellowHead2Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("YellowHead2Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double YellowHead1Segment1Value
        {
            get
            {
                return _yellowHead1Segment1Value;
            }
            set
            {
                if (_yellowHead1Segment1Value != value)
                {
                    _yellowHead1Segment1Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("YellowHead1Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double YellowHead1Segment2Value
        {
            get
            {
                return _yellowHead1Segment2Value;
            }
            set
            {
                if (_yellowHead1Segment2Value != value)
                {
                    _yellowHead1Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("YellowHead1Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double YellowHead1Segment3Value
        {
            get
            {
                return _yellowHead1Segment3Value;
            }
            set
            {
                if (_yellowHead1Segment3Value != value)
                {
                    _yellowHead1Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("YellowHead1Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double YellowHead1Segment4Value
        {
            get
            {
                return _yellowHead1Segment4Value;
            }
            set
            {
                if (_yellowHead1Segment4Value != value)
                {
                    _yellowHead1Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("YellowHead1Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }


        public double yellow2Head3Segment1Value
        {
            get
            {
                return _yellow2Head3Segment1Value;
            }
            set
            {
                if (_yellow2Head3Segment1Value != value)
                {
                    _yellow2Head3Segment1Value = value;
                    try
                    {
                        //  _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("yellow2Head3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double yellow2Head3Segment2Value
        {
            get
            {
                return _yellow2Head3Segment2Value;
            }
            set
            {
                if (_yellow2Head3Segment2Value != value)
                {
                    _yellow2Head3Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("yellow2Head3Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double yellow2Head3Segment3Value
        {
            get
            {
                return _yellow2Head3Segment3Value;
            }
            set
            {
                if (_yellow2Head3Segment3Value != value)
                {
                    _yellow2Head3Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("yellow2Head3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double yellow2Head3Segment4Value
        {
            get
            {
                return _yellow2Head3Segment4Value;
            }
            set
            {
                if (_yellow2Head3Segment4Value != value)
                {
                    _yellow2Head3Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("yellow2Head3Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double yellow2Head2Segment1Value
        {
            get
            {
                return _yellow2Head2Segment1Value;
            }
            set
            {
                if (_yellow2Head2Segment1Value != value)
                {
                    _yellow2Head2Segment1Value = value;
                    OnPropertyChanged("yellow2Head2Segment1Value");
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("yellow2Head2Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double yellow2Head2Segment2Value
        {
            get
            {
                return _yellow2Head2Segment2Value;
            }
            set
            {
                if (_yellow2Head2Segment2Value != value)
                {
                    _yellow2Head2Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("yellow2Head2Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double yellow2Head2Segment3Value
        {
            get
            {
                return _yellow2Head2Segment3Value;
            }
            set
            {
                if (_yellow2Head2Segment3Value != value)
                {
                    _yellow2Head2Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("yellow2Head2Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double yellow2Head2Segment4Value
        {
            get
            {
                return _yellow2Head2Segment4Value;
            }
            set
            {
                if (_yellow2Head2Segment4Value != value)
                {
                    _yellow2Head2Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("yellow2Head2Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double yellow2Head1Segment1Value
        {
            get
            {
                return _yellow2Head1Segment1Value;
            }
            set
            {
                if (_yellow2Head1Segment1Value != value)
                {
                    _yellow2Head1Segment1Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("yellow2Head1Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double yellow2Head1Segment2Value
        {
            get
            {
                return _yellow2Head1Segment2Value;
            }
            set
            {
                if (_yellow2Head1Segment2Value != value)
                {
                    _yellow2Head1Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("yellow2Head1Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double yellow2Head1Segment3Value
        {
            get
            {
                return _yellow2Head1Segment3Value;
            }
            set
            {
                if (_yellow2Head1Segment3Value != value)
                {
                    _yellow2Head1Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("yellow2Head1Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double yellow2Head1Segment4Value
        {
            get
            {
                return _yellow2Head1Segment4Value;
            }
            set
            {
                if (_yellow2Head1Segment4Value != value)
                {
                    _yellow2Head1Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_yellow1L);
                        OnPropertyChanged("yellow2Head1Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        //Current Head Vultage

        public double Yellow1CurrentHead1Voltages
        {
            get
            {
                return _Yellow1CurrentHead1Voltages;
            }
            set
            {
                if (_Yellow1CurrentHead1Voltages != value)
                {
                    _Yellow1CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("Yellow1CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Yellow2CurrentHead1Voltages
        {
            get
            {
                return _Yellow2CurrentHead1Voltages;
            }
            set
            {
                if (_Yellow2CurrentHead1Voltages != value)
                {
                    _Yellow2CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("Yellow2CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Yellow3CurrentHead1Voltages
        {
            get
            {
                return _Yellow3CurrentHead1Voltages;
            }
            set
            {
                if (_Yellow3CurrentHead1Voltages != value)
                {
                    _Yellow3CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("Yellow3CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Yellow4CurrentHead1Voltages
        {
            get
            {
                return _Yellow4CurrentHead1Voltages;
            }
            set
            {
                if (_Yellow4CurrentHead1Voltages != value)
                {
                    _Yellow4CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("Yellow4CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Yellow1CurrentHead2Voltages
        {
            get
            {
                return _Yellow1CurrentHead2Voltages;
            }
            set
            {
                if (_Yellow1CurrentHead2Voltages != value)
                {
                    _Yellow1CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("Yellow1CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Yellow2CurrentHead2Voltages
        {
            get
            {
                return _Yellow2CurrentHead2Voltages;
            }
            set
            {
                if (_Yellow2CurrentHead2Voltages != value)
                {
                    _Yellow2CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("Yellow2CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Yellow3CurrentHead2Voltages
        {
            get
            {
                return _Yellow3CurrentHead2Voltages;
            }
            set
            {
                if (_Yellow3CurrentHead2Voltages != value)
                {
                    _Yellow3CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("Yellow3CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Yellow4CurrentHead2Voltages
        {
            get
            {
                return _Yellow4CurrentHead2Voltages;
            }
            set
            {
                if (_Yellow4CurrentHead2Voltages != value)
                {
                    _Yellow4CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("Yellow4CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Yellow1CurrentHead3Voltages
        {
            get
            {
                return _Yellow1CurrentHead3Voltages;
            }
            set
            {
                if (_Yellow1CurrentHead3Voltages != value)
                {
                    _Yellow1CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("Yellow1CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Yellow2CurrentHead3Voltages
        {
            get
            {
                return _Yellow2CurrentHead3Voltages;
            }
            set
            {
                if (_Yellow2CurrentHead3Voltages != value)
                {
                    _Yellow2CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("Yellow2CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Yellow3CurrentHead3Voltages
        {
            get
            {
                return _Yellow3CurrentHead3Voltages;
            }
            set
            {
                if (_Yellow3CurrentHead3Voltages != value)
                {
                    _Yellow3CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("Yellow3CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Yellow4CurrentHead3Voltages
        {
            get
            {
                return _Yellow4CurrentHead3Voltages;
            }
            set
            {
                if (_Yellow4CurrentHead3Voltages != value)
                {
                    _Yellow4CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetYellow1LValue(_Yellow1L);
                        OnPropertyChanged("Yellow4CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double TargetValue
        {
            get
            {
                return _TargetValue;
            }
            set
            {
                if (_TargetValue != value)
                {
                    _TargetValue = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("TargetValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
