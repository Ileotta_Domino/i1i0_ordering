﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
   public class HeadViolet : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string _VioletHead3Segment1Value;
        double _VioletHead3Segment3Value;

        public HeadViolet()
        { }
        public string VioletHead3Segment1Value
        {
            get
            {
                return _VioletHead3Segment1Value;
            }
            set
            {
                if (_VioletHead3Segment1Value != value)
                {
                    _VioletHead3Segment1Value = value;
                    OnPropertyChanged("VioletHead3Segment1Value");
                    try
                    {
                        //  _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double VioletHead3Segment3Value
        {
            get
            {
                return _VioletHead3Segment3Value;
            }
            set
            {
                if (_VioletHead3Segment3Value != value)
                {
                    _VioletHead3Segment3Value = value;
                    OnPropertyChanged("VioletHead3Segment3Value");
                    try
                    {
                        //  _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
