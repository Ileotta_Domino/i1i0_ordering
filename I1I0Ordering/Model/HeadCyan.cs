﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class HeadCyan : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string _CyanHead3Segment1Value;
        double _CyanHead3Segment3Value;

        public HeadCyan()
        { }
        public string CyanHead3Segment1Value
        {
            get
            {
                return _CyanHead3Segment1Value;
            }
            set
            {
                if (_CyanHead3Segment1Value != value)
                {
                    _CyanHead3Segment1Value = value;
                    OnPropertyChanged("CyanHead3Segment1Value");
                    try
                    {
                        //  _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("CyanHead3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double CyanHead3Segment3Value
        {
            get
            {
                return _CyanHead3Segment3Value;
            }
            set
            {
                if (_CyanHead3Segment3Value != value)
                {
                    _CyanHead3Segment3Value = value;
                    OnPropertyChanged("CyanHead3Segment3Value");
                    try
                    {
                        //  _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("CyanHead3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
