﻿using I1I0Ordering.Model.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.ViewModels
{
    public class Extractor :IExtractor
    {
        public int FindLine(string input)
        {
            string numberOfSets;
            int number=0;
            string line;

            using (var reader = new StreamReader(@"c:\Users\I_Leotta\Documents\Test_Organiser.txt"))
                // Read the file and display it line by line.
            //    System.IO.StreamReader file = new System.IO.StreamReader(input);
            while ((line = reader.ReadLine()) != null)
            {
                if (line.Contains("NUMBER_OF_SETS"))
                {
                     numberOfSets = new String(line.Where(Char.IsDigit).ToArray());
                    Int32.TryParse(numberOfSets, out number);
                    //   Console.WriteLine(counter.ToString() + ": " + line);
                }

             //   counter++;
            }

          //  reader.Close();
            return number;
        }

        public string ExtractValueFromFile(string FileName, int value, int startInLine )
        {
            string output="";
            string line = File.ReadLines(FileName).Skip(value).Take(1).First();
             output = line.Substring(startInLine, 6);
            output = output.Replace("\t", "");

            return output;
        }

        public string ExtractDensityFromFile(string FileName, int value, int startInLine)
        {
            string output = "";
            string line = File.ReadLines(FileName).Skip(value).Take(1).First();
            output = line.Substring(startInLine-1, 4);


            return output;
        }

        public string ExtractValue(string FileName, int value)
        {
            string output = "";
            string line = File.ReadLines(FileName).Skip(value).Take(1).First();
            output = line.Substring(0, 4);


            return output;
        }

    }
}
