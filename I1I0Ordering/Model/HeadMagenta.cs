﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class HeadMagenta : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string _MagentaHead3Segment1Value;
        double _MagentaHead3Segment3Value;

        public HeadMagenta()
        { }
        public string MagentaHead3Segment1Value
        {
            get
            {
                return _MagentaHead3Segment1Value;
            }
            set
            {
                if (_MagentaHead3Segment1Value != value)
                {
                    _MagentaHead3Segment1Value = value;
                    OnPropertyChanged("MagentaHead3Segment1Value");
                    try
                    {
                        //  _LABDensityValuesProcessor.SetMagenta1LValue(_Magenta1L);
                        OnPropertyChanged("MagentaHead3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double MagentaHead3Segment3Value
        {
            get
            {
                return _MagentaHead3Segment3Value;
            }
            set
            {
                if (_MagentaHead3Segment3Value != value)
                {
                    _MagentaHead3Segment3Value = value;
                    OnPropertyChanged("MagentaHead3Segment3Value");
                    try
                    {
                        //  _LABDensityValuesProcessor.SetMagenta1LValue(_Magenta1L);
                        OnPropertyChanged("MagentaHead3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
