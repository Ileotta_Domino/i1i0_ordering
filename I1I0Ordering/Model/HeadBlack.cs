﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class HeadBlack : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string _BlackHead3Segment1Value;
        double _BlackHead3Segment3Value;

        public HeadBlack()
        { }
        public string BlackHead3Segment1Value
        {
            get
            {
                return _BlackHead3Segment1Value;
            }
            set
            {
                if (_BlackHead3Segment1Value != value)
                {
                    _BlackHead3Segment1Value = value;
                    OnPropertyChanged("BlackHead3Segment1Value");
                    try
                    {
                        //  _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double BlackHead3Segment3Value
        {
            get
            {
                return _BlackHead3Segment3Value;
            }
            set
            {
                if (_BlackHead3Segment3Value != value)
                {
                    _BlackHead3Segment3Value = value;
                    OnPropertyChanged("BlackHead3Segment3Value");
                    try
                    {
                        //  _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
