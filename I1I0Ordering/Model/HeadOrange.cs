﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class HeadOrange : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string _OrangeHead3Segment1Value;
        double _OrangeHead3Segment3Value;

        public HeadOrange()
        { }
        public string OrangeHead3Segment1Value
        {
            get
            {
                return _OrangeHead3Segment1Value;
            }
            set
            {
                if (_OrangeHead3Segment1Value != value)
                {
                    _OrangeHead3Segment1Value = value;
                    OnPropertyChanged("OrangeHead3Segment1Value");
                    try
                    {
                        //  _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double OrangeHead3Segment3Value
        {
            get
            {
                return _OrangeHead3Segment3Value;
            }
            set
            {
                if (_OrangeHead3Segment3Value != value)
                {
                    _OrangeHead3Segment3Value = value;
                    OnPropertyChanged("OrangeHead3Segment3Value");
                    try
                    {
                        //  _LABDensityValuesProcessor.SetOrange1LValue(_Orange1L);
                        OnPropertyChanged("OrangeHead3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
