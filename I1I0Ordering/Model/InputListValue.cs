﻿using I1I0Ordering.Model.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class InputListValue: INotifyPropertyChanged, IInputListValue
    {
        public event PropertyChangedEventHandler PropertyChanged;



        public List<string> InputList ;

        public InputListValue()
        {
            InputList = new List<string>();
        }
        public void InputListAdd(string input)
        {
            InputList.Add(input);
        }


    }
}
