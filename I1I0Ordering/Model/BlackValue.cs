﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class BlackValue: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string _Black1L;
        string _Black1A;
        string _Black1B;
        string _Black1D;
        string _Black2L;
        string _Black2A;
        string _Black2B;
        string _Black2D;

        double _BlackHead3Segment1Value;
        double _BlackHead3Segment2Value;
        double _BlackHead3Segment3Value;
        double _BlackHead3Segment4Value;
        double _BlackHead2Segment1Value;
        double _BlackHead2Segment2Value;
        double _BlackHead2Segment3Value;
        double _BlackHead2Segment4Value;
        double _BlackHead1Segment1Value;
        double _BlackHead1Segment2Value;
        double _BlackHead1Segment3Value;
        double _BlackHead1Segment4Value;

        double _Black2Head3Segment1Value;
        double _Black2Head3Segment2Value;
        double _Black2Head3Segment3Value;
        double _Black2Head3Segment4Value;
        double _Black2Head2Segment1Value;
        double _Black2Head2Segment2Value;
        double _Black2Head2Segment3Value;
        double _Black2Head2Segment4Value;
        double _Black2Head1Segment1Value;
        double _Black2Head1Segment2Value;
        double _Black2Head1Segment3Value;
        double _Black2Head1Segment4Value;

        double _Black1CurrentHead1Voltages;
        double _Black2CurrentHead1Voltages;
        double _Black3CurrentHead1Voltages;
        double _Black4CurrentHead1Voltages;
        double _Black1CurrentHead2Voltages;
        double _Black2CurrentHead2Voltages;
        double _Black3CurrentHead2Voltages;
        double _Black4CurrentHead2Voltages;
        double _Black1CurrentHead3Voltages;
        double _Black2CurrentHead3Voltages;
        double _Black3CurrentHead3Voltages;
        double _Black4CurrentHead3Voltages;

        double _TargetValue;

        private readonly ILABDensityValuesProcessor _LABDensityValuesProcessor;



        public BlackValue()
        {
        }

        public string Result1LValue
        {
            get
            {
                return _Black1L;
            }
            set
            {
                if (_Black1L != value)
                {
                    _Black1L = value;
                    try
                    {
                        OnPropertyChanged("Result1LValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1AValue
        {
            get
            {
                return _Black1A;
            }
            set
            {
                if (_Black1A != value)
                {
                    _Black1A = value;
                    try
                    {
                        OnPropertyChanged("Result1AValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    } 


                }
            }
        }

        public string Result1BValue
        {
            get
            {
                return _Black1B;
            }
            set
            {
                if (_Black1B != value)
                {
                    _Black1B = value;
                    try
                    {
                        OnPropertyChanged("Result1BValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1DensityValue
        {
            get
            {
                return _Black1D;
            }
            set
            {
                if (_Black1D != value)
                {
                    _Black1D = value;
                    try
                    {
                        OnPropertyChanged("Result1DensityValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2LValue
        {
            get
            {
                return _Black2L;
            }
            set
            {
                if (_Black2L != value)
                {
                    _Black2L = value;
                    try
                    {
                        OnPropertyChanged("Result2LValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2AValue
        {
            get
            {
                return _Black2A;
            }
            set
            {
                if (_Black2A != value)
                {
                    _Black2A = value;
                    try
                    {
                        OnPropertyChanged("Result2AValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2BValue
        {
            get
            {
                return _Black2B;
            }
            set
            {
                if (_Black2B != value)
                {
                    _Black2B = value;
                    try
                    {
                        OnPropertyChanged("Result2BValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2DensityValue
        {
            get
            {
                return _Black2D;
            }
            set
            {
                if (_Black2D != value)
                {
                    _Black2D = value;
                    try
                    {
                        OnPropertyChanged("Result2DensityValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }


        public double BlackHead3Segment1Value
        {
            get
            {
                return _BlackHead3Segment1Value;
            }
            set
            {
                if (_BlackHead3Segment1Value != value)
                {
                    _BlackHead3Segment1Value = value;
                    try
                    {
                        //  _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double BlackHead3Segment2Value
        {
            get
            {
                return _BlackHead3Segment2Value;
            }
            set
            {
                if (_BlackHead3Segment2Value != value)
                {
                    _BlackHead3Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead3Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double BlackHead3Segment3Value
        {
            get
            {
                return _BlackHead3Segment3Value;
            }
            set
            {
                if (_BlackHead3Segment3Value != value)
                {
                    _BlackHead3Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double BlackHead3Segment4Value
        {
            get
            {
                return _BlackHead3Segment4Value;
            }
            set
            {
                if (_BlackHead3Segment4Value != value)
                {
                    _BlackHead3Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead3Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double BlackHead2Segment1Value
        {
            get
            {
                return _BlackHead2Segment1Value;
            }
            set
            {
                if (_BlackHead2Segment1Value != value)
                {
                    _BlackHead2Segment1Value = value;
                    OnPropertyChanged("BlackHead2Segment1Value");
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead2Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double BlackHead2Segment2Value
        {
            get
            {
                return _BlackHead2Segment2Value;
            }
            set
            {
                if (_BlackHead2Segment2Value != value)
                {
                    _BlackHead2Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead2Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double BlackHead2Segment3Value
        {
            get
            {
                return _BlackHead2Segment3Value;
            }
            set
            {
                if (_BlackHead2Segment3Value != value)
                {
                    _BlackHead2Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead2Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double BlackHead2Segment4Value
        {
            get
            {
                return _BlackHead2Segment4Value;
            }
            set
            {
                if (_BlackHead2Segment4Value != value)
                {
                    _BlackHead2Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead2Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double BlackHead1Segment1Value
        {
            get
            {
                return _BlackHead1Segment1Value;
            }
            set
            {
                if (_BlackHead1Segment1Value != value)
                {
                    _BlackHead1Segment1Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead1Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double BlackHead1Segment2Value
        {
            get
            {
                return _BlackHead1Segment2Value;
            }
            set
            {
                if (_BlackHead1Segment2Value != value)
                {
                    _BlackHead1Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead1Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double BlackHead1Segment3Value
        {
            get
            {
                return _BlackHead1Segment3Value;
            }
            set
            {
                if (_BlackHead1Segment3Value != value)
                {
                    _BlackHead1Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead1Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double BlackHead1Segment4Value
        {
            get
            {
                return _BlackHead1Segment4Value;
            }
            set
            {
                if (_BlackHead1Segment4Value != value)
                {
                    _BlackHead1Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("BlackHead1Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Black2Head3Segment1Value
        {
            get
            {
                return _Black2Head3Segment1Value;
            }
            set
            {
                if (_Black2Head3Segment1Value != value)
                {
                    _Black2Head3Segment1Value = value;
                    try
                    {
                        //  _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2Head3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black2Head3Segment2Value
        {
            get
            {
                return _Black2Head3Segment2Value;
            }
            set
            {
                if (_Black2Head3Segment2Value != value)
                {
                    _Black2Head3Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2Head3Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black2Head3Segment3Value
        {
            get
            {
                return _Black2Head3Segment3Value;
            }
            set
            {
                if (_Black2Head3Segment3Value != value)
                {
                    _Black2Head3Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2Head3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black2Head3Segment4Value
        {
            get
            {
                return _Black2Head3Segment4Value;
            }
            set
            {
                if (_Black2Head3Segment4Value != value)
                {
                    _Black2Head3Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2Head3Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black2Head2Segment1Value
        {
            get
            {
                return _Black2Head2Segment1Value;
            }
            set
            {
                if (_Black2Head2Segment1Value != value)
                {
                    _Black2Head2Segment1Value = value;
                    OnPropertyChanged("Black2Head2Segment1Value");
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2Head2Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Black2Head2Segment2Value
        {
            get
            {
                return _Black2Head2Segment2Value;
            }
            set
            {
                if (_Black2Head2Segment2Value != value)
                {
                    _Black2Head2Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2Head2Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black2Head2Segment3Value
        {
            get
            {
                return _Black2Head2Segment3Value;
            }
            set
            {
                if (_Black2Head2Segment3Value != value)
                {
                    _Black2Head2Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2Head2Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black2Head2Segment4Value
        {
            get
            {
                return _Black2Head2Segment4Value;
            }
            set
            {
                if (_Black2Head2Segment4Value != value)
                {
                    _Black2Head2Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2Head2Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black2Head1Segment1Value
        {
            get
            {
                return _Black2Head1Segment1Value;
            }
            set
            {
                if (_Black2Head1Segment1Value != value)
                {
                    _Black2Head1Segment1Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2Head1Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Black2Head1Segment2Value
        {
            get
            {
                return _Black2Head1Segment2Value;
            }
            set
            {
                if (_Black2Head1Segment2Value != value)
                {
                    _Black2Head1Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2Head1Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black2Head1Segment3Value
        {
            get
            {
                return _Black2Head1Segment3Value;
            }
            set
            {
                if (_Black2Head1Segment3Value != value)
                {
                    _Black2Head1Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2Head1Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black2Head1Segment4Value
        {
            get
            {
                return _Black2Head1Segment4Value;
            }
            set
            {
                if (_Black2Head1Segment4Value != value)
                {
                    _Black2Head1Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2Head1Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        //Black Current Head Voltage

        public double Black1CurrentHead1Voltages
        {
            get
            {
                return _Black1CurrentHead1Voltages;
            }
            set
            {
                if (_Black1CurrentHead1Voltages != value)
                {
                    _Black1CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black1CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black2CurrentHead1Voltages
        {
            get
            {
                return _Black2CurrentHead1Voltages;
            }
            set
            {
                if (_Black2CurrentHead1Voltages != value)
                {
                    _Black2CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black3CurrentHead1Voltages
        {
            get
            {
                return _Black3CurrentHead1Voltages;
            }
            set
            {
                if (_Black3CurrentHead1Voltages != value)
                {
                    _Black3CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black3CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Black4CurrentHead1Voltages
        {
            get
            {
                return _Black4CurrentHead1Voltages;
            }
            set
            {
                if (_Black4CurrentHead1Voltages != value)
                {
                    _Black4CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black4CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Black1CurrentHead2Voltages
        {
            get
            {
                return _Black1CurrentHead2Voltages;
            }
            set
            {
                if (_Black1CurrentHead2Voltages != value)
                {
                    _Black1CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black1CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black2CurrentHead2Voltages
        {
            get
            {
                return _Black2CurrentHead2Voltages;
            }
            set
            {
                if (_Black2CurrentHead2Voltages != value)
                {
                    _Black2CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black3CurrentHead2Voltages
        {
            get
            {
                return _Black3CurrentHead2Voltages;
            }
            set
            {
                if (_Black3CurrentHead2Voltages != value)
                {
                    _Black3CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black3CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Black4CurrentHead2Voltages
        {
            get
            {
                return _Black4CurrentHead2Voltages;
            }
            set
            {
                if (_Black4CurrentHead2Voltages != value)
                {
                    _Black4CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black4CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Black1CurrentHead3Voltages
        {
            get
            {
                return _Black1CurrentHead3Voltages;
            }
            set
            {
                if (_Black1CurrentHead3Voltages != value)
                {
                    _Black1CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black1CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black2CurrentHead3Voltages
        {
            get
            {
                return _Black2CurrentHead3Voltages;
            }
            set
            {
                if (_Black2CurrentHead3Voltages != value)
                {
                    _Black2CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black2CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Black3CurrentHead3Voltages
        {
            get
            {
                return _Black3CurrentHead3Voltages;
            }
            set
            {
                if (_Black3CurrentHead3Voltages != value)
                {
                    _Black3CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black3CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Black4CurrentHead3Voltages
        {
            get
            {
                return _Black4CurrentHead3Voltages;
            }
            set
            {
                if (_Black4CurrentHead3Voltages != value)
                {
                    _Black4CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetBlack1LValue(_Black1L);
                        OnPropertyChanged("Black4CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double TargetValue
        {
            get
            {
                return _TargetValue;
            }
            set
            {
                if (_TargetValue != value)
                {
                    _TargetValue = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("TargetValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

