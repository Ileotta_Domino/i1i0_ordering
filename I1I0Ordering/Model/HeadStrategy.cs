﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    abstract class HeadStrategy
    {
     //   private readonly ILABDensityValuesProcessor _LABDensityValuesProcessor;

        //public HeadStrategy(ILABDensityValuesProcessor lABDensityValuesProcessor)
        //{
        //    _LABDensityValuesProcessor = lABDensityValuesProcessor;
        //}
        public abstract void OrderHeadByColour(int input, List<double> inputList);
    }
}
