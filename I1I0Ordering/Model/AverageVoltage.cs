﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class AverageVoltage : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        double _Seg1Value;
        double _Seg2Value;
        double _Seg3Value;
        double _Seg4Value;

        public double Seg1Value
        {
            get
            {
                return _Seg1Value;
            }
                set
            {
                if (_Seg1Value != value)
                {
                    _Seg1Value = value;
                    try
                    {
                        OnPropertyChanged("Seg1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Seg2Value
        {
            get
            {
                return _Seg2Value;
            }
            set
            {
                if (_Seg2Value != value)
                {
                    _Seg2Value = value;
                    try
                    {
                        OnPropertyChanged("Seg2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Seg3Value
        {
            get
            {
                return _Seg3Value;
            }
            set
            {
                if (_Seg3Value != value)
                {
                    _Seg3Value = value;
                    try
                    {
                        OnPropertyChanged("Seg3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Seg4Value
        {
            get
            {
                return _Seg4Value;
            }
            set
            {
                if (_Seg4Value != value)
                {
                    _Seg4Value = value;
                    try
                    {
                        OnPropertyChanged("Seg4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
