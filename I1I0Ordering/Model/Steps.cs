﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class Steps
    {
        public int Step {get; set;}
        public int SubLength { get; set; }

        public Steps(int _step, int _subLength)
        {
            Step = _step;
            SubLength = _subLength;
        }
    }
}
