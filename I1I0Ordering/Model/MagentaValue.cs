﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class MagentaValue : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string _Magenta1L;
        string _Magenta1A;
        string _Magenta1B;
        string _Magenta1D;
        string _Magenta2L;
        string _Magenta2A;
        string _Magenta2B;
        string _Magenta2D;

        double _magentaHead3Segment1Value;
        double _magentaHead3Segment2Value;
        double _magentaHead3Segment3Value;
        double _magentaHead3Segment4Value;
        double _magentaHead2Segment1Value;
        double _magentaHead2Segment2Value;
        double _magentaHead2Segment3Value;
        double _magentaHead2Segment4Value;
        double _magentaHead1Segment1Value;
        double _magentaHead1Segment2Value;
        double _magentaHead1Segment3Value;
        double _magentaHead1Segment4Value;

        double _magenta2Head3Segment1Value;
        double _magenta2Head3Segment2Value;
        double _magenta2Head3Segment3Value;
        double _magenta2Head3Segment4Value;
        double _magenta2Head2Segment1Value;
        double _magenta2Head2Segment2Value;
        double _magenta2Head2Segment3Value;
        double _magenta2Head2Segment4Value;
        double _magenta2Head1Segment1Value;
        double _magenta2Head1Segment2Value;
        double _magenta2Head1Segment3Value;
        double _magenta2Head1Segment4Value;

        double _Magenta1CurrentHead1Voltages;
        double _Magenta2CurrentHead1Voltages;
        double _Magenta3CurrentHead1Voltages;
        double _Magenta4CurrentHead1Voltages;
        double _Magenta1CurrentHead2Voltages;
        double _Magenta2CurrentHead2Voltages;
        double _Magenta3CurrentHead2Voltages;
        double _Magenta4CurrentHead2Voltages;
        double _Magenta1CurrentHead3Voltages;
        double _Magenta2CurrentHead3Voltages;
        double _Magenta3CurrentHead3Voltages;
        double _Magenta4CurrentHead3Voltages;

        double _TargetValue;
        public MagentaValue()
        {
        }

        public string Result1LValue
        {
            get
            {
                return _Magenta1L;
            }
            set
            {
                if (_Magenta1L != value)
                {
                    _Magenta1L = value;
                    try
                    {
                        OnPropertyChanged("Result1LValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1AValue
        {
            get
            {
                return _Magenta1A;
            }
            set
            {
                if (_Magenta1A != value)
                {
                    _Magenta1A = value;
                    try
                    {
                        OnPropertyChanged("Result1AValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1BValue
        {
            get
            {
                return _Magenta1B;
            }
            set
            {
                if (_Magenta1B != value)
                {
                    _Magenta1B = value;
                    try
                    {
                        OnPropertyChanged("Result1BValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1DensityValue
        {
            get
            {
                return _Magenta1D;
            }
            set
            {
                if (_Magenta1D != value)
                {
                    _Magenta1D = value;
                    try
                    {
                        OnPropertyChanged("Result1DensityValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2LValue
        {
            get
            {
                return _Magenta2L;
            }
            set
            {
                if (_Magenta2L != value)
                {
                    _Magenta2L = value;
                    try
                    {
                        OnPropertyChanged("Result2LValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2AValue
        {
            get
            {
                return _Magenta2A;
            }
            set
            {
                if (_Magenta2A != value)
                {
                    _Magenta2A = value;
                    try
                    {
                        OnPropertyChanged("Result2AValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2BValue
        {
            get
            {
                return _Magenta2B;
            }
            set
            {
                if (_Magenta2B != value)
                {
                    _Magenta2B = value;
                    try
                    {
                        OnPropertyChanged("Result2BValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2DensityValue
        {
            get
            {
                return _Magenta2D;
            }
            set
            {
                if (_Magenta2D != value)
                {
                    _Magenta2D = value;
                    try
                    {
                        OnPropertyChanged("Result2DensityValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }


        public double MagentaHead3Segment1Value
        {
            get
            {
                return _magentaHead3Segment1Value;
            }
            set
            {
                if (_magentaHead3Segment1Value != value)
                {
                    _magentaHead3Segment1Value = value;
                    try
                    {
                        //  _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("MagentaHead3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double MagentaHead3Segment2Value
        {
            get
            {
                return _magentaHead3Segment2Value;
            }
            set
            {
                if (_magentaHead3Segment2Value != value)
                {
                    _magentaHead3Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("MagentaHead3Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double MagentaHead3Segment3Value
        {
            get
            {
                return _magentaHead3Segment3Value;
            }
            set
            {
                if (_magentaHead3Segment3Value != value)
                {
                    _magentaHead3Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("MagentaHead3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double MagentaHead3Segment4Value
        {
            get
            {
                return _magentaHead3Segment4Value;
            }
            set
            {
                if (_magentaHead3Segment4Value != value)
                {
                    _magentaHead3Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("MagentaHead3Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double MagentaHead2Segment1Value
        {
            get
            {
                return _magentaHead2Segment1Value;
            }
            set
            {
                if (_magentaHead2Segment1Value != value)
                {
                    _magentaHead2Segment1Value = value;
                    OnPropertyChanged("MagentaHead2Segment1Value");
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("MagentaHead2Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double MagentaHead2Segment2Value
        {
            get
            {
                return _magentaHead2Segment2Value;
            }
            set
            {
                if (_magentaHead2Segment2Value != value)
                {
                    _magentaHead2Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("MagentaHead2Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double MagentaHead2Segment3Value
        {
            get
            {
                return _magentaHead2Segment3Value;
            }
            set
            {
                if (_magentaHead2Segment3Value != value)
                {
                    _magentaHead2Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("MagentaHead2Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double MagentaHead2Segment4Value
        {
            get
            {
                return _magentaHead2Segment4Value;
            }
            set
            {
                if (_magentaHead2Segment4Value != value)
                {
                    _magentaHead2Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("MagentaHead2Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double MagentaHead1Segment1Value
        {
            get
            {
                return _magentaHead1Segment1Value;
            }
            set
            {
                if (_magentaHead1Segment1Value != value)
                {
                    _magentaHead1Segment1Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("MagentaHead1Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double MagentaHead1Segment2Value
        {
            get
            {
                return _magentaHead1Segment2Value;
            }
            set
            {
                if (_magentaHead1Segment2Value != value)
                {
                    _magentaHead1Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("MagentaHead1Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double MagentaHead1Segment3Value
        {
            get
            {
                return _magentaHead1Segment3Value;
            }
            set
            {
                if (_magentaHead1Segment3Value != value)
                {
                    _magentaHead1Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("MagentaHead1Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double MagentaHead1Segment4Value
        {
            get
            {
                return _magentaHead1Segment4Value;
            }
            set
            {
                if (_magentaHead1Segment4Value != value)
                {
                    _magentaHead1Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("MagentaHead1Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double magenta2Head3Segment1Value
        {
            get
            {
                return _magenta2Head3Segment1Value;
            }
            set
            {
                if (_magenta2Head3Segment1Value != value)
                {
                    _magenta2Head3Segment1Value = value;
                    try
                    {
                        //  _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("magenta2Head3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double magenta2Head3Segment2Value
        {
            get
            {
                return _magenta2Head3Segment2Value;
            }
            set
            {
                if (_magenta2Head3Segment2Value != value)
                {
                    _magenta2Head3Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("magenta2Head3Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double magenta2Head3Segment3Value
        {
            get
            {
                return _magenta2Head3Segment3Value;
            }
            set
            {
                if (_magenta2Head3Segment3Value != value)
                {
                    _magenta2Head3Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("magenta2Head3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double magenta2Head3Segment4Value
        {
            get
            {
                return _magenta2Head3Segment4Value;
            }
            set
            {
                if (_magenta2Head3Segment4Value != value)
                {
                    _magenta2Head3Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("magenta2Head3Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double magenta2Head2Segment1Value
        {
            get
            {
                return _magenta2Head2Segment1Value;
            }
            set
            {
                if (_magenta2Head2Segment1Value != value)
                {
                    _magenta2Head2Segment1Value = value;
                    OnPropertyChanged("magenta2Head2Segment1Value");
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("magenta2Head2Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double magenta2Head2Segment2Value
        {
            get
            {
                return _magenta2Head2Segment2Value;
            }
            set
            {
                if (_magenta2Head2Segment2Value != value)
                {
                    _magenta2Head2Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("magenta2Head2Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double magenta2Head2Segment3Value
        {
            get
            {
                return _magenta2Head2Segment3Value;
            }
            set
            {
                if (_magenta2Head2Segment3Value != value)
                {
                    _magenta2Head2Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("magenta2Head2Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double magenta2Head2Segment4Value
        {
            get
            {
                return _magenta2Head2Segment4Value;
            }
            set
            {
                if (_magenta2Head2Segment4Value != value)
                {
                    _magenta2Head2Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("magenta2Head2Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double magenta2Head1Segment1Value
        {
            get
            {
                return _magenta2Head1Segment1Value;
            }
            set
            {
                if (_magenta2Head1Segment1Value != value)
                {
                    _magenta2Head1Segment1Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("magenta2Head1Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double magenta2Head1Segment2Value
        {
            get
            {
                return _magenta2Head1Segment2Value;
            }
            set
            {
                if (_magenta2Head1Segment2Value != value)
                {
                    _magenta2Head1Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("magenta2Head1Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double magenta2Head1Segment3Value
        {
            get
            {
                return _magenta2Head1Segment3Value;
            }
            set
            {
                if (_magenta2Head1Segment3Value != value)
                {
                    _magenta2Head1Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("magenta2Head1Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double magenta2Head1Segment4Value
        {
            get
            {
                return _magenta2Head1Segment4Value;
            }
            set
            {
                if (_magenta2Head1Segment4Value != value)
                {
                    _magenta2Head1Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetMagenta1LValue(_magenta1L);
                        OnPropertyChanged("magenta2Head1Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Magenta1CurrentHead1Voltages
        {
            get
            {
                return _Magenta1CurrentHead1Voltages;
            }
            set
            {
                if (_Magenta1CurrentHead1Voltages != value)
                {
                    _Magenta1CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Magenta1CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Magenta2CurrentHead1Voltages
        {
            get
            {
                return _Magenta2CurrentHead1Voltages;
            }
            set
            {
                if (_Magenta2CurrentHead1Voltages != value)
                {
                    _Magenta2CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Magenta2CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Magenta3CurrentHead1Voltages
        {
            get
            {
                return _Magenta3CurrentHead1Voltages;
            }
            set
            {
                if (_Magenta3CurrentHead1Voltages != value)
                {
                    _Magenta3CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Magenta3CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Magenta4CurrentHead1Voltages
        {
            get
            {
                return _Magenta4CurrentHead1Voltages;
            }
            set
            {
                if (_Magenta4CurrentHead1Voltages != value)
                {
                    _Magenta4CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Magenta4CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Magenta1CurrentHead2Voltages
        {
            get
            {
                return _Magenta1CurrentHead2Voltages;
            }
            set
            {
                if (_Magenta1CurrentHead2Voltages != value)
                {
                    _Magenta1CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Magenta1CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Magenta2CurrentHead2Voltages
        {
            get
            {
                return _Magenta2CurrentHead2Voltages;
            }
            set
            {
                if (_Magenta2CurrentHead2Voltages != value)
                {
                    _Magenta2CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Magenta2CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Magenta3CurrentHead2Voltages
        {
            get
            {
                return _Magenta3CurrentHead2Voltages;
            }
            set
            {
                if (_Magenta3CurrentHead2Voltages != value)
                {
                    _Magenta3CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Magenta3CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Magenta4CurrentHead2Voltages
        {
            get
            {
                return _Magenta4CurrentHead2Voltages;
            }
            set
            {
                if (_Magenta4CurrentHead2Voltages != value)
                {
                    _Magenta4CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Magenta4CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Magenta1CurrentHead3Voltages
        {
            get
            {
                return _Magenta1CurrentHead3Voltages;
            }
            set
            {
                if (_Magenta1CurrentHead3Voltages != value)
                {
                    _Magenta1CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Magenta1CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Magenta2CurrentHead3Voltages
        {
            get
            {
                return _Magenta2CurrentHead3Voltages;
            }
            set
            {
                if (_Magenta2CurrentHead3Voltages != value)
                {
                    _Magenta2CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Magenta2CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Magenta3CurrentHead3Voltages
        {
            get
            {
                return _Magenta3CurrentHead3Voltages;
            }
            set
            {
                if (_Magenta3CurrentHead3Voltages != value)
                {
                    _Magenta3CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Magenta3CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Magenta4CurrentHead3Voltages
        {
            get
            {
                return _Magenta4CurrentHead3Voltages;
            }
            set
            {
                if (_Magenta4CurrentHead3Voltages != value)
                {
                    _Magenta4CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Magenta4CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double TargetValue
        {
            get
            {
                return _TargetValue;
            }
            set
            {
                if (_TargetValue != value)
                {
                    _TargetValue = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("TargetValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

