﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    class MagentaHeadStrategy : HeadStrategy
    {
        double DensitytoShow;
        MagentaValue _magentaValue { get; set; }
        public HeadMagenta _headMagenta { get; set; }

        private readonly IHeadValueProcessor _headValueProcessor;
        public MagentaHeadStrategy(IHeadValueProcessor headValueProcessor)
        {
            _headMagenta = new HeadMagenta();
            _headValueProcessor = headValueProcessor;
        }
        public override void OrderHeadByColour(int input, List<double> inputList)
        {
            _magentaValue = new MagentaValue();

            DensitytoShow = Math.Round(inputList.Average(), 2);

            switch (input + 1)
            {
                case 1:
                    _headValueProcessor.SetMagentaHead3Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 2:
                    _headValueProcessor.SetMagentaHead3Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 3:
                    _headValueProcessor.SetMagentaHead3Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 4:
                    _headValueProcessor.SetMagentaHead3Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 5:
                    _headValueProcessor.SetMagentaHead2Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 6:
                    _headValueProcessor.SetMagentaHead2Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 7:
                    _headValueProcessor.SetMagentaHead2Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 8:
                    _headValueProcessor.SetMagentaHead2Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 9:
                    _headValueProcessor.SetMagentaHead1Segment1Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 10:
                    _headValueProcessor.SetMagentaHead1Segment2Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 11:
                    _headValueProcessor.SetMagentaHead1Segment3Value(DensitytoShow);
                    inputList.Clear();
                    break;
                case 12:
                    _headValueProcessor.SetMagentaHead1Segment4Value(DensitytoShow);
                    inputList.Clear();
                    break;
            }
        }
    }
}
