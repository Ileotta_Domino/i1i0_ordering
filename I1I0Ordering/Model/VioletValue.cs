﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
   public class VioletValue : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string _Violet1L;
        string _Violet1A;
        string _Violet1B;
        string _Violet1D;
        string _Violet2L;
        string _Violet2A;
        string _Violet2B;
        string _Violet2D;
        double _VioletSegmentValue;

        string _VioletHead3Segment1Value;
        double _VioletHead3Segment2Value;
        double _VioletHead3Segment3Value;
        double _VioletHead3Segment4Value;
        double _VioletHead2Segment1Value;
        double _VioletHead2Segment2Value;
        double _VioletHead2Segment3Value;
        double _VioletHead2Segment4Value;
        double _VioletHead1Segment1Value;
        double _VioletHead1Segment2Value;
        double _VioletHead1Segment3Value;
        double _VioletHead1Segment4Value;
         

        string _Violet2Head3Segment1Value;
        double _Violet2Head3Segment2Value;
        double _Violet2Head3Segment3Value;
        double _Violet2Head3Segment4Value;
        double _Violet2Head2Segment1Value;
        double _Violet2Head2Segment2Value;
        double _Violet2Head2Segment3Value;
        double _Violet2Head2Segment4Value;
        double _Violet2Head1Segment1Value;
        double _Violet2Head1Segment2Value;
        double _Violet2Head1Segment3Value;
        double _Violet2Head1Segment4Value;

        double _Violet1CurrentHead1Voltages;
        double _Violet2CurrentHead1Voltages;
        double _Violet3CurrentHead1Voltages;
        double _Violet4CurrentHead1Voltages;
        double _Violet1CurrentHead2Voltages;
        double _Violet2CurrentHead2Voltages;
        double _Violet3CurrentHead2Voltages;
        double _Violet4CurrentHead2Voltages;
        double _Violet1CurrentHead3Voltages;
        double _Violet2CurrentHead3Voltages;
        double _Violet3CurrentHead3Voltages;
        double _Violet4CurrentHead3Voltages;

        double _TargetValue;

        public VioletValue()
        {
        }
        
        public string Result1LValue
        {
            get
            {
                return _Violet1L;
            }
            set
            {
                if (_Violet1L != value)
                {
                    _Violet1L = value;
                    try
                    {
                      //  _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Result1LValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1AValue
        {
            get
            {
                return _Violet1A;
            }
            set
            {
                if (_Violet1A != value)
                {
                    _Violet1A = value;
                    try
                    {
                      //  _LABDensityValuesProcessor.SetViolet1LValue(_Violet1A);
                        OnPropertyChanged("Result1AValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1BValue
        {
            get
            {
                return _Violet1B;
            }
            set
            {
                if (_Violet1B != value)
                {
                    _Violet1B = value;
                    try
                    {
                      //  _LABDensityValuesProcessor.SetViolet1LValue(_Violet1B);
                        OnPropertyChanged("Result1BValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1DensityValue
        {
            get
            {
                return _Violet1D;
            }
            set
            {
                if (_Violet1D != value)
                {
                    _Violet1D = value;
                    try
                    {
                       // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1D);
                        OnPropertyChanged("Result1DensityValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2LValue
        {
            get
            {
                return _Violet2L;
            }
            set
            {
                if (_Violet2L != value)
                {
                    _Violet2L = value;
                    try
                    {
                      //  _LABDensityValuesProcessor.SetViolet1LValue(_Violet2L);
                        OnPropertyChanged("Result2LValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2AValue
        {
            get
            {
                return _Violet2A;
            }
            set
            {
                if (_Violet2A != value)
                {
                    _Violet2A = value;
                    try
                    {
                      //  _LABDensityValuesProcessor.SetViolet1LValue(_Violet2A);
                        OnPropertyChanged("Result2AValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2BValue
        {
            get
            {
                return _Violet2B;
            }
            set
            {
                if (_Violet2B != value)
                {
                    _Violet2B = value;
                    try
                    {
                      //  _LABDensityValuesProcessor.SetViolet1LValue(_Violet2B);
                        OnPropertyChanged("Result2BValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2DensityValue
        {
            get
            {
                return _Violet2D;
            }
            set
            {
                if (_Violet2D != value)
                {
                    _Violet2D = value;
                    try
                    {
                     //   _LABDensityValuesProcessor.SetViolet1LValue(_Violet2D);
                        OnPropertyChanged("Result2DensityValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string VioletHead3Segment1Value
        {
            get
            {
                return _VioletHead3Segment1Value;
            }
            set
            {
                if (_VioletHead3Segment1Value != value)
                {
                    _VioletHead3Segment1Value = value;
                    try
                    {
                      //  _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double VioletHead3Segment2Value
        {
            get
            {
                return _VioletHead3Segment2Value;
            }
            set
            {
                if (_VioletHead3Segment2Value != value)
                {
                    _VioletHead3Segment2Value = value;
                    try
                    {
                       // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead3Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double VioletHead3Segment3Value
        {
            get
            {
                return _VioletHead3Segment3Value;
            }
            set
            {
                if (_VioletHead3Segment3Value != value)
                {
                    _VioletHead3Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double VioletHead3Segment4Value
        {
            get
            {
                return _VioletHead3Segment4Value;
            }
            set
            {
                if (_VioletHead3Segment4Value != value)
                {
                    _VioletHead3Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead3Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double VioletHead2Segment1Value
        {
            get
            {
                return _VioletHead2Segment1Value;
            }
            set
            {
                if (_VioletHead2Segment1Value != value)
                {
                    _VioletHead2Segment1Value = value;
                    OnPropertyChanged("VioletHead2Segment1Value");
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead2Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double VioletHead2Segment2Value
        {
            get
            {
                return _VioletHead2Segment2Value;
            }
            set
            {
                if (_VioletHead2Segment2Value != value)
                {
                    _VioletHead2Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead2Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double VioletHead2Segment3Value
        {
            get
            {
                return _VioletHead2Segment3Value;
            }
            set
            {
                if (_VioletHead2Segment3Value != value)
                {
                    _VioletHead2Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead2Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double VioletHead2Segment4Value
        {
            get
            {
                return _VioletHead2Segment4Value;
            }
            set
            {
                if (_VioletHead2Segment4Value != value)
                {
                    _VioletHead2Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead2Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double VioletHead1Segment1Value
        {
            get
            {
                return _VioletHead1Segment1Value;
            }
            set
            {
                if (_VioletHead1Segment1Value != value)
                {
                    _VioletHead1Segment1Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead1Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double VioletHead1Segment2Value
        {
            get
            {
                return _VioletHead1Segment2Value;
            }
            set
            {
                if (_VioletHead1Segment2Value != value)
                {
                    _VioletHead1Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead1Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double VioletHead1Segment3Value
        {
            get
            {
                return _VioletHead1Segment3Value;
            }
            set
            {
                if (_VioletHead1Segment3Value != value)
                {
                    _VioletHead1Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead1Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double VioletHead1Segment4Value
        {
            get
            {
                return _VioletHead1Segment4Value;
            }
            set
            {
                if (_VioletHead1Segment4Value != value)
                {
                    _VioletHead1Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("VioletHead1Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public string Violet2Head3Segment1Value
        {
            get
            {
                return _Violet2Head3Segment1Value;
            }
            set
            {
                if (_Violet2Head3Segment1Value != value)
                {
                    _Violet2Head3Segment1Value = value;
                    try
                    {
                        //  _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2Head3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet2Head3Segment2Value
        {
            get
            {
                return _Violet2Head3Segment2Value;
            }
            set
            {
                if (_Violet2Head3Segment2Value != value)
                {
                    _Violet2Head3Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2Head3Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet2Head3Segment3Value
        {
            get
            {
                return _Violet2Head3Segment3Value;
            }
            set
            {
                if (_Violet2Head3Segment3Value != value)
                {
                    _Violet2Head3Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2Head3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }


        /// <summary>
        /// test voltage
        /// </summary>
        /// 
        public double Violet2Head3Segment4Value
        {
            get
            {
                return _Violet2Head3Segment4Value;
            }
            set
            {
                if (_Violet2Head3Segment4Value != value)
                {
                    _Violet2Head3Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2Head3Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Violet2Head2Segment1Value
        {
            get
            {
                return _Violet2Head2Segment1Value;
            }
            set
            {
                if (_Violet2Head2Segment1Value != value)
                {
                    _Violet2Head2Segment1Value = value;
                    OnPropertyChanged("Violet2Head2Segment1Value");
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2Head2Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Violet2Head2Segment2Value
        {
            get
            {
                return _Violet2Head2Segment2Value;
            }
            set
            {
                if (_Violet2Head2Segment2Value != value)
                {
                    _Violet2Head2Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2Head2Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet2Head2Segment3Value
        {
            get
            {
                return _Violet2Head2Segment3Value;
            }
            set
            {
                if (_Violet2Head2Segment3Value != value)
                {
                    _Violet2Head2Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2Head2Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet2Head2Segment4Value
        {
            get
            {
                return _Violet2Head2Segment4Value;
            }
            set
            {
                if (_Violet2Head2Segment4Value != value)
                {
                    _Violet2Head2Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2Head2Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet2Head1Segment1Value
        {
            get
            {
                return _Violet2Head1Segment1Value;
            }
            set
            {
                if (_Violet2Head1Segment1Value != value)
                {
                    _Violet2Head1Segment1Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2Head1Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Violet2Head1Segment2Value
        {
            get
            {
                return _Violet2Head1Segment2Value;
            }
            set
            {
                if (_Violet2Head1Segment2Value != value)
                {
                    _Violet2Head1Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2Head1Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet2Head1Segment3Value
        {
            get
            {
                return _Violet2Head1Segment3Value;
            }
            set
            {
                if (_Violet2Head1Segment3Value != value)
                {
                    _Violet2Head1Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2Head1Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet2Head1Segment4Value
        {
            get
            {
                return _Violet2Head1Segment4Value;
            }
            set
            {
                if (_Violet2Head1Segment4Value != value)
                {
                    _Violet2Head1Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2Head1Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Current Head Voltages
        /// </summary>
        /// <param name="propertyName"></param>
        public double Violet1CurrentHead1Voltages
        {
            get
            {
                return _Violet1CurrentHead1Voltages;
            }
            set
            {
                if (_Violet1CurrentHead1Voltages != value)
                {
                    _Violet1CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet1CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet2CurrentHead1Voltages
        {
            get
            {
                return _Violet2CurrentHead1Voltages;
            }
            set
            {
                if (_Violet2CurrentHead1Voltages != value)
                {
                    _Violet2CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet3CurrentHead1Voltages
        {
            get
            {
                return _Violet3CurrentHead1Voltages;
            }
            set
            {
                if (_Violet3CurrentHead1Voltages != value)
                {
                    _Violet3CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet3CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Violet4CurrentHead1Voltages
        {
            get
            {
                return _Violet4CurrentHead1Voltages;
            }
            set
            {
                if (_Violet4CurrentHead1Voltages != value)
                {
                    _Violet4CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet4CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Violet1CurrentHead2Voltages
        {
            get
            {
                return _Violet1CurrentHead2Voltages;
            }
            set
            {
                if (_Violet1CurrentHead2Voltages != value)
                {
                    _Violet1CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet1CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet2CurrentHead2Voltages
        {
            get
            {
                return _Violet2CurrentHead2Voltages;
            }
            set
            {
                if (_Violet2CurrentHead2Voltages != value)
                {
                    _Violet2CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet3CurrentHead2Voltages
        {
            get
            {
                return _Violet3CurrentHead2Voltages;
            }
            set
            {
                if (_Violet3CurrentHead2Voltages != value)
                {
                    _Violet3CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet3CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Violet4CurrentHead2Voltages
        {
            get
            {
                return _Violet4CurrentHead2Voltages;
            }
            set
            {
                if (_Violet4CurrentHead2Voltages != value)
                {
                    _Violet4CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet4CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Violet1CurrentHead3Voltages
        {
            get
            {
                return _Violet1CurrentHead3Voltages;
            }
            set
            {
                if (_Violet1CurrentHead3Voltages != value)
                {
                    _Violet1CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet1CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet2CurrentHead3Voltages
        {
            get
            {
                return _Violet2CurrentHead3Voltages;
            }
            set
            {
                if (_Violet2CurrentHead3Voltages != value)
                {
                    _Violet2CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet2CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Violet3CurrentHead3Voltages
        {
            get
            {
                return _Violet3CurrentHead3Voltages;
            }
            set
            {
                if (_Violet3CurrentHead3Voltages != value)
                {
                    _Violet3CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet3CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Violet4CurrentHead3Voltages
        {
            get
            {
                return _Violet4CurrentHead3Voltages;
            }
            set
            {
                if (_Violet4CurrentHead3Voltages != value)
                {
                    _Violet4CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("Violet4CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double TargetValue
        {
            get
            {
                return _TargetValue;
            }
            set
            {
                if (_TargetValue != value)
                {
                    _TargetValue = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("TargetValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
} 
