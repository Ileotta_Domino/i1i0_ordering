﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class CyanValue : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string _Cyan1L;
        string _Cyan1A;
        string _Cyan1B;
        string _Cyan1D;
        string _Cyan2L;
        string _Cyan2A;
        string _Cyan2B;
        string _Cyan2D;

        double _cyanHead3Segment1Value;
        double _cyanHead3Segment2Value;
        double _cyanHead3Segment3Value;
        double _cyanHead3Segment4Value;
        double _cyanHead2Segment1Value;
        double _cyanHead2Segment2Value;
        double _cyanHead2Segment3Value;
        double _cyanHead2Segment4Value;
        double _cyanHead1Segment1Value;
        double _cyanHead1Segment2Value;
        double _cyanHead1Segment3Value;
        double _cyanHead1Segment4Value;

        double _cyan2Head3Segment1Value;
        double _cyan2Head3Segment2Value;
        double _cyan2Head3Segment3Value;
        double _cyan2Head3Segment4Value;
        double _cyan2Head2Segment1Value;
        double _cyan2Head2Segment2Value;
        double _cyan2Head2Segment3Value;
        double _cyan2Head2Segment4Value;
        double _cyan2Head1Segment1Value;
        double _cyan2Head1Segment2Value;
        double _cyan2Head1Segment3Value;
        double _cyan2Head1Segment4Value;

        double _Cyan1CurrentHead1Voltages;
        double _Cyan2CurrentHead1Voltages;
        double _Cyan3CurrentHead1Voltages;
        double _Cyan4CurrentHead1Voltages;
        double _Cyan1CurrentHead2Voltages;
        double _Cyan2CurrentHead2Voltages;
        double _Cyan3CurrentHead2Voltages;
        double _Cyan4CurrentHead2Voltages;
        double _Cyan1CurrentHead3Voltages;
        double _Cyan2CurrentHead3Voltages;
        double _Cyan3CurrentHead3Voltages;
        double _Cyan4CurrentHead3Voltages;

        double _TargetValue;

        public CyanValue()
        {
        }

        public string Result1LValue
        {
            get
            {
                return _Cyan1L;
            }
            set
            {
                if (_Cyan1L != value)
                {
                    _Cyan1L = value;
                    try
                    {
                        OnPropertyChanged("Result1LValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1AValue
        {
            get
            {
                return _Cyan1A;
            }
            set
            {
                if (_Cyan1A != value)
                {
                    _Cyan1A = value;
                    try
                    {
                        OnPropertyChanged("Result1AValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1BValue
        {
            get
            {
                return _Cyan1B;
            }
            set
            {
                if (_Cyan1B != value)
                {
                    _Cyan1B = value;
                    try
                    {
                        OnPropertyChanged("Result1BValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result1DensityValue
        {
            get
            {
                return _Cyan1D;
            }
            set
            {
                if (_Cyan1D != value)
                {
                    _Cyan1D = value;
                    try
                    {
                        OnPropertyChanged("Result1DensityValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2LValue
        {
            get
            {
                return _Cyan2L;
            }
            set
            {
                if (_Cyan2L != value)
                {
                    _Cyan2L = value;
                    try
                    {
                        OnPropertyChanged("Result2LValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2AValue
        {
            get
            {
                return _Cyan2A;
            }
            set
            {
                if (_Cyan2A != value)
                {
                    _Cyan2A = value;
                    try
                    {
                        OnPropertyChanged("Result2AValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2BValue
        {
            get
            {
                return _Cyan2B;
            }
            set
            {
                if (_Cyan2B != value)
                {
                    _Cyan2B = value;
                    try
                    {
                        OnPropertyChanged("Result2BValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Result2DensityValue
        {
            get
            {
                return _Cyan2D;
            }
            set
            {
                if (_Cyan2D != value)
                {
                    _Cyan2D = value;
                    try
                    {
                        OnPropertyChanged("Result2DensityValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public double CyanHead3Segment1Value
        {
            get
            {
                return _cyanHead3Segment1Value;
            }
            set
            {
                if (_cyanHead3Segment1Value != value)
                {
                    _cyanHead3Segment1Value = value;
                    try
                    {
                        //  _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("CyanHead3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double CyanHead3Segment2Value
        {
            get
            {
                return _cyanHead3Segment2Value;
            }
            set
            {
                if (_cyanHead3Segment2Value != value)
                {
                    _cyanHead3Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("CyanHead3Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double CyanHead3Segment3Value
        {
            get
            {
                return _cyanHead3Segment3Value;
            }
            set
            {
                if (_cyanHead3Segment3Value != value)
                {
                    _cyanHead3Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("CyanHead3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double CyanHead3Segment4Value
        {
            get
            {
                return _cyanHead3Segment4Value;
            }
            set
            {
                if (_cyanHead3Segment4Value != value)
                {
                    _cyanHead3Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("CyanHead3Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double CyanHead2Segment1Value
        {
            get
            {
                return _cyanHead2Segment1Value;
            }
            set
            {
                if (_cyanHead2Segment1Value != value)
                {
                    _cyanHead2Segment1Value = value;
                    OnPropertyChanged("CyanHead2Segment1Value");
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("CyanHead2Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double CyanHead2Segment2Value
        {
            get
            {
                return _cyanHead2Segment2Value;
            }
            set
            {
                if (_cyanHead2Segment2Value != value)
                {
                    _cyanHead2Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("CyanHead2Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double CyanHead2Segment3Value
        {
            get
            {
                return _cyanHead2Segment3Value;
            }
            set
            {
                if (_cyanHead2Segment3Value != value)
                {
                    _cyanHead2Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("CyanHead2Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double CyanHead2Segment4Value
        {
            get
            {
                return _cyanHead2Segment4Value;
            }
            set
            {
                if (_cyanHead2Segment4Value != value)
                {
                    _cyanHead2Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("CyanHead2Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double CyanHead1Segment1Value
        {
            get
            {
                return _cyanHead1Segment1Value;
            }
            set
            {
                if (_cyanHead1Segment1Value != value)
                {
                    _cyanHead1Segment1Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("CyanHead1Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double CyanHead1Segment2Value
        {
            get
            {
                return _cyanHead1Segment2Value;
            }
            set
            {
                if (_cyanHead1Segment2Value != value)
                {
                    _cyanHead1Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("CyanHead1Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double CyanHead1Segment3Value
        {
            get
            {
                return _cyanHead1Segment3Value;
            }
            set
            {
                if (_cyanHead1Segment3Value != value)
                {
                    _cyanHead1Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("CyanHead1Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double CyanHead1Segment4Value
        {
            get
            {
                return _cyanHead1Segment4Value;
            }
            set
            {
                if (_cyanHead1Segment4Value != value)
                {
                    _cyanHead1Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("CyanHead1Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double cyan2Head3Segment1Value
        {
            get
            {
                return _cyan2Head3Segment1Value;
            }
            set
            {
                if (_cyan2Head3Segment1Value != value)
                {
                    _cyan2Head3Segment1Value = value;
                    try
                    {
                        //  _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("cyan2Head3Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double cyan2Head3Segment2Value
        {
            get
            {
                return _cyan2Head3Segment2Value;
            }
            set
            {
                if (_cyan2Head3Segment2Value != value)
                {
                    _cyan2Head3Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("cyan2Head3Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double cyan2Head3Segment3Value
        {
            get
            {
                return _cyan2Head3Segment3Value;
            }
            set
            {
                if (_cyan2Head3Segment3Value != value)
                {
                    _cyan2Head3Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("cyan2Head3Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double cyan2Head3Segment4Value
        {
            get
            {
                return _cyan2Head3Segment4Value;
            }
            set
            {
                if (_cyan2Head3Segment4Value != value)
                {
                    _cyan2Head3Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("cyan2Head3Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double cyan2Head2Segment1Value
        {
            get
            {
                return _cyan2Head2Segment1Value;
            }
            set
            {
                if (_cyan2Head2Segment1Value != value)
                {
                    _cyan2Head2Segment1Value = value;
                    OnPropertyChanged("cyan2Head2Segment1Value");
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("cyan2Head2Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double cyan2Head2Segment2Value
        {
            get
            {
                return _cyan2Head2Segment2Value;
            }
            set
            {
                if (_cyan2Head2Segment2Value != value)
                {
                    _cyan2Head2Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("cyan2Head2Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double cyan2Head2Segment3Value
        {
            get
            {
                return _cyan2Head2Segment3Value;
            }
            set
            {
                if (_cyan2Head2Segment3Value != value)
                {
                    _cyan2Head2Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("cyan2Head2Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double cyan2Head2Segment4Value
        {
            get
            {
                return _cyan2Head2Segment4Value;
            }
            set
            {
                if (_cyan2Head2Segment4Value != value)
                {
                    _cyan2Head2Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("cyan2Head2Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double cyan2Head1Segment1Value
        {
            get
            {
                return _cyan2Head1Segment1Value;
            }
            set
            {
                if (_cyan2Head1Segment1Value != value)
                {
                    _cyan2Head1Segment1Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("cyan2Head1Segment1Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double cyan2Head1Segment2Value
        {
            get
            {
                return _cyan2Head1Segment2Value;
            }
            set
            {
                if (_cyan2Head1Segment2Value != value)
                {
                    _cyan2Head1Segment2Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("cyan2Head1Segment2Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double cyan2Head1Segment3Value
        {
            get
            {
                return _cyan2Head1Segment3Value;
            }
            set
            {
                if (_cyan2Head1Segment3Value != value)
                {
                    _cyan2Head1Segment3Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("cyan2Head1Segment3Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double cyan2Head1Segment4Value
        {
            get
            {
                return _cyan2Head1Segment4Value;
            }
            set
            {
                if (_cyan2Head1Segment4Value != value)
                {
                    _cyan2Head1Segment4Value = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_cyan1L);
                        OnPropertyChanged("cyan2Head1Segment4Value");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Cyan1CurrentHead1Voltages
        {
            get
            {
                return _Cyan1CurrentHead1Voltages;
            }
            set
            {
                if (_Cyan1CurrentHead1Voltages != value)
                {
                    _Cyan1CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("Cyan1CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Cyan2CurrentHead1Voltages
        {
            get
            {
                return _Cyan2CurrentHead1Voltages;
            }
            set
            {
                if (_Cyan2CurrentHead1Voltages != value)
                {
                    _Cyan2CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("Cyan2CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Cyan3CurrentHead1Voltages
        {
            get
            {
                return _Cyan3CurrentHead1Voltages;
            }
            set
            {
                if (_Cyan3CurrentHead1Voltages != value)
                {
                    _Cyan3CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("Cyan3CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Cyan4CurrentHead1Voltages
        {
            get
            {
                return _Cyan4CurrentHead1Voltages;
            }
            set
            {
                if (_Cyan4CurrentHead1Voltages != value)
                {
                    _Cyan4CurrentHead1Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("Cyan4CurrentHead1Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Cyan1CurrentHead2Voltages
        {
            get
            {
                return _Cyan1CurrentHead2Voltages;
            }
            set
            {
                if (_Cyan1CurrentHead2Voltages != value)
                {
                    _Cyan1CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("Cyan1CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Cyan2CurrentHead2Voltages
        {
            get
            {
                return _Cyan2CurrentHead2Voltages;
            }
            set
            {
                if (_Cyan2CurrentHead2Voltages != value)
                {
                    _Cyan2CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("Cyan2CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Cyan3CurrentHead2Voltages
        {
            get
            {
                return _Cyan3CurrentHead2Voltages;
            }
            set
            {
                if (_Cyan3CurrentHead2Voltages != value)
                {
                    _Cyan3CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("Cyan3CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Cyan4CurrentHead2Voltages
        {
            get
            {
                return _Cyan4CurrentHead2Voltages;
            }
            set
            {
                if (_Cyan4CurrentHead2Voltages != value)
                {
                    _Cyan4CurrentHead2Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("Cyan4CurrentHead2Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Cyan1CurrentHead3Voltages
        {
            get
            {
                return _Cyan1CurrentHead3Voltages;
            }
            set
            {
                if (_Cyan1CurrentHead3Voltages != value)
                {
                    _Cyan1CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("Cyan1CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Cyan2CurrentHead3Voltages
        {
            get
            {
                return _Cyan2CurrentHead3Voltages;
            }
            set
            {
                if (_Cyan2CurrentHead3Voltages != value)
                {
                    _Cyan2CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("Cyan2CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public double Cyan3CurrentHead3Voltages
        {
            get
            {
                return _Cyan3CurrentHead3Voltages;
            }
            set
            {
                if (_Cyan3CurrentHead3Voltages != value)
                {
                    _Cyan3CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("Cyan3CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double Cyan4CurrentHead3Voltages
        {
            get
            {
                return _Cyan4CurrentHead3Voltages;
            }
            set
            {
                if (_Cyan4CurrentHead3Voltages != value)
                {
                    _Cyan4CurrentHead3Voltages = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetCyan1LValue(_Cyan1L);
                        OnPropertyChanged("Cyan4CurrentHead3Voltages");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double TargetValue
        {
            get
            {
                return _TargetValue;
            }
            set
            {
                if (_TargetValue != value)
                {
                    _TargetValue = value;
                    try
                    {
                        // _LABDensityValuesProcessor.SetViolet1LValue(_Violet1L);
                        OnPropertyChanged("TargetValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}