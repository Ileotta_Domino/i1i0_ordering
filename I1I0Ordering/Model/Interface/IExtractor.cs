﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model.Interface
{
    public interface IExtractor
    {
        int FindLine(string path);
        string ExtractValueFromFile(string FileName, int value, int startInLine);
       string ExtractDensityFromFile(string FileName, int value, int startInLine);
    }
}
