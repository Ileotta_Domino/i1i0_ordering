﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Model
{
    public class HeadSegment
    {
        public double SegmentOne { get; set; }
        public double SegmentTwo { get; set; }
        public double SegmentThree { get; set; }
        public double SegmentFour { get; set; }
    }
}
