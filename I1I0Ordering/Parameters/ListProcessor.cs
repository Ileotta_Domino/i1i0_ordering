﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Parameters
{
    public class ListProcessor: IListProcessor
    {
        public int GetElementAt(int input)
        {

            return ListKeeper.GetElementAtList(input);
        }

        public int GetListLength()
        {
            return ListKeeper.ListLength();
        }
        public List<int> GetList()
        {
            return ListKeeper.StepsList;
        }

        public List<int> GetVioletFirstList()
        {
            return ListKeeper.VioletFirstLinesList;
        }

    }
}
