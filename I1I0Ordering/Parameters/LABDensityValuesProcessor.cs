﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Parameters
{
    public class LABDensityValuesProcessor : ILABDensityValuesProcessor
    {
        //string Violet1L;

        public string GetViolet1LValue()
        {
            return LABDensityValuesKeeper.Violet1LValue;
        }
        public void SetViolet1LValue(string  inputToSet)
        {
            LABDensityValuesKeeper.Violet1LValue = inputToSet;
        }

        public string GetVioletHead1Segment1Value()
        {
            return LABDensityValuesKeeper.VioletHead1Segment1Value;
        }
        public void SetVioletHead1Segment1Value(string inputToSet)
        {
            LABDensityValuesKeeper.VioletHead1Segment1Value = inputToSet;
        }

    }
}
