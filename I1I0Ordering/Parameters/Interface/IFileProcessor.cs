﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Parameters.Interface
{
    public interface IFileProcessor
    {
        string GetBlankFileLocation();
        void SetBlankFileLocation(string BlankFileLocationToSet);
        string GetLogFileLocation();
        void SetLogFileLocation(string LogFileLocationToSet);
        string checkFile(string FileName);
    }
}
