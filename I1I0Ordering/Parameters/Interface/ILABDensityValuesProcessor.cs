﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Parameters.Interface
{
    public interface ILABDensityValuesProcessor
    {
        string GetViolet1LValue();
        void SetViolet1LValue(string inputToSet);
        string  GetVioletHead1Segment1Value();
        void SetVioletHead1Segment1Value(string inputToSet);
    }
}
