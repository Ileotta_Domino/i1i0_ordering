﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Parameters.Interface
{
   public  interface IHeadValueProcessor
    {
        /////////////////////////////////////////
        ///Violet
        /////////////////////////////////////////

        /// <summary>
        /// Head 3
        /// </summary>
        /// <returns></returns>
        double GetVioletHead3Segment1Value();
        void SetVioletHead3Segment1Value(double input);
        double GetVioletHead3Segment2Value();
        void SetVioletHead3Segment2Value(double input);
        double GetVioletHead3Segment3Value();
        void SetVioletHead3Segment3Value(double input);
        double GetVioletHead3Segment4Value();
        void SetVioletHead3Segment4Value(double input);
        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        double GetVioletHead2Segment1Value();
        void SetVioletHead2Segment1Value(double input);
        double GetVioletHead2Segment2Value();
        void SetVioletHead2Segment2Value(double input);
        double GetVioletHead2Segment3Value();
        void SetVioletHead2Segment3Value(double input);
        double GetVioletHead2Segment4Value();
        void SetVioletHead2Segment4Value(double input);
        /// <summary>
        /// Head 1
        /// </summary>
        /// <returns></returns>
        double GetVioletHead1Segment1Value();
        void SetVioletHead1Segment1Value(double input);
        double GetVioletHead1Segment2Value();
        void SetVioletHead1Segment2Value(double input);
        double GetVioletHead1Segment3Value();
        void SetVioletHead1Segment3Value(double input);
        double GetVioletHead1Segment4Value();
        void SetVioletHead1Segment4Value(double input);

        /////////////////////////////////////////
        ///Violet2
        /////////////////////////////////////////

        /// <summary>
        /// Head 3
        /// </summary>
        /// <returns></returns>
        double GetViolet2Head3Segment1Value();
        void SetViolet2Head3Segment1Value(double input);
        double GetViolet2Head3Segment2Value();
        void SetViolet2Head3Segment2Value(double input);
        double GetViolet2Head3Segment3Value();
        void SetViolet2Head3Segment3Value(double input);
        double GetViolet2Head3Segment4Value();
        void SetViolet2Head3Segment4Value(double input);
        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        double GetViolet2Head2Segment1Value();
        void SetViolet2Head2Segment1Value(double input);
        double GetViolet2Head2Segment2Value();
        void SetViolet2Head2Segment2Value(double input);
        double GetViolet2Head2Segment3Value();
        void SetViolet2Head2Segment3Value(double input);
        double GetViolet2Head2Segment4Value();
        void SetViolet2Head2Segment4Value(double input);
        /// <summary>
        /// Head 1
        /// </summary>
        /// <returns></returns>
        double GetViolet2Head1Segment1Value();
        void SetViolet2Head1Segment1Value(double input);
        double GetViolet2Head1Segment2Value();
        void SetViolet2Head1Segment2Value(double input);
        double GetViolet2Head1Segment3Value();
        void SetViolet2Head1Segment3Value(double input);
        double GetViolet2Head1Segment4Value();
        void SetViolet2Head1Segment4Value(double input);


        /////////////////////////////////////////
        ///Orange
        /////////////////////////////////////////

        /// <summary>
        /// Head 3
        /// </summary>
        /// <returns></returns>
        double GetOrangeHead3Segment1Value();
        void SetOrangeHead3Segment1Value(double input);
        double GetOrangeHead3Segment2Value();
        void SetOrangeHead3Segment2Value(double input);
        double GetOrangeHead3Segment3Value();
        void SetOrangeHead3Segment3Value(double input);
        double GetOrangeHead3Segment4Value();
        void SetOrangeHead3Segment4Value(double input);
        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        double GetOrangeHead2Segment1Value();
        void SetOrangeHead2Segment1Value(double input);
        double GetOrangeHead2Segment2Value();
        void SetOrangeHead2Segment2Value(double input);
        double GetOrangeHead2Segment3Value();
        void SetOrangeHead2Segment3Value(double input);
        double GetOrangeHead2Segment4Value();
        void SetOrangeHead2Segment4Value(double input);
        /// <summary>
        /// Head 1
        /// </summary>
        /// <returns></returns>
        double GetOrangeHead1Segment1Value();
        void SetOrangeHead1Segment1Value(double input);
        double GetOrangeHead1Segment2Value();
        void SetOrangeHead1Segment2Value(double input);
        double GetOrangeHead1Segment3Value();
        void SetOrangeHead1Segment3Value(double input);
        double GetOrangeHead1Segment4Value();
        void SetOrangeHead1Segment4Value(double input);


        /////////////////////////////////////////
        ///Orange2
        /////////////////////////////////////////

        /// <summary>
        /// Head 3
        /// </summary>
        /// <returns></returns>
        double GetOrange2Head3Segment1Value();
        void SetOrange2Head3Segment1Value(double input);
        double GetOrange2Head3Segment2Value();
        void SetOrange2Head3Segment2Value(double input);
        double GetOrange2Head3Segment3Value();
        void SetOrange2Head3Segment3Value(double input);
        double GetOrange2Head3Segment4Value();
        void SetOrange2Head3Segment4Value(double input);
        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        double GetOrange2Head2Segment1Value();
        void SetOrange2Head2Segment1Value(double input);
        double GetOrange2Head2Segment2Value();
        void SetOrange2Head2Segment2Value(double input);
        double GetOrange2Head2Segment3Value();
        void SetOrange2Head2Segment3Value(double input);
        double GetOrange2Head2Segment4Value();
        void SetOrange2Head2Segment4Value(double input);
        /// <summary>
        /// Head 1
        /// </summary>
        /// <returns></returns>
        double GetOrange2Head1Segment1Value();
        void SetOrange2Head1Segment1Value(double input);
        double GetOrange2Head1Segment2Value();
        void SetOrange2Head1Segment2Value(double input);
        double GetOrange2Head1Segment3Value();
        void SetOrange2Head1Segment3Value(double input);
        double GetOrange2Head1Segment4Value();
        void SetOrange2Head1Segment4Value(double input);

        /////////////////////////////////////////
        ///Black
        /////////////////////////////////////////

        /// <summary>
        /// Head 3
        /// </summary>
        /// <returns></returns>
        double GetBlackHead3Segment1Value();
        void SetBlackHead3Segment1Value(double input);
        double GetBlackHead3Segment2Value();
        void SetBlackHead3Segment2Value(double input);
        double GetBlackHead3Segment3Value();
        void SetBlackHead3Segment3Value(double input);
        double GetBlackHead3Segment4Value();
        void SetBlackHead3Segment4Value(double input);
        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        double GetBlackHead2Segment1Value();
        void SetBlackHead2Segment1Value(double input);
        double GetBlackHead2Segment2Value();
        void SetBlackHead2Segment2Value(double input);
        double GetBlackHead2Segment3Value();
        void SetBlackHead2Segment3Value(double input);
        double GetBlackHead2Segment4Value();
        void SetBlackHead2Segment4Value(double input);
        /// <summary>
        /// Head 1
        /// </summary>
        /// <returns></returns>
        double GetBlackHead1Segment1Value();
        void SetBlackHead1Segment1Value(double input);
        double GetBlackHead1Segment2Value();
        void SetBlackHead1Segment2Value(double input);
        double GetBlackHead1Segment3Value();
        void SetBlackHead1Segment3Value(double input);
        double GetBlackHead1Segment4Value();
        void SetBlackHead1Segment4Value(double input);

        /////////////////////////////////////////
        ///Black2
        /////////////////////////////////////////

        /// <summary>
        /// Head 3
        /// </summary>
        /// <returns></returns>
        double GetBlack2Head3Segment1Value();
        void SetBlack2Head3Segment1Value(double input);
        double GetBlack2Head3Segment2Value();
        void SetBlack2Head3Segment2Value(double input);
        double GetBlack2Head3Segment3Value();
        void SetBlack2Head3Segment3Value(double input);
        double GetBlack2Head3Segment4Value();
        void SetBlack2Head3Segment4Value(double input);
        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        double GetBlack2Head2Segment1Value();
        void SetBlack2Head2Segment1Value(double input);
        double GetBlack2Head2Segment2Value();
        void SetBlack2Head2Segment2Value(double input);
        double GetBlack2Head2Segment3Value();
        void SetBlack2Head2Segment3Value(double input);
        double GetBlack2Head2Segment4Value();
        void SetBlack2Head2Segment4Value(double input);
        /// <summary>
        /// Head 1
        /// </summary>
        /// <returns></returns>
        double GetBlack2Head1Segment1Value();
        void SetBlack2Head1Segment1Value(double input);
        double GetBlack2Head1Segment2Value();
        void SetBlack2Head1Segment2Value(double input);
        double GetBlack2Head1Segment3Value();
        void SetBlack2Head1Segment3Value(double input);
        double GetBlack2Head1Segment4Value();
        void SetBlack2Head1Segment4Value(double input);

        /////////////////////////////////////////
        ///Yellow
        /////////////////////////////////////////

        /// <summary>
        /// Head 3
        /// </summary>
        /// <returns></returns>
        double GetYellowHead3Segment1Value();
        void SetYellowHead3Segment1Value(double input);
        double GetYellowHead3Segment2Value();
        void SetYellowHead3Segment2Value(double input);
        double GetYellowHead3Segment3Value();
        void SetYellowHead3Segment3Value(double input);
        double GetYellowHead3Segment4Value();
        void SetYellowHead3Segment4Value(double input);
        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        double GetYellowHead2Segment1Value();
        void SetYellowHead2Segment1Value(double input);
        double GetYellowHead2Segment2Value();
        void SetYellowHead2Segment2Value(double input);
        double GetYellowHead2Segment3Value();
        void SetYellowHead2Segment3Value(double input);
        double GetYellowHead2Segment4Value();
        void SetYellowHead2Segment4Value(double input);
        /// <summary>
        /// Head 1
        /// </summary>
        /// <returns></returns>
        double GetYellowHead1Segment1Value();
        void SetYellowHead1Segment1Value(double input);
        double GetYellowHead1Segment2Value();
        void SetYellowHead1Segment2Value(double input);
        double GetYellowHead1Segment3Value();
        void SetYellowHead1Segment3Value(double input);
        double GetYellowHead1Segment4Value();
        void SetYellowHead1Segment4Value(double input);

        /////////////////////////////////////////
        ///Yellow
        /////////////////////////////////////////

        /// <summary>
        /// Head 3
        /// </summary>
        /// <returns></returns>
        double GetYellow2Head3Segment1Value();
        void SetYellow2Head3Segment1Value(double input);
        double GetYellow2Head3Segment2Value();
        void SetYellow2Head3Segment2Value(double input);
        double GetYellow2Head3Segment3Value();
        void SetYellow2Head3Segment3Value(double input);
        double GetYellow2Head3Segment4Value();
        void SetYellow2Head3Segment4Value(double input);
        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        double GetYellow2Head2Segment1Value();
        void SetYellow2Head2Segment1Value(double input);
        double GetYellow2Head2Segment2Value();
        void SetYellow2Head2Segment2Value(double input);
        double GetYellow2Head2Segment3Value();
        void SetYellow2Head2Segment3Value(double input);
        double GetYellow2Head2Segment4Value();
        void SetYellow2Head2Segment4Value(double input);
        /// <summary>
        /// Head 1
        /// </summary>
        /// <returns></returns>
        double GetYellow2Head1Segment1Value();
        void SetYellow2Head1Segment1Value(double input);
        double GetYellow2Head1Segment2Value();
        void SetYellow2Head1Segment2Value(double input);
        double GetYellow2Head1Segment3Value();
        void SetYellow2Head1Segment3Value(double input);
        double GetYellow2Head1Segment4Value();
        void SetYellow2Head1Segment4Value(double input);
        /////////////////////////////////////////
        ///Magenta
        /////////////////////////////////////////

        /// <summary>
        /// Head 3
        /// </summary>
        /// <returns></returns>
        double GetMagentaHead3Segment1Value();
        void SetMagentaHead3Segment1Value(double input);
        double GetMagentaHead3Segment2Value();
        void SetMagentaHead3Segment2Value(double input);
        double GetMagentaHead3Segment3Value();
        void SetMagentaHead3Segment3Value(double input);
        double GetMagentaHead3Segment4Value();
        void SetMagentaHead3Segment4Value(double input);
        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        double GetMagentaHead2Segment1Value();
        void SetMagentaHead2Segment1Value(double input);
        double GetMagentaHead2Segment2Value();
        void SetMagentaHead2Segment2Value(double input);
        double GetMagentaHead2Segment3Value();
        void SetMagentaHead2Segment3Value(double input);
        double GetMagentaHead2Segment4Value();
        void SetMagentaHead2Segment4Value(double input);
        /// <summary>
        /// Head 1
        /// </summary>
        /// <returns></returns>
        double GetMagentaHead1Segment1Value();
        void SetMagentaHead1Segment1Value(double input);
        double GetMagentaHead1Segment2Value();
        void SetMagentaHead1Segment2Value(double input);
        double GetMagentaHead1Segment3Value();
        void SetMagentaHead1Segment3Value(double input);
        double GetMagentaHead1Segment4Value();
        void SetMagentaHead1Segment4Value(double input);

        /////////////////////////////////////////
        ///Magenta2
        /////////////////////////////////////////

        /// <summary>
        /// Head 3
        /// </summary>
        /// <returns></returns>
        double GetMagenta2Head3Segment1Value();
        void SetMagenta2Head3Segment1Value(double input);
        double GetMagenta2Head3Segment2Value();
        void SetMagenta2Head3Segment2Value(double input);
        double GetMagenta2Head3Segment3Value();
        void SetMagenta2Head3Segment3Value(double input);
        double GetMagenta2Head3Segment4Value();
        void SetMagenta2Head3Segment4Value(double input);
        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        double GetMagenta2Head2Segment1Value();
        void SetMagenta2Head2Segment1Value(double input);
        double GetMagenta2Head2Segment2Value();
        void SetMagenta2Head2Segment2Value(double input);
        double GetMagenta2Head2Segment3Value();
        void SetMagenta2Head2Segment3Value(double input);
        double GetMagenta2Head2Segment4Value();
        void SetMagenta2Head2Segment4Value(double input);
        /// <summary>
        /// Head 1
        /// </summary>
        /// <returns></returns>
        double GetMagenta2Head1Segment1Value();
        void SetMagenta2Head1Segment1Value(double input);
        double GetMagenta2Head1Segment2Value();
        void SetMagenta2Head1Segment2Value(double input);
        double GetMagenta2Head1Segment3Value();
        void SetMagenta2Head1Segment3Value(double input);
        double GetMagenta2Head1Segment4Value();
        void SetMagenta2Head1Segment4Value(double input);

        /////////////////////////////////////////
        ///Cyan
        /////////////////////////////////////////

        /// <summary>
        /// Head 3
        /// </summary>
        /// <returns></returns>
        double GetCyanHead3Segment1Value();
        void SetCyanHead3Segment1Value(double input);
        double GetCyanHead3Segment2Value();
        void SetCyanHead3Segment2Value(double input);
        double GetCyanHead3Segment3Value();
        void SetCyanHead3Segment3Value(double input);
        double GetCyanHead3Segment4Value();
        void SetCyanHead3Segment4Value(double input);
        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        double GetCyanHead2Segment1Value();
        void SetCyanHead2Segment1Value(double input);
        double GetCyanHead2Segment2Value();
        void SetCyanHead2Segment2Value(double input);
        double GetCyanHead2Segment3Value();
        void SetCyanHead2Segment3Value(double input);
        double GetCyanHead2Segment4Value();
        void SetCyanHead2Segment4Value(double input);
        /// <summary>
        /// Head 1
        /// </summary>
        /// <returns></returns>
        double GetCyanHead1Segment1Value();
        void SetCyanHead1Segment1Value(double input);
        double GetCyanHead1Segment2Value();
        void SetCyanHead1Segment2Value(double input);
        double GetCyanHead1Segment3Value();
        void SetCyanHead1Segment3Value(double input);
        double GetCyanHead1Segment4Value();
        void SetCyanHead1Segment4Value(double input);

        /////////////////////////////////////////
        ///Cyan2
        /////////////////////////////////////////

        /// <summary>
        /// Head 3
        /// </summary>
        /// <returns></returns>
        double GetCyan2Head3Segment1Value();
        void SetCyan2Head3Segment1Value(double input);
        double GetCyan2Head3Segment2Value();
        void SetCyan2Head3Segment2Value(double input);
        double GetCyan2Head3Segment3Value();
        void SetCyan2Head3Segment3Value(double input);
        double GetCyan2Head3Segment4Value();
        void SetCyan2Head3Segment4Value(double input);
        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        double GetCyan2Head2Segment1Value();
        void SetCyan2Head2Segment1Value(double input);
        double GetCyan2Head2Segment2Value();
        void SetCyan2Head2Segment2Value(double input);
        double GetCyan2Head2Segment3Value();
        void SetCyan2Head2Segment3Value(double input);
        double GetCyan2Head2Segment4Value();
        void SetCyan2Head2Segment4Value(double input);
        /// <summary>
        /// Head 1
        /// </summary>
        /// <returns></returns>
        double GetCyan2Head1Segment1Value();
        void SetCyan2Head1Segment1Value(double input);
        double GetCyan2Head1Segment2Value();
        void SetCyan2Head1Segment2Value(double input);
        double GetCyan2Head1Segment3Value();
        void SetCyan2Head1Segment3Value(double input);
        double GetCyan2Head1Segment4Value();
        void SetCyan2Head1Segment4Value(double input);
    }
}
