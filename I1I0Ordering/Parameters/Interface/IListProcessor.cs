﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Parameters.Interface
{
    public interface IListProcessor
    {
        int GetElementAt(int input);
        int GetListLength();

        List<int> GetList();
        List<int> GetVioletFirstList();
    }
}
