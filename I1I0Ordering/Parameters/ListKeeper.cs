﻿using I1I0Ordering.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Parameters
{
    public class ListKeeper
    {
        private static List<Steps> Tuplelist = new List<Steps>();
        public static List<int> StepsList = null;
        public static List<int> VioletFirstLinesList = null;
        public static List<int> OrangeFirstLinesList = null;
        public static List<int> BlackFirstLinesList = null;
        public static List<int> YellowFirstLinesList = null;
        public static List<int> CyanFirstLinesList = null;

        public ListKeeper()
        {
            StepsList = new List<int>()
        {
         64,
         64,
         76,
         76,
         40,
         40,
         76,
         76,
         64,
         64,
         52,
         52,
         64,
         64,
         76,
         76,
         40,
         40,
         76,
         76,
         64,
         64,
         52,
         52
};

            VioletFirstLinesList = new List<int>()
            {
                1,
                13,
                25,
                37,
                49,
                61
            };
        }

        public static List<int> createVioletD4List()
        {
         return   VioletFirstLinesList = new List<int>()
            {
                1,
                13,
                25,
                37,
                49,
                61
            };

        }

        public static List<int> createVioletD3List()
        {
            return VioletFirstLinesList = new List<int>()
            {
                2,
                14,
                26,
                38,
                50,
                62
            };

        }

        public static List<int> createOrangeD4List()
        {
            return OrangeFirstLinesList = new List<int>()
            {
                3,
                15,
                27,
                39,
                51,
                63
            };

        }

        public static List<int> createOrangeD3List()
        {
            return OrangeFirstLinesList = new List<int>()
            {
                4,
                16,
                28,
                40,
                52,
                64
            };

        }

        public static List<int> createBlackD4List()
        {
            return BlackFirstLinesList = new List<int>()
            {
                5,
                17,
                29,
                41,
                53,
                65
            };

        }
        public static List<int> createBlackD3List()
        {
            return BlackFirstLinesList = new List<int>()
            {
                6,
                18,
                30,
                42,
                54,
                66
            };

        }

        public static List<int> createYellowD4List()
        {
            return YellowFirstLinesList = new List<int>()
            {
                7,
                19,
                31,
                43,
                55,
                67
            };

        }
        public static List<int> createYellowD3List()
        {
            return YellowFirstLinesList = new List<int>()
            {
                8,
                20,
                32,
                44,
                56,
                68
            };

        }
        public static List<int> createMagentaD4List()
        {
            return YellowFirstLinesList = new List<int>()
            {
                9,
                21,
                33,
                45,
                57,
                69
            };

        }

        public static List<int> createMagentaD3List()
        {
            return YellowFirstLinesList = new List<int>()
            {
                10,
                22,
                34,
                46,
                58,
                70
            };

        }
        public static List<int> createCyanD4List()
        {
            return CyanFirstLinesList = new List<int>()
            {
                11,
                23,
                35,
                47,
                59,
                71
            };

        }
        public static List<int> createCyanD3List()
        {
            return CyanFirstLinesList = new List<int>()
            {
                12,
                36,
                48,
                60,
                72
            };

        }

        public static int GetElementAtList(int input)
        {
            int element = StepsList.ElementAt(input);
            return element;
        }

        public static int ListLength()
        {
            int length;
            length = StepsList.Count();
            return length;
        }
        


    }
}
