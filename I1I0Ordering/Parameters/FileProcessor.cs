﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Parameters
{
   public class FileProcessor: IFileProcessor
    {
        public string GetBlankFileLocation()
        {
            return FileKeeper.BlankFileLocation ;
        }
        public void SetBlankFileLocation(string BlankFileLocationToSet)
        {
            FileKeeper.BlankFileLocation = BlankFileLocationToSet;
        }


        public string checkFile(string FileName)
        {
            string Output = "OK";
            try {

                FileInfo fInfo = new FileInfo(FileName);
                if (!fInfo.Exists)
                {
                    throw new FileNotFoundException("The file was not found.", FileName);
                }

                if (new FileInfo(FileName).Length == 0)
                {
                    throw new ArgumentNullException("The file is empty");
                }
            }
            catch (FileNotFoundException)
            {
                Output = "NotFound";
            }
            catch (ArgumentNullException )
            {
                Output = "Empty";
            }

            return Output;
        }


        public string GetLogFileLocation()
        {
            return FileKeeper.LogFileLocation;
        }
        public void SetLogFileLocation(string LogFileLocationToSet)
        {
            FileKeeper.LogFileLocation = LogFileLocationToSet;
        }

        
    }
}
 