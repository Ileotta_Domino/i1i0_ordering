﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Parameters
{
    public class HeadValueKeeper
    {
        public static double VioletHead3Segment1;
        public static double VioletHead3Segment2;
        public static double VioletHead3Segment3;
        public static double VioletHead3Segment4;
        public static double VioletHead2Segment1;
        public static double VioletHead2Segment2;
        public static double VioletHead2Segment3;
        public static double VioletHead2Segment4;
        public static double VioletHead1Segment1;
        public static double VioletHead1Segment2;
        public static double VioletHead1Segment3;
        public static double VioletHead1Segment4;

        public static double Violet2Head3Segment1;
        public static double Violet2Head3Segment2;
        public static double Violet2Head3Segment3;
        public static double Violet2Head3Segment4;
        public static double Violet2Head2Segment1;
        public static double Violet2Head2Segment2;
        public static double Violet2Head2Segment3;
        public static double Violet2Head2Segment4;
        public static double Violet2Head1Segment1;
        public static double Violet2Head1Segment2;
        public static double Violet2Head1Segment3;
        public static double Violet2Head1Segment4;

        public static double OrangeHead3Segment1;
        public static double OrangeHead3Segment2;
        public static double OrangeHead3Segment3;
        public static double OrangeHead3Segment4;
        public static double OrangeHead2Segment1;
        public static double OrangeHead2Segment2;
        public static double OrangeHead2Segment3;
        public static double OrangeHead2Segment4;
        public static double OrangeHead1Segment1;
        public static double OrangeHead1Segment2;
        public static double OrangeHead1Segment3;
        public static double OrangeHead1Segment4;

        public static double Orange2Head3Segment1;
        public static double Orange2Head3Segment2;
        public static double Orange2Head3Segment3;
        public static double Orange2Head3Segment4;
        public static double Orange2Head2Segment1;
        public static double Orange2Head2Segment2;
        public static double Orange2Head2Segment3;
        public static double Orange2Head2Segment4;
        public static double Orange2Head1Segment1;
        public static double Orange2Head1Segment2;
        public static double Orange2Head1Segment3;
        public static double Orange2Head1Segment4;

        public static double BlackHead3Segment1;
        public static double BlackHead3Segment2;
        public static double BlackHead3Segment3;
        public static double BlackHead3Segment4;
        public static double BlackHead2Segment1;
        public static double BlackHead2Segment2;
        public static double BlackHead2Segment3;
        public static double BlackHead2Segment4;
        public static double BlackHead1Segment1;
        public static double BlackHead1Segment2;
        public static double BlackHead1Segment3;
        public static double BlackHead1Segment4;

        public static double Black2Head3Segment1;
        public static double Black2Head3Segment2;
        public static double Black2Head3Segment3;
        public static double Black2Head3Segment4;
        public static double Black2Head2Segment1;
        public static double Black2Head2Segment2;
        public static double Black2Head2Segment3;
        public static double Black2Head2Segment4;
        public static double Black2Head1Segment1;
        public static double Black2Head1Segment2;
        public static double Black2Head1Segment3;
        public static double Black2Head1Segment4;

        public static double YellowHead3Segment1;
        public static double YellowHead3Segment2;
        public static double YellowHead3Segment3;
        public static double YellowHead3Segment4;
        public static double YellowHead2Segment1;
        public static double YellowHead2Segment2;
        public static double YellowHead2Segment3;
        public static double YellowHead2Segment4;
        public static double YellowHead1Segment1;
        public static double YellowHead1Segment2;
        public static double YellowHead1Segment3;
        public static double YellowHead1Segment4;

        public static double Yellow2Head3Segment1;
        public static double Yellow2Head3Segment2;
        public static double Yellow2Head3Segment3;
        public static double Yellow2Head3Segment4;
        public static double Yellow2Head2Segment1;
        public static double Yellow2Head2Segment2;
        public static double Yellow2Head2Segment3;
        public static double Yellow2Head2Segment4;
        public static double Yellow2Head1Segment1;
        public static double Yellow2Head1Segment2;
        public static double Yellow2Head1Segment3;
        public static double Yellow2Head1Segment4;

        public static double MagentaHead3Segment1;
        public static double MagentaHead3Segment2;
        public static double MagentaHead3Segment3;
        public static double MagentaHead3Segment4;
        public static double MagentaHead2Segment1;
        public static double MagentaHead2Segment2;
        public static double MagentaHead2Segment3;
        public static double MagentaHead2Segment4;
        public static double MagentaHead1Segment1;
        public static double MagentaHead1Segment2;
        public static double MagentaHead1Segment3;
        public static double MagentaHead1Segment4;

        public static double Magenta2Head3Segment1;
        public static double Magenta2Head3Segment2;
        public static double Magenta2Head3Segment3;
        public static double Magenta2Head3Segment4;
        public static double Magenta2Head2Segment1;
        public static double Magenta2Head2Segment2;
        public static double Magenta2Head2Segment3;
        public static double Magenta2Head2Segment4;
        public static double Magenta2Head1Segment1;
        public static double Magenta2Head1Segment2;
        public static double Magenta2Head1Segment3;
        public static double Magenta2Head1Segment4;

        public static double CyanHead3Segment1;
        public static double CyanHead3Segment2;
        public static double CyanHead3Segment3;
        public static double CyanHead3Segment4;
        public static double CyanHead2Segment1;
        public static double CyanHead2Segment2;
        public static double CyanHead2Segment3;
        public static double CyanHead2Segment4;
        public static double CyanHead1Segment1;
        public static double CyanHead1Segment2;
        public static double CyanHead1Segment3;
        public static double CyanHead1Segment4;


        public static double Cyan2Head3Segment1;
        public static double Cyan2Head3Segment2;
        public static double Cyan2Head3Segment3;
        public static double Cyan2Head3Segment4;
        public static double Cyan2Head2Segment1;
        public static double Cyan2Head2Segment2;
        public static double Cyan2Head2Segment3;
        public static double Cyan2Head2Segment4;
        public static double Cyan2Head1Segment1;
        public static double Cyan2Head1Segment2;
        public static double Cyan2Head1Segment3;
        public static double Cyan2Head1Segment4;

        public static double CurrentVoltageMagenta;
        public static double CurrentVoltageViolet;
        public static double CurrentVoltageCyan;
        public static double CurrentVoltageBlack;
        public static double CurrentVoltageYellow;
        public static double CurrentVoltageOrange;
        /////////////////////////////////////////
        ///Violet
        /////////////////////////////////////////

        /// <summary>
        /// Head3
        /// </summary>
        public static double VioletHead3Segment1Value
        {
            get { return VioletHead3Segment1; }
            set { VioletHead3Segment1 = value; }
        }
        public static double VioletHead3Segment2Value
        {
            get { return VioletHead3Segment2; }
            set { VioletHead3Segment2 = value; }
        }
        public static double VioletHead3Segment3Value
        {
            get { return VioletHead3Segment3; }
            set { VioletHead3Segment3 = value; }
        }
        public static double VioletHead3Segment4Value
        {
            get { return VioletHead3Segment4; }
            set { VioletHead3Segment4 = value; }
        }
        /// <summary>
        /// Head 2
        /// </summary>
        public static double VioletHead2Segment1Value
        {
            get { return VioletHead2Segment1; }
            set { VioletHead2Segment1 = value; }
        }
        public static double VioletHead2Segment2Value
        {
            get { return VioletHead2Segment2; }
            set { VioletHead2Segment2 = value; }
        }
        public static double VioletHead2Segment3Value
        {
            get { return VioletHead2Segment3; }
            set { VioletHead2Segment3 = value; }
        }
        public static double VioletHead2Segment4Value
        {
            get { return VioletHead2Segment4; }
            set { VioletHead2Segment4 = value; }
        }
        /// <summary>
        /// Head1
        /// </summary>
        public static double VioletHead1Segment1Value
        {
            get { return VioletHead1Segment1; }
            set { VioletHead1Segment1 = value; }
        }
        public static double VioletHead1Segment2Value
        {
            get { return VioletHead1Segment2; }
            set { VioletHead1Segment2 = value; }
        }
        public static double VioletHead1Segment3Value
        {
            get { return VioletHead1Segment3; }
            set { VioletHead1Segment3 = value; }
        }
        public static double VioletHead1Segment4Value
        {
            get { return VioletHead1Segment4; }
            set { VioletHead1Segment4 = value; }
        }

        /////////////////////////////////////////
        ///Violet2
        /////////////////////////////////////////

        /// <summary>
        /// Head3
        /// </summary>
        public static double Violet2Head3Segment1Value
        {
            get { return Violet2Head3Segment1; }
            set { Violet2Head3Segment1 = value; }
        }
        public static double Violet2Head3Segment2Value
        {
            get { return Violet2Head3Segment2; }
            set { Violet2Head3Segment2 = value; }
        }
        public static double Violet2Head3Segment3Value
        {
            get { return Violet2Head3Segment3; }
            set { Violet2Head3Segment3 = value; }
        }
        public static double Violet2Head3Segment4Value
        {
            get { return Violet2Head3Segment4; }
            set { Violet2Head3Segment4 = value; }
        }
        /// <summary>
        /// Head 2
        /// </summary>
        public static double Violet2Head2Segment1Value
        {
            get { return Violet2Head2Segment1; }
            set { Violet2Head2Segment1 = value; }
        }
        public static double Violet2Head2Segment2Value
        {
            get { return Violet2Head2Segment2; }
            set { Violet2Head2Segment2 = value; }
        }
        public static double Violet2Head2Segment3Value
        {
            get { return Violet2Head2Segment3; }
            set { Violet2Head2Segment3 = value; }
        }
        public static double Violet2Head2Segment4Value
        {
            get { return Violet2Head2Segment4; }
            set { Violet2Head2Segment4 = value; }
        }
        /// <summary>
        /// Head1
        /// </summary>
        public static double Violet2Head1Segment1Value
        {
            get { return Violet2Head1Segment1; }
            set { Violet2Head1Segment1 = value; }
        }
        public static double Violet2Head1Segment2Value
        {
            get { return Violet2Head1Segment2; }
            set { Violet2Head1Segment2 = value; }
        }
        public static double Violet2Head1Segment3Value
        {
            get { return Violet2Head1Segment3; }
            set { Violet2Head1Segment3 = value; }
        }
        public static double Violet2Head1Segment4Value
        {
            get { return Violet2Head1Segment4; }
            set { Violet2Head1Segment4 = value; }
        }

        /////////////////////////////////////////
        ///Orange
        /////////////////////////////////////////
        /// <summary>
        /// Head3
        /// </summary>
        public static double OrangeHead3Segment1Value
        {
            get { return OrangeHead3Segment1; }
            set { OrangeHead3Segment1 = value; }
        }
        public static double OrangeHead3Segment2Value
        {
            get { return OrangeHead3Segment2; }
            set { OrangeHead3Segment2 = value; }
        }
        public static double OrangeHead3Segment3Value
        {
            get { return OrangeHead3Segment3; }
            set { OrangeHead3Segment3 = value; }
        }
        public static double OrangeHead3Segment4Value
        {
            get { return OrangeHead3Segment4; }
            set { OrangeHead3Segment4 = value; }
        }
        /// <summary>
        /// Head 2
        /// </summary>
        public static double OrangeHead2Segment1Value
        {
            get { return OrangeHead2Segment1; }
            set { OrangeHead2Segment1 = value; }
        }
        public static double OrangeHead2Segment2Value
        {
            get { return OrangeHead2Segment2; }
            set { OrangeHead2Segment2 = value; }
        }
        public static double OrangeHead2Segment3Value
        {
            get { return OrangeHead2Segment3; }
            set { OrangeHead2Segment3 = value; }
        }
        public static double OrangeHead2Segment4Value
        {
            get { return OrangeHead2Segment4; }
            set { OrangeHead2Segment4 = value; }
        }
        /// <summary>
        /// Head1
        /// </summary>
        public static double OrangeHead1Segment1Value
        {
            get { return OrangeHead1Segment1; }
            set { OrangeHead1Segment1 = value; }
        }
        public static double OrangeHead1Segment2Value
        {
            get { return OrangeHead1Segment2; }
            set { OrangeHead1Segment2 = value; }
        }
        public static double OrangeHead1Segment3Value
        {
            get { return OrangeHead1Segment3; }
            set { OrangeHead1Segment3 = value; }
        }
        public static double OrangeHead1Segment4Value
        {
            get { return OrangeHead1Segment4; }
            set { OrangeHead1Segment4 = value; }
        }

        /////////////////////////////////////////
        ///Orange2
        /////////////////////////////////////////
        /// <summary>
        /// Head3
        /// </summary>
        public static double Orange2Head3Segment1Value
        {
            get { return Orange2Head3Segment1; }
            set { Orange2Head3Segment1 = value; }
        }
        public static double Orange2Head3Segment2Value
        {
            get { return Orange2Head3Segment2; }
            set { Orange2Head3Segment2 = value; }
        }
        public static double Orange2Head3Segment3Value
        {
            get { return Orange2Head3Segment3; }
            set { Orange2Head3Segment3 = value; }
        }
        public static double Orange2Head3Segment4Value
        {
            get { return Orange2Head3Segment4; }
            set { Orange2Head3Segment4 = value; }
        }
        /// <summary>
        /// Head 2
        /// </summary>
        public static double Orange2Head2Segment1Value
        {
            get { return Orange2Head2Segment1; }
            set { Orange2Head2Segment1 = value; }
        }
        public static double Orange2Head2Segment2Value
        {
            get { return Orange2Head2Segment2; }
            set { Orange2Head2Segment2 = value; }
        }
        public static double Orange2Head2Segment3Value
        {
            get { return Orange2Head2Segment3; }
            set { Orange2Head2Segment3 = value; }
        }
        public static double Orange2Head2Segment4Value
        {
            get { return Orange2Head2Segment4; }
            set { Orange2Head2Segment4 = value; }
        }
        /// <summary>
        /// Head1
        /// </summary>
        public static double Orange2Head1Segment1Value
        {
            get { return Orange2Head1Segment1; }
            set { Orange2Head1Segment1 = value; }
        }
        public static double Orange2Head1Segment2Value
        {
            get { return Orange2Head1Segment2; }
            set { Orange2Head1Segment2 = value; }
        }
        public static double Orange2Head1Segment3Value
        {
            get { return Orange2Head1Segment3; }
            set { Orange2Head1Segment3 = value; }
        }
        public static double Orange2Head1Segment4Value
        {
            get { return Orange2Head1Segment4; }
            set { Orange2Head1Segment4 = value; }
        }

        /////////////////////////////////////////
        ///Black
        /////////////////////////////////////////
        /// <summary>
        /// Head3
        /// </summary>
        public static double BlackHead3Segment1Value
        {
            get { return BlackHead3Segment1; }
            set { BlackHead3Segment1 = value; }
        }
        public static double BlackHead3Segment2Value
        {
            get { return BlackHead3Segment2; }
            set { BlackHead3Segment2 = value; }
        }
        public static double BlackHead3Segment3Value
        {
            get { return BlackHead3Segment3; }
            set { BlackHead3Segment3 = value; }
        }
        public static double BlackHead3Segment4Value
        {
            get { return BlackHead3Segment4; }
            set { BlackHead3Segment4 = value; }
        }
        /// <summary>
        /// Head 2
        /// </summary>
        public static double BlackHead2Segment1Value
        {
            get { return BlackHead2Segment1; }
            set { BlackHead2Segment1 = value; }
        }
        public static double BlackHead2Segment2Value
        {
            get { return BlackHead2Segment2; }
            set { BlackHead2Segment2 = value; }
        }
        public static double BlackHead2Segment3Value
        {
            get { return BlackHead2Segment3; }
            set { BlackHead2Segment3 = value; }
        }
        public static double BlackHead2Segment4Value
        {
            get { return BlackHead2Segment4; }
            set { BlackHead2Segment4 = value; }
        }
        /// <summary>
        /// Head1
        /// </summary>
        public static double BlackHead1Segment1Value
        {
            get { return BlackHead1Segment1; }
            set { BlackHead1Segment1 = value; }
        }
        public static double BlackHead1Segment2Value
        {
            get { return BlackHead1Segment2; }
            set { BlackHead1Segment2 = value; }
        }
        public static double BlackHead1Segment3Value
        {
            get { return BlackHead1Segment3; }
            set { BlackHead1Segment3 = value; }
        }
        public static double BlackHead1Segment4Value
        {
            get { return BlackHead1Segment4; }
            set { BlackHead1Segment4 = value; }
        }


        /////////////////////////////////////////
        ///Black2
        /////////////////////////////////////////
        /// <summary>
        /// Head3
        /// </summary>
        public static double Black2Head3Segment1Value
        {
            get { return Black2Head3Segment1; }
            set { Black2Head3Segment1 = value; }
        }
        public static double Black2Head3Segment2Value
        {
            get { return Black2Head3Segment2; }
            set { Black2Head3Segment2 = value; }
        }
        public static double Black2Head3Segment3Value
        {
            get { return Black2Head3Segment3; }
            set { Black2Head3Segment3 = value; }
        }
        public static double Black2Head3Segment4Value
        {
            get { return Black2Head3Segment4; }
            set { Black2Head3Segment4 = value; }
        }
        /// <summary>
        /// Head 2
        /// </summary>
        public static double Black2Head2Segment1Value
        {
            get { return Black2Head2Segment1; }
            set { Black2Head2Segment1 = value; }
        }
        public static double Black2Head2Segment2Value
        {
            get { return Black2Head2Segment2; }
            set { Black2Head2Segment2 = value; }
        }
        public static double Black2Head2Segment3Value
        {
            get { return Black2Head2Segment3; }
            set { Black2Head2Segment3 = value; }
        }
        public static double Black2Head2Segment4Value
        {
            get { return Black2Head2Segment4; }
            set { Black2Head2Segment4 = value; }
        }
        /// <summary>
        /// Head1
        /// </summary>
        public static double Black2Head1Segment1Value
        {
            get { return Black2Head1Segment1; }
            set { Black2Head1Segment1 = value; }
        }
        public static double Black2Head1Segment2Value
        {
            get { return Black2Head1Segment2; }
            set { Black2Head1Segment2 = value; }
        }
        public static double Black2Head1Segment3Value
        {
            get { return Black2Head1Segment3; }
            set { Black2Head1Segment3 = value; }
        }
        public static double Black2Head1Segment4Value
        {
            get { return Black2Head1Segment4; }
            set { Black2Head1Segment4 = value; }
        }

        /////////////////////////////////////////
        ///Yellow
        /////////////////////////////////////////
        /// <summary>
        /// Head3
        /// </summary>
        public static double YellowHead3Segment1Value
        {
            get { return YellowHead3Segment1; }
            set { YellowHead3Segment1 = value; }
        }
        public static double YellowHead3Segment2Value
        {
            get { return YellowHead3Segment2; }
            set { YellowHead3Segment2 = value; }
        }
        public static double YellowHead3Segment3Value
        {
            get { return YellowHead3Segment3; }
            set { YellowHead3Segment3 = value; }
        }
        public static double YellowHead3Segment4Value
        {
            get { return YellowHead3Segment4; }
            set { YellowHead3Segment4 = value; }
        }
        /// <summary>
        /// Head 2
        /// </summary>
        public static double YellowHead2Segment1Value
        {
            get { return YellowHead2Segment1; }
            set { YellowHead2Segment1 = value; }
        }
        public static double YellowHead2Segment2Value
        {
            get { return YellowHead2Segment2; }
            set { YellowHead2Segment2 = value; }
        }
        public static double YellowHead2Segment3Value
        {
            get { return YellowHead2Segment3; }
            set { YellowHead2Segment3 = value; }
        }
        public static double YellowHead2Segment4Value
        {
            get { return YellowHead2Segment4; }
            set { YellowHead2Segment4 = value; }
        }
        /// <summary>
        /// Head1
        /// </summary>
        public static double YellowHead1Segment1Value
        {
            get { return YellowHead1Segment1; }
            set { YellowHead1Segment1 = value; }
        }
        public static double YellowHead1Segment2Value
        {
            get { return YellowHead1Segment2; }
            set { YellowHead1Segment2 = value; }
        }
        public static double YellowHead1Segment3Value
        {
            get { return YellowHead1Segment3; }
            set { YellowHead1Segment3 = value; }
        }
        public static double YellowHead1Segment4Value
        {
            get { return YellowHead1Segment4; }
            set { YellowHead1Segment4 = value; }
        }

        /////////////////////////////////////////
        ///Yellow2
        /////////////////////////////////////////
        /// <summary>
        /// Head3
        /// </summary>
        public static double Yellow2Head3Segment1Value
        {
            get { return Yellow2Head3Segment1; }
            set { Yellow2Head3Segment1 = value; }
        }
        public static double Yellow2Head3Segment2Value
        {
            get { return Yellow2Head3Segment2; }
            set { Yellow2Head3Segment2 = value; }
        }
        public static double Yellow2Head3Segment3Value
        {
            get { return Yellow2Head3Segment3; }
            set { Yellow2Head3Segment3 = value; }
        }
        public static double Yellow2Head3Segment4Value
        {
            get { return Yellow2Head3Segment4; }
            set { Yellow2Head3Segment4 = value; }
        }
        /// <summary>
        /// Head 2
        /// </summary>
        public static double Yellow2Head2Segment1Value
        {
            get { return Yellow2Head2Segment1; }
            set { Yellow2Head2Segment1 = value; }
        }
        public static double Yellow2Head2Segment2Value
        {
            get { return Yellow2Head2Segment2; }
            set { Yellow2Head2Segment2 = value; }
        }
        public static double Yellow2Head2Segment3Value
        {
            get { return Yellow2Head2Segment3; }
            set { Yellow2Head2Segment3 = value; }
        }
        public static double Yellow2Head2Segment4Value
        {
            get { return Yellow2Head2Segment4; }
            set { Yellow2Head2Segment4 = value; }
        }
        /// <summary>
        /// Head1
        /// </summary>
        public static double Yellow2Head1Segment1Value
        {
            get { return Yellow2Head1Segment1; }
            set { Yellow2Head1Segment1 = value; }
        }
        public static double Yellow2Head1Segment2Value
        {
            get { return Yellow2Head1Segment2; }
            set { Yellow2Head1Segment2 = value; }
        }
        public static double Yellow2Head1Segment3Value
        {
            get { return Yellow2Head1Segment3; }
            set { Yellow2Head1Segment3 = value; }
        }
        public static double Yellow2Head1Segment4Value
        {
            get { return Yellow2Head1Segment4; }
            set { Yellow2Head1Segment4 = value; }
        }

        /////////////////////////////////////////
        ///Magenta
        /////////////////////////////////////////
        /// <summary>
        /// Head3
        /// </summary>
        public static double MagentaHead3Segment1Value
        {
            get { return MagentaHead3Segment1; }
            set { MagentaHead3Segment1 = value; }
        }
        public static double MagentaHead3Segment2Value
        {
            get { return MagentaHead3Segment2; }
            set { MagentaHead3Segment2 = value; }
        }
        public static double MagentaHead3Segment3Value
        {
            get { return MagentaHead3Segment3; }
            set { MagentaHead3Segment3 = value; }
        }
        public static double MagentaHead3Segment4Value
        {
            get { return MagentaHead3Segment4; }
            set { MagentaHead3Segment4 = value; }
        }
        /// <summary>
        /// Head 2
        /// </summary>
        public static double MagentaHead2Segment1Value
        {
            get { return MagentaHead2Segment1; }
            set { MagentaHead2Segment1 = value; }
        }
        public static double MagentaHead2Segment2Value
        {
            get { return MagentaHead2Segment2; }
            set { MagentaHead2Segment2 = value; }
        }
        public static double MagentaHead2Segment3Value
        {
            get { return MagentaHead2Segment3; }
            set { MagentaHead2Segment3 = value; }
        }
        public static double MagentaHead2Segment4Value
        {
            get { return MagentaHead2Segment4; }
            set { MagentaHead2Segment4 = value; }
        }
        /// <summary>
        /// Head1
        /// </summary>
        public static double MagentaHead1Segment1Value
        {
            get { return MagentaHead1Segment1; }
            set { MagentaHead1Segment1 = value; }
        }
        public static double MagentaHead1Segment2Value
        {
            get { return MagentaHead1Segment2; }
            set { MagentaHead1Segment2 = value; }
        }
        public static double MagentaHead1Segment3Value
        {
            get { return MagentaHead1Segment3; }
            set { MagentaHead1Segment3 = value; }
        }
        public static double MagentaHead1Segment4Value
        {
            get { return MagentaHead1Segment4; }
            set { MagentaHead1Segment4 = value; }
        }

        /////////////////////////////////////////
        ///Magenta2
        /////////////////////////////////////////
        /// <summary>
        /// Head3
        /// </summary>
        public static double Magenta2Head3Segment1Value
        {
            get { return Magenta2Head3Segment1; }
            set { Magenta2Head3Segment1 = value; }
        }
        public static double Magenta2Head3Segment2Value
        {
            get { return Magenta2Head3Segment2; }
            set { Magenta2Head3Segment2 = value; }
        }
        public static double Magenta2Head3Segment3Value
        {
            get { return Magenta2Head3Segment3; }
            set { Magenta2Head3Segment3 = value; }
        }
        public static double Magenta2Head3Segment4Value
        {
            get { return Magenta2Head3Segment4; }
            set { Magenta2Head3Segment4 = value; }
        }
        /// <summary>
        /// Head 2
        /// </summary>
        public static double Magenta2Head2Segment1Value
        {
            get { return Magenta2Head2Segment1; }
            set { Magenta2Head2Segment1 = value; }
        }
        public static double Magenta2Head2Segment2Value
        {
            get { return Magenta2Head2Segment2; }
            set { Magenta2Head2Segment2 = value; }
        }
        public static double Magenta2Head2Segment3Value
        {
            get { return Magenta2Head2Segment3; }
            set { Magenta2Head2Segment3 = value; }
        }
        public static double Magenta2Head2Segment4Value
        {
            get { return Magenta2Head2Segment4; }
            set { Magenta2Head2Segment4 = value; }
        }
        /// <summary>
        /// Head1
        /// </summary>
        public static double Magenta2Head1Segment1Value
        {
            get { return Magenta2Head1Segment1; }
            set { Magenta2Head1Segment1 = value; }
        }
        public static double Magenta2Head1Segment2Value
        {
            get { return Magenta2Head1Segment2; }
            set { Magenta2Head1Segment2 = value; }
        }
        public static double Magenta2Head1Segment3Value
        {
            get { return Magenta2Head1Segment3; }
            set { Magenta2Head1Segment3 = value; }
        }
        public static double Magenta2Head1Segment4Value
        {
            get { return Magenta2Head1Segment4; }
            set { Magenta2Head1Segment4 = value; }
        }
        /////////////////////////////////////////
        ///Cyan
        /////////////////////////////////////////
        /// <summary>
        /// Head3
        /// </summary>
        public static double CyanHead3Segment1Value
        {
            get { return CyanHead3Segment1; }
            set { CyanHead3Segment1 = value; }
        }
        public static double CyanHead3Segment2Value
        {
            get { return CyanHead3Segment2; }
            set { CyanHead3Segment2 = value; }
        }
        public static double CyanHead3Segment3Value
        {
            get { return CyanHead3Segment3; }
            set { CyanHead3Segment3 = value; }
        }
        public static double CyanHead3Segment4Value
        {
            get { return CyanHead3Segment4; }
            set { CyanHead3Segment4 = value; }
        }
        /// <summary>
        /// Head 2
        /// </summary>
        public static double CyanHead2Segment1Value
        {
            get { return CyanHead2Segment1; }
            set { CyanHead2Segment1 = value; }
        }
        public static double CyanHead2Segment2Value
        {
            get { return CyanHead2Segment2; }
            set { CyanHead2Segment2 = value; }
        }
        public static double CyanHead2Segment3Value
        {
            get { return CyanHead2Segment3; }
            set { CyanHead2Segment3 = value; }
        }
        public static double CyanHead2Segment4Value
        {
            get { return CyanHead2Segment4; }
            set { CyanHead2Segment4 = value; }
        }
        /// <summary>
        /// Head1
        /// </summary>
        public static double CyanHead1Segment1Value
        {
            get { return CyanHead1Segment1; }
            set { CyanHead1Segment1 = value; }
        }
        public static double CyanHead1Segment2Value
        {
            get { return CyanHead1Segment2; }
            set { CyanHead1Segment2 = value; }
        }
        public static double CyanHead1Segment3Value
        {
            get { return CyanHead1Segment3; }
            set { CyanHead1Segment3 = value; }
        }
        public static double CyanHead1Segment4Value
        {
            get { return CyanHead1Segment4; }
            set { CyanHead1Segment4 = value; }
        }

        /////////////////////////////////////////
        ///Cyan2
        /////////////////////////////////////////
        /// <summary>
        /// Head3
        /// </summary>
        public static double Cyan2Head3Segment1Value
        {
            get { return Cyan2Head3Segment1; }
            set { Cyan2Head3Segment1 = value; }
        }
        public static double Cyan2Head3Segment2Value
        {
            get { return Cyan2Head3Segment2; }
            set { Cyan2Head3Segment2 = value; }
        }
        public static double Cyan2Head3Segment3Value
        {
            get { return Cyan2Head3Segment3; }
            set { Cyan2Head3Segment3 = value; }
        }
        public static double Cyan2Head3Segment4Value
        {
            get { return Cyan2Head3Segment4; }
            set { Cyan2Head3Segment4 = value; }
        }
        /// <summary>
        /// Head 2
        /// </summary>
        public static double Cyan2Head2Segment1Value
        {
            get { return Cyan2Head2Segment1; }
            set { Cyan2Head2Segment1 = value; }
        }
        public static double Cyan2Head2Segment2Value
        {
            get { return Cyan2Head2Segment2; }
            set { Cyan2Head2Segment2 = value; }
        }
        public static double Cyan2Head2Segment3Value
        {
            get { return Cyan2Head2Segment3; }
            set { Cyan2Head2Segment3 = value; }
        }
        public static double Cyan2Head2Segment4Value
        {
            get { return Cyan2Head2Segment4; }
            set { Cyan2Head2Segment4 = value; }
        }
        /// <summary>
        /// Head1
        /// </summary>
        public static double Cyan2Head1Segment1Value
        {
            get { return Cyan2Head1Segment1; }
            set { Cyan2Head1Segment1 = value; }
        }
        public static double Cyan2Head1Segment2Value
        {
            get { return Cyan2Head1Segment2; }
            set { Cyan2Head1Segment2 = value; }
        }
        public static double Cyan2Head1Segment3Value
        {
            get { return Cyan2Head1Segment3; }
            set { Cyan2Head1Segment3 = value; }
        }
        public static double Cyan2Head1Segment4Value
        {
            get { return Cyan2Head1Segment4; }
            set { Cyan2Head1Segment4 = value; }
        }

        public static double CurrentVoltageMagentaValue
        {
            get { return CurrentVoltageMagenta; }
            set { CurrentVoltageMagenta = value; }
        }
        public static double CurrentVoltageVioletValue
        {
            get { return CurrentVoltageViolet; }
            set { CurrentVoltageViolet = value; }
        }
        public static double CurrentVoltageCyanValue
        {
            get { return CurrentVoltageCyan; }
            set { CurrentVoltageCyan = value; }
        }
        public static double CurrentVoltageBlackValue
        {
            get { return CurrentVoltageBlack; }
            set { CurrentVoltageBlack = value; }
        }
        public static double CurrentVoltageYellowValue
        {
            get { return CurrentVoltageYellow; }
            set { CurrentVoltageYellow = value; }
        }
        public static double CurrentVoltageOrangeValue
        {
            get { return CurrentVoltageOrange; }
            set { CurrentVoltageOrange = value; }
        }


    }
} 
