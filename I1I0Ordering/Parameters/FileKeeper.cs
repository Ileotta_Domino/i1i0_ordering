﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Parameters
{
   public class FileKeeper
    {
        string path= Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        static string filename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\i1iO_UV90_CMYKOV_version4_00_3Head.xlsx";
        static double fiddleFactor;
        static double targetValue;

        public static string BlankFileLocation
        {
            get { return filename; }
            set { filename = value; }
        }
        public static string LogFileLocation
        {
            get { return filename; }
            set { filename = value; }
        }

        public static double FiddleFactor
        {
            get { return fiddleFactor; }
            set { fiddleFactor = value; }
        }

        public static double Target
        {
            get { return targetValue; }
            set { targetValue = value; }
        }

    }
}
