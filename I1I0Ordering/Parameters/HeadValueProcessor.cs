﻿using I1I0Ordering.Parameters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I1I0Ordering.Parameters
{
    public class HeadValueProcessor: IHeadValueProcessor
    {
        ////////////////////////////////////////////////
        ///Violet
        ///////////////////////////////////////////////
        /// <summary>
        /// Head 3 
        /// </summary>
        /// <returns></returns>
        public double GetVioletHead3Segment1Value()
        {return HeadValueKeeper.VioletHead3Segment1Value; }
        public void SetVioletHead3Segment1Value(double input)
        {  HeadValueKeeper.VioletHead3Segment1Value=input; }
        public double GetVioletHead3Segment2Value()
        { return HeadValueKeeper.VioletHead3Segment2Value; }
        public void SetVioletHead3Segment2Value(double input)
        { HeadValueKeeper.VioletHead3Segment2Value = input; }
        public double GetVioletHead3Segment3Value()
        { return HeadValueKeeper.VioletHead3Segment3Value; }
        public void SetVioletHead3Segment3Value(double input)
        { HeadValueKeeper.VioletHead3Segment3Value = input; }
        public double GetVioletHead3Segment4Value()
        { return HeadValueKeeper.VioletHead3Segment4Value; }
        public void SetVioletHead3Segment4Value(double input)
        { HeadValueKeeper.VioletHead3Segment4Value = input; }


       /// <summary>
       /// Head2
       /// </summary>
       /// <returns></returns>
        public double GetVioletHead2Segment1Value()
        { return HeadValueKeeper.VioletHead2Segment1Value; }
        public void SetVioletHead2Segment1Value(double input)
        { HeadValueKeeper.VioletHead2Segment1Value = input; }
        public double GetVioletHead2Segment2Value()
        { return HeadValueKeeper.VioletHead2Segment2Value; }
        public void SetVioletHead2Segment2Value(double input)
        { HeadValueKeeper.VioletHead2Segment2Value = input; }
        public double GetVioletHead2Segment3Value()
        { return HeadValueKeeper.VioletHead2Segment3Value; }
        public void SetVioletHead2Segment3Value(double input)
        { HeadValueKeeper.VioletHead2Segment3Value = input; }
        public double GetVioletHead2Segment4Value()
        { return HeadValueKeeper.VioletHead2Segment4Value; }
        public void SetVioletHead2Segment4Value(double input)
        { HeadValueKeeper.VioletHead2Segment4Value = input; }

        /// <summary>
        /// Head1
        /// </summary>
        /// <returns></returns>
        public double GetVioletHead1Segment1Value()
        { return HeadValueKeeper.VioletHead1Segment1Value; }
        public void SetVioletHead1Segment1Value(double input)
        { HeadValueKeeper.VioletHead1Segment1Value = input; }
        public double GetVioletHead1Segment2Value()
        { return HeadValueKeeper.VioletHead1Segment2Value; }
        public void SetVioletHead1Segment2Value(double input)
        { HeadValueKeeper.VioletHead1Segment2Value = input; }
        public double GetVioletHead1Segment3Value()
        { return HeadValueKeeper.VioletHead1Segment3Value; }
        public void SetVioletHead1Segment3Value(double input)
        { HeadValueKeeper.VioletHead1Segment3Value = input; }
        public double GetVioletHead1Segment4Value()
        { return HeadValueKeeper.VioletHead1Segment4Value; }
        public void SetVioletHead1Segment4Value(double input)
        { HeadValueKeeper.VioletHead1Segment4Value = input; }

        ////////////////////////////////////////////////
        ///Violet2
        ///////////////////////////////////////////////
        /// <summary>
        /// Head 3 
        /// </summary>
        /// <returns></returns>
        public double GetViolet2Head3Segment1Value()
        { return HeadValueKeeper.Violet2Head3Segment1Value; }
        public void SetViolet2Head3Segment1Value(double input)
        { HeadValueKeeper.Violet2Head3Segment1Value = input; }
        public double GetViolet2Head3Segment2Value()
        { return HeadValueKeeper.Violet2Head3Segment2Value; }
        public void SetViolet2Head3Segment2Value(double input)
        { HeadValueKeeper.Violet2Head3Segment2Value = input; }
        public double GetViolet2Head3Segment3Value()
        { return HeadValueKeeper.Violet2Head3Segment3Value; }
        public void SetViolet2Head3Segment3Value(double input)
        { HeadValueKeeper.Violet2Head3Segment3Value = input; }
        public double GetViolet2Head3Segment4Value()
        { return HeadValueKeeper.Violet2Head3Segment4Value; }
        public void SetViolet2Head3Segment4Value(double input)
        { HeadValueKeeper.Violet2Head3Segment4Value = input; }


        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        public double GetViolet2Head2Segment1Value()
        { return HeadValueKeeper.Violet2Head2Segment1Value; }
        public void SetViolet2Head2Segment1Value(double input)
        { HeadValueKeeper.Violet2Head2Segment1Value = input; }
        public double GetViolet2Head2Segment2Value()
        { return HeadValueKeeper.Violet2Head2Segment2Value; }
        public void SetViolet2Head2Segment2Value(double input)
        { HeadValueKeeper.Violet2Head2Segment2Value = input; }
        public double GetViolet2Head2Segment3Value()
        { return HeadValueKeeper.Violet2Head2Segment3Value; }
        public void SetViolet2Head2Segment3Value(double input)
        { HeadValueKeeper.Violet2Head2Segment3Value = input; }
        public double GetViolet2Head2Segment4Value()
        { return HeadValueKeeper.Violet2Head2Segment4Value; }
        public void SetViolet2Head2Segment4Value(double input)
        { HeadValueKeeper.Violet2Head2Segment4Value = input; }

        /// <summary>
        /// Head1
        /// </summary>
        /// <returns></returns>
        public double GetViolet2Head1Segment1Value()
        { return HeadValueKeeper.Violet2Head1Segment1Value; }
        public void SetViolet2Head1Segment1Value(double input)
        { HeadValueKeeper.Violet2Head1Segment1Value = input; }
        public double GetViolet2Head1Segment2Value()
        { return HeadValueKeeper.Violet2Head1Segment2Value; }
        public void SetViolet2Head1Segment2Value(double input)
        { HeadValueKeeper.Violet2Head1Segment2Value = input; }
        public double GetViolet2Head1Segment3Value()
        { return HeadValueKeeper.Violet2Head1Segment3Value; }
        public void SetViolet2Head1Segment3Value(double input)
        { HeadValueKeeper.Violet2Head1Segment3Value = input; }
        public double GetViolet2Head1Segment4Value()
        { return HeadValueKeeper.Violet2Head1Segment4Value; }
        public void SetViolet2Head1Segment4Value(double input)
        { HeadValueKeeper.Violet2Head1Segment4Value = input; }

        ////////////////////////////////////////////////
        ///Orange
        ///////////////////////////////////////////////
        /// <summary>
        /// Head 3 
        /// </summary>
        /// <returns></returns>
        public double GetOrangeHead3Segment1Value()
        { return HeadValueKeeper.OrangeHead3Segment1Value; }
        public void SetOrangeHead3Segment1Value(double input)
        { HeadValueKeeper.OrangeHead3Segment1Value = input; }
        public double GetOrangeHead3Segment2Value()
        { return HeadValueKeeper.OrangeHead3Segment2Value; }
        public void SetOrangeHead3Segment2Value(double input)
        { HeadValueKeeper.OrangeHead3Segment2Value = input; }
        public double GetOrangeHead3Segment3Value()
        { return HeadValueKeeper.OrangeHead3Segment3Value; }
        public void SetOrangeHead3Segment3Value(double input)
        { HeadValueKeeper.OrangeHead3Segment3Value = input; }
        public double GetOrangeHead3Segment4Value()
        { return HeadValueKeeper.OrangeHead3Segment4Value; }
        public void SetOrangeHead3Segment4Value(double input)
        { HeadValueKeeper.OrangeHead3Segment4Value = input; }

        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        public double GetOrangeHead2Segment1Value()
        { return HeadValueKeeper.OrangeHead2Segment1Value; }
        public void SetOrangeHead2Segment1Value(double input)
        { HeadValueKeeper.OrangeHead2Segment1Value = input; }
        public double GetOrangeHead2Segment2Value()
        { return HeadValueKeeper.OrangeHead2Segment2Value; }
        public void SetOrangeHead2Segment2Value(double input)
        { HeadValueKeeper.OrangeHead2Segment2Value = input; }
        public double GetOrangeHead2Segment3Value()
        { return HeadValueKeeper.OrangeHead2Segment3Value; }
        public void SetOrangeHead2Segment3Value(double input)
        { HeadValueKeeper.OrangeHead2Segment3Value = input; }
        public double GetOrangeHead2Segment4Value()
        { return HeadValueKeeper.OrangeHead2Segment4Value; }
        public void SetOrangeHead2Segment4Value(double input)
        { HeadValueKeeper.OrangeHead2Segment4Value = input; }

        /// <summary>
        /// Head1
        /// </summary>
        /// <returns></returns>
        public double GetOrangeHead1Segment1Value()
        { return HeadValueKeeper.OrangeHead1Segment1Value; }
        public void SetOrangeHead1Segment1Value(double input)
        { HeadValueKeeper.OrangeHead1Segment1Value = input; }
        public double GetOrangeHead1Segment2Value()
        { return HeadValueKeeper.OrangeHead1Segment2Value; }
        public void SetOrangeHead1Segment2Value(double input)
        { HeadValueKeeper.OrangeHead1Segment2Value = input; }
        public double GetOrangeHead1Segment3Value()
        { return HeadValueKeeper.OrangeHead1Segment3Value; }
        public void SetOrangeHead1Segment3Value(double input)
        { HeadValueKeeper.OrangeHead1Segment3Value = input; }
        public double GetOrangeHead1Segment4Value()
        { return HeadValueKeeper.OrangeHead1Segment4Value; }
        public void SetOrangeHead1Segment4Value(double input)
        { HeadValueKeeper.OrangeHead1Segment4Value = input; }



        ////////////////////////////////////////////////
        ///Orange2
        ///////////////////////////////////////////////
        /// <summary>
        /// Head 3 
        /// </summary>
        /// <returns></returns>
        public double GetOrange2Head3Segment1Value()
        { return HeadValueKeeper.Orange2Head3Segment1Value; }
        public void SetOrange2Head3Segment1Value(double input)
        { HeadValueKeeper.Orange2Head3Segment1Value = input; }
        public double GetOrange2Head3Segment2Value()
        { return HeadValueKeeper.Orange2Head3Segment2Value; }
        public void SetOrange2Head3Segment2Value(double input)
        { HeadValueKeeper.Orange2Head3Segment2Value = input; }
        public double GetOrange2Head3Segment3Value()
        { return HeadValueKeeper.Orange2Head3Segment3Value; }
        public void SetOrange2Head3Segment3Value(double input)
        { HeadValueKeeper.Orange2Head3Segment3Value = input; }
        public double GetOrange2Head3Segment4Value()
        { return HeadValueKeeper.Orange2Head3Segment4Value; }
        public void SetOrange2Head3Segment4Value(double input)
        { HeadValueKeeper.Orange2Head3Segment4Value = input; }

        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        public double GetOrange2Head2Segment1Value()
        { return HeadValueKeeper.Orange2Head2Segment1Value; }
        public void SetOrange2Head2Segment1Value(double input)
        { HeadValueKeeper.Orange2Head2Segment1Value = input; }
        public double GetOrange2Head2Segment2Value()
        { return HeadValueKeeper.Orange2Head2Segment2Value; }
        public void SetOrange2Head2Segment2Value(double input)
        { HeadValueKeeper.Orange2Head2Segment2Value = input; }
        public double GetOrange2Head2Segment3Value()
        { return HeadValueKeeper.Orange2Head2Segment3Value; }
        public void SetOrange2Head2Segment3Value(double input)
        { HeadValueKeeper.Orange2Head2Segment3Value = input; }
        public double GetOrange2Head2Segment4Value()
        { return HeadValueKeeper.Orange2Head2Segment4Value; }
        public void SetOrange2Head2Segment4Value(double input)
        { HeadValueKeeper.Orange2Head2Segment4Value = input; }

        /// <summary>
        /// Head1
        /// </summary>
        /// <returns></returns>
        public double GetOrange2Head1Segment1Value()
        { return HeadValueKeeper.Orange2Head1Segment1Value; }
        public void SetOrange2Head1Segment1Value(double input)
        { HeadValueKeeper.Orange2Head1Segment1Value = input; }
        public double GetOrange2Head1Segment2Value()
        { return HeadValueKeeper.Orange2Head1Segment2Value; }
        public void SetOrange2Head1Segment2Value(double input)
        { HeadValueKeeper.Orange2Head1Segment2Value = input; }
        public double GetOrange2Head1Segment3Value()
        { return HeadValueKeeper.Orange2Head1Segment3Value; }
        public void SetOrange2Head1Segment3Value(double input)
        { HeadValueKeeper.Orange2Head1Segment3Value = input; }
        public double GetOrange2Head1Segment4Value()
        { return HeadValueKeeper.Orange2Head1Segment4Value; }
        public void SetOrange2Head1Segment4Value(double input)
        { HeadValueKeeper.Orange2Head1Segment4Value = input; }


        ////////////////////////////////////////////////
        ///Black
        ///////////////////////////////////////////////
        /// <summary>
        /// Head 3 
        /// </summary>
        /// <returns></returns>
        public double GetBlackHead3Segment1Value()
        { return HeadValueKeeper.BlackHead3Segment1Value; }
        public void SetBlackHead3Segment1Value(double input)
        { HeadValueKeeper.BlackHead3Segment1Value = input; }
        public double GetBlackHead3Segment2Value()
        { return HeadValueKeeper.BlackHead3Segment2Value; }
        public void SetBlackHead3Segment2Value(double input)
        { HeadValueKeeper.BlackHead3Segment2Value = input; }
        public double GetBlackHead3Segment3Value()
        { return HeadValueKeeper.BlackHead3Segment3Value; }
        public void SetBlackHead3Segment3Value(double input)
        { HeadValueKeeper.BlackHead3Segment3Value = input; }
        public double GetBlackHead3Segment4Value()
        { return HeadValueKeeper.BlackHead3Segment4Value; }
        public void SetBlackHead3Segment4Value(double input)
        { HeadValueKeeper.BlackHead3Segment4Value = input; }

        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        public double GetBlackHead2Segment1Value()
        { return HeadValueKeeper.BlackHead2Segment1Value; }
        public void SetBlackHead2Segment1Value(double input)
        { HeadValueKeeper.BlackHead2Segment1Value = input; }
        public double GetBlackHead2Segment2Value()
        { return HeadValueKeeper.BlackHead2Segment2Value; }
        public void SetBlackHead2Segment2Value(double input)
        { HeadValueKeeper.BlackHead2Segment2Value = input; }
        public double GetBlackHead2Segment3Value()
        { return HeadValueKeeper.BlackHead2Segment3Value; }
        public void SetBlackHead2Segment3Value(double input)
        { HeadValueKeeper.BlackHead2Segment3Value = input; }
        public double GetBlackHead2Segment4Value()
        { return HeadValueKeeper.BlackHead2Segment4Value; }
        public void SetBlackHead2Segment4Value(double input)
        { HeadValueKeeper.BlackHead2Segment4Value = input; }

        /// <summary>
        /// Head1
        /// </summary>
        /// <returns></returns>
        public double GetBlackHead1Segment1Value()
        { return HeadValueKeeper.BlackHead1Segment1Value; }
        public void SetBlackHead1Segment1Value(double input)
        { HeadValueKeeper.BlackHead1Segment1Value = input; }
        public double GetBlackHead1Segment2Value()
        { return HeadValueKeeper.BlackHead1Segment2Value; }
        public void SetBlackHead1Segment2Value(double input)
        { HeadValueKeeper.BlackHead1Segment2Value = input; }
        public double GetBlackHead1Segment3Value()
        { return HeadValueKeeper.BlackHead1Segment3Value; }
        public void SetBlackHead1Segment3Value(double input)
        { HeadValueKeeper.BlackHead1Segment3Value = input; }
        public double GetBlackHead1Segment4Value()
        { return HeadValueKeeper.BlackHead1Segment4Value; }
        public void SetBlackHead1Segment4Value(double input)
        { HeadValueKeeper.BlackHead1Segment4Value = input; }

        ////////////////////////////////////////////////
        ///Black2
        ///////////////////////////////////////////////
        /// <summary>
        /// Head 3 
        /// </summary>
        /// <returns></returns>
        public double GetBlack2Head3Segment1Value()
        { return HeadValueKeeper.Black2Head3Segment1Value; }
        public void SetBlack2Head3Segment1Value(double input)
        { HeadValueKeeper.Black2Head3Segment1Value = input; }
        public double GetBlack2Head3Segment2Value()
        { return HeadValueKeeper.Black2Head3Segment2Value; }
        public void SetBlack2Head3Segment2Value(double input)
        { HeadValueKeeper.Black2Head3Segment2Value = input; }
        public double GetBlack2Head3Segment3Value()
        { return HeadValueKeeper.Black2Head3Segment3Value; }
        public void SetBlack2Head3Segment3Value(double input)
        { HeadValueKeeper.Black2Head3Segment3Value = input; }
        public double GetBlack2Head3Segment4Value()
        { return HeadValueKeeper.Black2Head3Segment4Value; }
        public void SetBlack2Head3Segment4Value(double input)
        { HeadValueKeeper.Black2Head3Segment4Value = input; }

        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        public double GetBlack2Head2Segment1Value()
        { return HeadValueKeeper.Black2Head2Segment1Value; }
        public void SetBlack2Head2Segment1Value(double input)
        { HeadValueKeeper.Black2Head2Segment1Value = input; }
        public double GetBlack2Head2Segment2Value()
        { return HeadValueKeeper.Black2Head2Segment2Value; }
        public void SetBlack2Head2Segment2Value(double input)
        { HeadValueKeeper.Black2Head2Segment2Value = input; }
        public double GetBlack2Head2Segment3Value()
        { return HeadValueKeeper.Black2Head2Segment3Value; }
        public void SetBlack2Head2Segment3Value(double input)
        { HeadValueKeeper.Black2Head2Segment3Value = input; }
        public double GetBlack2Head2Segment4Value()
        { return HeadValueKeeper.Black2Head2Segment4Value; }
        public void SetBlack2Head2Segment4Value(double input)
        { HeadValueKeeper.Black2Head2Segment4Value = input; }

        /// <summary>
        /// Head1
        /// </summary>
        /// <returns></returns>
        public double GetBlack2Head1Segment1Value()
        { return HeadValueKeeper.Black2Head1Segment1Value; }
        public void SetBlack2Head1Segment1Value(double input)
        { HeadValueKeeper.Black2Head1Segment1Value = input; }
        public double GetBlack2Head1Segment2Value()
        { return HeadValueKeeper.Black2Head1Segment2Value; }
        public void SetBlack2Head1Segment2Value(double input)
        { HeadValueKeeper.Black2Head1Segment2Value = input; }
        public double GetBlack2Head1Segment3Value()
        { return HeadValueKeeper.Black2Head1Segment3Value; }
        public void SetBlack2Head1Segment3Value(double input)
        { HeadValueKeeper.Black2Head1Segment3Value = input; }
        public double GetBlack2Head1Segment4Value()
        { return HeadValueKeeper.Black2Head1Segment4Value; }
        public void SetBlack2Head1Segment4Value(double input)
        { HeadValueKeeper.Black2Head1Segment4Value = input; }

        ////////////////////////////////////////////////
        ///Yellow
        ///////////////////////////////////////////////
        /// <summary>
        /// Head 3 
        /// </summary>
        /// <returns></returns>
        public double GetYellowHead3Segment1Value()
        { return HeadValueKeeper.YellowHead3Segment1Value; }
        public void SetYellowHead3Segment1Value(double input)
        { HeadValueKeeper.YellowHead3Segment1Value = input; }
        public double GetYellowHead3Segment2Value()
        { return HeadValueKeeper.YellowHead3Segment2Value; }
        public void SetYellowHead3Segment2Value(double input)
        { HeadValueKeeper.YellowHead3Segment2Value = input; }
        public double GetYellowHead3Segment3Value()
        { return HeadValueKeeper.YellowHead3Segment3Value; }
        public void SetYellowHead3Segment3Value(double input)
        { HeadValueKeeper.YellowHead3Segment3Value = input; }
        public double GetYellowHead3Segment4Value()
        { return HeadValueKeeper.YellowHead3Segment4Value; }
        public void SetYellowHead3Segment4Value(double input)
        { HeadValueKeeper.YellowHead3Segment4Value = input; }

        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        public double GetYellowHead2Segment1Value()
        { return HeadValueKeeper.YellowHead2Segment1Value; }
        public void SetYellowHead2Segment1Value(double input)
        { HeadValueKeeper.YellowHead2Segment1Value = input; }
        public double GetYellowHead2Segment2Value()
        { return HeadValueKeeper.YellowHead2Segment2Value; }
        public void SetYellowHead2Segment2Value(double input)
        { HeadValueKeeper.YellowHead2Segment2Value = input; }
        public double GetYellowHead2Segment3Value()
        { return HeadValueKeeper.YellowHead2Segment3Value; }
        public void SetYellowHead2Segment3Value(double input)
        { HeadValueKeeper.YellowHead2Segment3Value = input; }
        public double GetYellowHead2Segment4Value()
        { return HeadValueKeeper.YellowHead2Segment4Value; }
        public void SetYellowHead2Segment4Value(double input)
        { HeadValueKeeper.YellowHead2Segment4Value = input; }

        /// <summary>
        /// Head1
        /// </summary>
        /// <returns></returns>
        public double GetYellowHead1Segment1Value()
        { return HeadValueKeeper.YellowHead1Segment1Value; }
        public void SetYellowHead1Segment1Value(double input)
        { HeadValueKeeper.YellowHead1Segment1Value = input; }
        public double GetYellowHead1Segment2Value()
        { return HeadValueKeeper.YellowHead1Segment2Value; }
        public void SetYellowHead1Segment2Value(double input)
        { HeadValueKeeper.YellowHead1Segment2Value = input; }
        public double GetYellowHead1Segment3Value()
        { return HeadValueKeeper.YellowHead1Segment3Value; }
        public void SetYellowHead1Segment3Value(double input)
        { HeadValueKeeper.YellowHead1Segment3Value = input; }
        public double GetYellowHead1Segment4Value()
        { return HeadValueKeeper.YellowHead1Segment4Value; }
        public void SetYellowHead1Segment4Value(double input)
        { HeadValueKeeper.YellowHead1Segment4Value = input; }

        ////////////////////////////////////////////////
        ///Yellow2
        ///////////////////////////////////////////////
        /// <summary>
        /// Head 3 
        /// </summary>
        /// <returns></returns>
        public double GetYellow2Head3Segment1Value()
        { return HeadValueKeeper.Yellow2Head3Segment1Value; }
        public void SetYellow2Head3Segment1Value(double input)
        { HeadValueKeeper.Yellow2Head3Segment1Value = input; }
        public double GetYellow2Head3Segment2Value()
        { return HeadValueKeeper.Yellow2Head3Segment2Value; }
        public void SetYellow2Head3Segment2Value(double input)
        { HeadValueKeeper.Yellow2Head3Segment2Value = input; }
        public double GetYellow2Head3Segment3Value()
        { return HeadValueKeeper.Yellow2Head3Segment3Value; }
        public void SetYellow2Head3Segment3Value(double input)
        { HeadValueKeeper.Yellow2Head3Segment3Value = input; }
        public double GetYellow2Head3Segment4Value()
        { return HeadValueKeeper.Yellow2Head3Segment4Value; }
        public void SetYellow2Head3Segment4Value(double input)
        { HeadValueKeeper.Yellow2Head3Segment4Value = input; }

        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        public double GetYellow2Head2Segment1Value()
        { return HeadValueKeeper.Yellow2Head2Segment1Value; }
        public void SetYellow2Head2Segment1Value(double input)
        { HeadValueKeeper.Yellow2Head2Segment1Value = input; }
        public double GetYellow2Head2Segment2Value()
        { return HeadValueKeeper.Yellow2Head2Segment2Value; }
        public void SetYellow2Head2Segment2Value(double input)
        { HeadValueKeeper.Yellow2Head2Segment2Value = input; }
        public double GetYellow2Head2Segment3Value()
        { return HeadValueKeeper.Yellow2Head2Segment3Value; }
        public void SetYellow2Head2Segment3Value(double input)
        { HeadValueKeeper.Yellow2Head2Segment3Value = input; }
        public double GetYellow2Head2Segment4Value()
        { return HeadValueKeeper.Yellow2Head2Segment4Value; }
        public void SetYellow2Head2Segment4Value(double input)
        { HeadValueKeeper.Yellow2Head2Segment4Value = input; }

        /// <summary>
        /// Head1
        /// </summary>
        /// <returns></returns>
        public double GetYellow2Head1Segment1Value()
        { return HeadValueKeeper.Yellow2Head1Segment1Value; }
        public void SetYellow2Head1Segment1Value(double input)
        { HeadValueKeeper.Yellow2Head1Segment1Value = input; }
        public double GetYellow2Head1Segment2Value()
        { return HeadValueKeeper.Yellow2Head1Segment2Value; }
        public void SetYellow2Head1Segment2Value(double input)
        { HeadValueKeeper.Yellow2Head1Segment2Value = input; }
        public double GetYellow2Head1Segment3Value()
        { return HeadValueKeeper.Yellow2Head1Segment3Value; }
        public void SetYellow2Head1Segment3Value(double input)
        { HeadValueKeeper.Yellow2Head1Segment3Value = input; }
        public double GetYellow2Head1Segment4Value()
        { return HeadValueKeeper.Yellow2Head1Segment4Value; }
        public void SetYellow2Head1Segment4Value(double input)
        { HeadValueKeeper.Yellow2Head1Segment4Value = input; }


        ////////////////////////////////////////////////
        ///Magenta
        ///////////////////////////////////////////////
        /// <summary>
        /// Head 3 
        /// </summary>
        /// <returns></returns>
        public double GetMagentaHead3Segment1Value()
        { return HeadValueKeeper.MagentaHead3Segment1Value; }
        public void SetMagentaHead3Segment1Value(double input)
        { HeadValueKeeper.MagentaHead3Segment1Value = input; }
        public double GetMagentaHead3Segment2Value()
        { return HeadValueKeeper.MagentaHead3Segment2Value; }
        public void SetMagentaHead3Segment2Value(double input)
        { HeadValueKeeper.MagentaHead3Segment2Value = input; }
        public double GetMagentaHead3Segment3Value()
        { return HeadValueKeeper.MagentaHead3Segment3Value; }
        public void SetMagentaHead3Segment3Value(double input)
        { HeadValueKeeper.MagentaHead3Segment3Value = input; }
        public double GetMagentaHead3Segment4Value()
        { return HeadValueKeeper.MagentaHead3Segment4Value; }
        public void SetMagentaHead3Segment4Value(double input)
        { HeadValueKeeper.MagentaHead3Segment4Value = input; }

        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        public double GetMagentaHead2Segment1Value()
        { return HeadValueKeeper.MagentaHead2Segment1Value; }
        public void SetMagentaHead2Segment1Value(double input)
        { HeadValueKeeper.MagentaHead2Segment1Value = input; }
        public double GetMagentaHead2Segment2Value()
        { return HeadValueKeeper.MagentaHead2Segment2Value; }
        public void SetMagentaHead2Segment2Value(double input)
        { HeadValueKeeper.MagentaHead2Segment2Value = input; }
        public double GetMagentaHead2Segment3Value()
        { return HeadValueKeeper.MagentaHead2Segment3Value; }
        public void SetMagentaHead2Segment3Value(double input)
        { HeadValueKeeper.MagentaHead2Segment3Value = input; }
        public double GetMagentaHead2Segment4Value()
        { return HeadValueKeeper.MagentaHead2Segment4Value; }
        public void SetMagentaHead2Segment4Value(double input)
        { HeadValueKeeper.MagentaHead2Segment4Value = input; }

        /// <summary>
        /// Head1
        /// </summary>
        /// <returns></returns>
        public double GetMagentaHead1Segment1Value()
        { return HeadValueKeeper.MagentaHead1Segment1Value; }
        public void SetMagentaHead1Segment1Value(double input)
        { HeadValueKeeper.MagentaHead1Segment1Value = input; }
        public double GetMagentaHead1Segment2Value()
        { return HeadValueKeeper.MagentaHead1Segment2Value; }
        public void SetMagentaHead1Segment2Value(double input)
        { HeadValueKeeper.MagentaHead1Segment2Value = input; }
        public double GetMagentaHead1Segment3Value()
        { return HeadValueKeeper.MagentaHead1Segment3Value; }
        public void SetMagentaHead1Segment3Value(double input)
        { HeadValueKeeper.MagentaHead1Segment3Value = input; }
        public double GetMagentaHead1Segment4Value()
        { return HeadValueKeeper.MagentaHead1Segment4Value; }
        public void SetMagentaHead1Segment4Value(double input)
        { HeadValueKeeper.MagentaHead1Segment4Value = input; }

        ////////////////////////////////////////////////
        ///Magenta2
        ///////////////////////////////////////////////
        /// <summary>
        /// Head 3 
        /// </summary>
        /// <returns></returns>
        public double GetMagenta2Head3Segment1Value()
        { return HeadValueKeeper.Magenta2Head3Segment1Value; }
        public void SetMagenta2Head3Segment1Value(double input)
        { HeadValueKeeper.Magenta2Head3Segment1Value = input; }
        public double GetMagenta2Head3Segment2Value()
        { return HeadValueKeeper.Magenta2Head3Segment2Value; }
        public void SetMagenta2Head3Segment2Value(double input)
        { HeadValueKeeper.Magenta2Head3Segment2Value = input; }
        public double GetMagenta2Head3Segment3Value()
        { return HeadValueKeeper.Magenta2Head3Segment3Value; }
        public void SetMagenta2Head3Segment3Value(double input)
        { HeadValueKeeper.Magenta2Head3Segment3Value = input; }
        public double GetMagenta2Head3Segment4Value()
        { return HeadValueKeeper.Magenta2Head3Segment4Value; }
        public void SetMagenta2Head3Segment4Value(double input)
        { HeadValueKeeper.Magenta2Head3Segment4Value = input; }

        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        public double GetMagenta2Head2Segment1Value()
        { return HeadValueKeeper.Magenta2Head2Segment1Value; }
        public void SetMagenta2Head2Segment1Value(double input)
        { HeadValueKeeper.Magenta2Head2Segment1Value = input; }
        public double GetMagenta2Head2Segment2Value()
        { return HeadValueKeeper.Magenta2Head2Segment2Value; }
        public void SetMagenta2Head2Segment2Value(double input)
        { HeadValueKeeper.Magenta2Head2Segment2Value = input; }
        public double GetMagenta2Head2Segment3Value()
        { return HeadValueKeeper.Magenta2Head2Segment3Value; }
        public void SetMagenta2Head2Segment3Value(double input)
        { HeadValueKeeper.Magenta2Head2Segment3Value = input; }
        public double GetMagenta2Head2Segment4Value()
        { return HeadValueKeeper.Magenta2Head2Segment4Value; }
        public void SetMagenta2Head2Segment4Value(double input)
        { HeadValueKeeper.Magenta2Head2Segment4Value = input; }

        /// <summary>
        /// Head1
        /// </summary>
        /// <returns></returns>
        public double GetMagenta2Head1Segment1Value()
        { return HeadValueKeeper.Magenta2Head1Segment1Value; }
        public void SetMagenta2Head1Segment1Value(double input)
        { HeadValueKeeper.Magenta2Head1Segment1Value = input; }
        public double GetMagenta2Head1Segment2Value()
        { return HeadValueKeeper.Magenta2Head1Segment2Value; }
        public void SetMagenta2Head1Segment2Value(double input)
        { HeadValueKeeper.Magenta2Head1Segment2Value = input; }
        public double GetMagenta2Head1Segment3Value()
        { return HeadValueKeeper.Magenta2Head1Segment3Value; }
        public void SetMagenta2Head1Segment3Value(double input)
        { HeadValueKeeper.Magenta2Head1Segment3Value = input; }
        public double GetMagenta2Head1Segment4Value()
        { return HeadValueKeeper.Magenta2Head1Segment4Value; }
        public void SetMagenta2Head1Segment4Value(double input)
        { HeadValueKeeper.Magenta2Head1Segment4Value = input; }
        ////////////////////////////////////////////////
        ///Cyan
        ///////////////////////////////////////////////
        /// <summary>
        /// Head 3 
        /// </summary>
        /// <returns></returns>
        public double GetCyanHead3Segment1Value()
        { return HeadValueKeeper.CyanHead3Segment1Value; }
        public void SetCyanHead3Segment1Value(double input)
        { HeadValueKeeper.CyanHead3Segment1Value = input; }
        public double GetCyanHead3Segment2Value()
        { return HeadValueKeeper.CyanHead3Segment2Value; }
        public void SetCyanHead3Segment2Value(double input)
        { HeadValueKeeper.CyanHead3Segment2Value = input; }
        public double GetCyanHead3Segment3Value()
        { return HeadValueKeeper.CyanHead3Segment3Value; }
        public void SetCyanHead3Segment3Value(double input)
        { HeadValueKeeper.CyanHead3Segment3Value = input; }
        public double GetCyanHead3Segment4Value()
        { return HeadValueKeeper.CyanHead3Segment4Value; }
        public void SetCyanHead3Segment4Value(double input)
        { HeadValueKeeper.CyanHead3Segment4Value = input; }

        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        public double GetCyanHead2Segment1Value()
        { return HeadValueKeeper.CyanHead2Segment1Value; }
        public void SetCyanHead2Segment1Value(double input)
        { HeadValueKeeper.CyanHead2Segment1Value = input; }
        public double GetCyanHead2Segment2Value()
        { return HeadValueKeeper.CyanHead2Segment2Value; }
        public void SetCyanHead2Segment2Value(double input)
        { HeadValueKeeper.CyanHead2Segment2Value = input; }
        public double GetCyanHead2Segment3Value()
        { return HeadValueKeeper.CyanHead2Segment3Value; }
        public void SetCyanHead2Segment3Value(double input)
        { HeadValueKeeper.CyanHead2Segment3Value = input; }
        public double GetCyanHead2Segment4Value()
        { return HeadValueKeeper.CyanHead2Segment4Value; }
        public void SetCyanHead2Segment4Value(double input)
        { HeadValueKeeper.CyanHead2Segment4Value = input; }

        /// <summary>
        /// Head1
        /// </summary>
        /// <returns></returns>
        public double GetCyanHead1Segment1Value()
        { return HeadValueKeeper.CyanHead1Segment1Value; }
        public void SetCyanHead1Segment1Value(double input)
        { HeadValueKeeper.CyanHead1Segment1Value = input; }
        public double GetCyanHead1Segment2Value()
        { return HeadValueKeeper.CyanHead1Segment2Value; }
        public void SetCyanHead1Segment2Value(double input)
        { HeadValueKeeper.CyanHead1Segment2Value = input; }
        public double GetCyanHead1Segment3Value()
        { return HeadValueKeeper.CyanHead1Segment3Value; }
        public void SetCyanHead1Segment3Value(double input)
        { HeadValueKeeper.CyanHead1Segment3Value = input; }
        public double GetCyanHead1Segment4Value()
        { return HeadValueKeeper.CyanHead1Segment4Value; }
        public void SetCyanHead1Segment4Value(double input)
        { HeadValueKeeper.CyanHead1Segment4Value = input; }

        ////////////////////////////////////////////////
        ///Cyan2
        ///////////////////////////////////////////////
        /// <summary>
        /// Head 3 
        /// </summary>
        /// <returns></returns>
        public double GetCyan2Head3Segment1Value()
        { return HeadValueKeeper.Cyan2Head3Segment1Value; }
        public void SetCyan2Head3Segment1Value(double input)
        { HeadValueKeeper.Cyan2Head3Segment1Value = input; }
        public double GetCyan2Head3Segment2Value()
        { return HeadValueKeeper.Cyan2Head3Segment2Value; }
        public void SetCyan2Head3Segment2Value(double input)
        { HeadValueKeeper.Cyan2Head3Segment2Value = input; }
        public double GetCyan2Head3Segment3Value()
        { return HeadValueKeeper.Cyan2Head3Segment3Value; }
        public void SetCyan2Head3Segment3Value(double input)
        { HeadValueKeeper.Cyan2Head3Segment3Value = input; }
        public double GetCyan2Head3Segment4Value()
        { return HeadValueKeeper.Cyan2Head3Segment4Value; }
        public void SetCyan2Head3Segment4Value(double input)
        { HeadValueKeeper.Cyan2Head3Segment4Value = input; }

        /// <summary>
        /// Head2
        /// </summary>
        /// <returns></returns>
        public double GetCyan2Head2Segment1Value()
        { return HeadValueKeeper.Cyan2Head2Segment1Value; }
        public void SetCyan2Head2Segment1Value(double input)
        { HeadValueKeeper.Cyan2Head2Segment1Value = input; }
        public double GetCyan2Head2Segment2Value()
        { return HeadValueKeeper.Cyan2Head2Segment2Value; }
        public void SetCyan2Head2Segment2Value(double input)
        { HeadValueKeeper.Cyan2Head2Segment2Value = input; }
        public double GetCyan2Head2Segment3Value()
        { return HeadValueKeeper.Cyan2Head2Segment3Value; }
        public void SetCyan2Head2Segment3Value(double input)
        { HeadValueKeeper.Cyan2Head2Segment3Value = input; }
        public double GetCyan2Head2Segment4Value()
        { return HeadValueKeeper.Cyan2Head2Segment4Value; }
        public void SetCyan2Head2Segment4Value(double input)
        { HeadValueKeeper.Cyan2Head2Segment4Value = input; }

        /// <summary>
        /// Head1
        /// </summary>
        /// <returns></returns>
        public double GetCyan2Head1Segment1Value()
        { return HeadValueKeeper.Cyan2Head1Segment1Value; }
        public void SetCyan2Head1Segment1Value(double input)
        { HeadValueKeeper.Cyan2Head1Segment1Value = input; }
        public double GetCyan2Head1Segment2Value()
        { return HeadValueKeeper.Cyan2Head1Segment2Value; }
        public void SetCyan2Head1Segment2Value(double input)
        { HeadValueKeeper.Cyan2Head1Segment2Value = input; }
        public double GetCyan2Head1Segment3Value()
        { return HeadValueKeeper.Cyan2Head1Segment3Value; }
        public void SetCyan2Head1Segment3Value(double input)
        { HeadValueKeeper.Cyan2Head1Segment3Value = input; }
        public double GetCyan2Head1Segment4Value()
        { return HeadValueKeeper.Cyan2Head1Segment4Value; }
        public void SetCyan2Head1Segment4Value(double input)
        { HeadValueKeeper.Cyan2Head1Segment4Value = input; }
    }
}
