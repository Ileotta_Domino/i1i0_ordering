﻿using I1I0Ordering.Model;
using I1I0Ordering.Model.Interface;
using I1I0Ordering.Parameters;
using I1I0Ordering.Parameters.Interface;
using I1I0Ordering.ViewModels;
using I1I0Ordering.ViewModels.Interfaces;
using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace I1I0Ordering
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private IKernel container;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var container = new StandardKernel();

            // Register types
            container.Bind<IExtractor>().To<Extractor>();
            container.Bind<IListProcessor>().To<ListProcessor>();
            container.Bind<ICalculatorTools>().To<CalculatorTools>();
            container.Bind<ILABDensityValuesProcessor>().To<LABDensityValuesProcessor>();
            container.Bind<IFileProcessor>().To<FileProcessor>();
            container.Bind<IHeadValueProcessor>().To<HeadValueProcessor>();
            container.Bind<ISuggestedValuesCalculator>().To<SuggestedValuesCalculator>();

            MainWindow mw = new MainWindow();
            mw.DataContext = container.Get<MainViewModel>(); 
            mw.Show();
        }
        private void ComposeObjects()
        {
            Current.MainWindow = this.container.Get<MainWindow>();
        }
         
        private void ConfigureContainer()
        {
            this.container = new StandardKernel();

        }
    }
}
